import UIKit
import Flutter
import flutter_downloader
import NaverThirdPartyLogin
import Firebase
@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(_ application: UIApplication,
                              didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }
        GeneratedPluginRegistrant.register(with: self)
        FlutterDownloaderPlugin.setPluginRegistrantCallback(registerPlugins)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    override func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return NaverThirdPartyLoginConnection.getSharedInstance().application(app, open: url, options: options)
    }
    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Pass device token to auth
        
        Auth.auth().setAPNSToken(deviceToken, type: .unknown)
        
        // Pass device token to messaging
        Messaging.messaging().apnsToken = deviceToken
        
        return super.application(application, didRegisterForRemoteNotificationsWithDeviceToken: deviceToken)
    }
    
    // https://firebase.google.com/docs/auth/ios/phone-auth#appendix:-using-phone-sign-in-without-swizzling
    // https://firebase.google.com/docs/cloud-messaging/ios/receive#handle-swizzle
    override func application(_ application: UIApplication,
                              didReceiveRemoteNotification notification: [AnyHashable : Any],
                              fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Handle the message for firebase auth phone verification
        if Auth.auth().canHandleNotification(notification) {
            completionHandler(.noData)
            return
        }
        
        // Handle it for firebase messaging analytics
        if ((notification["gcm.message_id"]) != nil) {
            Messaging.messaging().appDidReceiveMessage(notification)
        }
        
        return super.application(application, didReceiveRemoteNotification: notification, fetchCompletionHandler: completionHandler)
    }
    
}
private func registerPlugins(registry: FlutterPluginRegistry) {
    if (!registry.hasPlugin("FlutterDownloaderPlugin")) {
        FlutterDownloaderPlugin.register(with: registry.registrar(forPlugin:"FlutterDownloaderPlugin")!)
    }
}

import 'dart:core';

String _fillZero(String timestamp) {
  if (timestamp.length == 1) {
    return "0"+timestamp;
  } else {
    return timestamp;
  }
}

String formattedTime(String timestamp) {
  DateTime targetTime = DateTime.tryParse(timestamp);
  if(targetTime == null) return timestamp;
  DateTime currentTime = DateTime.now();


  if (currentTime.difference(targetTime).inMinutes == 0) {
    return "방금";
  }
  if (currentTime.difference(targetTime).inMinutes < 60) {
    return "${currentTime.difference(targetTime).inMinutes}분 전";
  }
  if (currentTime.difference(targetTime).inHours < 24) {
    return "${currentTime.difference(targetTime).inHours}시간 전";
  }
  if (targetTime.year != currentTime.year) {
    return "${targetTime.year.toString()}년 ${targetTime.month.toString()}월 ${targetTime.day.toString()}일 ${_fillZero(targetTime.hour.toString())}:${_fillZero(targetTime.minute.toString())}";
  }
  return "${targetTime.month.toString()}월 ${targetTime.day.toString()}일 ${_fillZero(targetTime.hour.toString())}:${_fillZero(targetTime.minute.toString())}";
}

String formattedTimeAfter(String timestamp){
  DateTime targetTime = DateTime.tryParse(timestamp);
  if(targetTime == null ) return "";
  return "${DateTime.now().difference(targetTime).abs().inDays}일 후";
}
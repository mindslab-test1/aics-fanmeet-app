import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
/*
 status.isGranted : 권한을 부여받은 상태
 status.isRestricted : 권한을 제한적으로 부여받은 상태 ex) 아이보호모드가 걸린 아이 핸드폰의 권한
 status.isDenied : 권한 부여를 거절당한 상태
 status.isUndetermined : 권한 부여에 대한 결정을 못내린 상태
 status.isLimited : iOS에서 선택한 사진에 대한 권한을 부여 받은 상태
*/
/*
 openImagePicker와 openImagesPicker, openVideoPicker가 storage 권한을 받는 이유
 1. storage에서 이미지 혹은 비디오만 선택하는 경우에 다양한 옵션이 제공된다. ex) 파일 확장자 체크, 파일 용량체크, 여러 이미지 선택
 2. 고객사에서 해당 경로로 파일을 선택해주기를 원함
*/
extension PermissionStatusUtils on PermissionStatus {
  bool isAvailable() {
    switch (this) {
      case PermissionStatus.granted:
      case PermissionStatus.restricted:
      case PermissionStatus.limited:
        return true;
      default:
        return false;
    }
  }

  bool shouldReAskPermission() {
    return this == PermissionStatus.denied;
  }
}

Future<void> askCameraPermissionIOS(PermissionStatus status) async {
  if (status.shouldReAskPermission()) {
    await openAppSettings();
  } else {
    await Permission.camera.request();
  }
}

Future<void> askGalleryPermissionIOS(PermissionStatus status) async {
  if (status.shouldReAskPermission()) {
    await openAppSettings();
  } else {
    await Permission.photos.request();
  }
}

Future<void> askGalleryPermissionAndroid(PermissionStatus status) async {
  await Permission.photos.request();
}

Future<void> askCameraPermissionAndroid(PermissionStatus status) async {
  await Permission.camera.request();
}

Future<void> askStoragePermissionIOS(PermissionStatus status) async {
  if (status.shouldReAskPermission()) {
    await openAppSettings();
  } else {
    await Permission.storage.request();
  }
}

Future<void> askStoragePermissionAndroid(PermissionStatus status) async {
  await Permission.storage.request();
}

bool checkVideoExtension(FilePickerResult filePickerResult) {
  List<String> extensions = ['mp4', 'avi', 'wmv', 'mov', 'MP4', 'AVI', 'WVM', 'MOV'];
  if(filePickerResult == null) return false;
  if (!extensions.contains(filePickerResult.files.first.extension)) {
    Fluttertoast.showToast(msg: "." + filePickerResult.files.first.extension + " 확장자 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
    return false;
  }
  return true;
}

bool checkVideoSize(FilePickerResult filePickerResult){
  if(filePickerResult == null) return false;
  if (filePickerResult.files.first.size > 524288000) {
    Fluttertoast.showToast(msg: "500 MB가 넘는 비디오 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
    return false;
  }
  return true;
}

bool checkVideoAvailability(FilePickerResult filePickerResult){
  return checkVideoSize(filePickerResult)&&checkVideoExtension(filePickerResult);
}

Future<File> getImageFromCamera() async {
  PickedFile imageFile = await ImagePicker().getImage(source: ImageSource.camera);
  return imageFile == null ? null : File(imageFile.path);
}

Future<File> getImageFromGallery() async{
  FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(type: FileType.image);
  if(filePickerResult == null) return null;
  if(filePickerResult.files.first.extension == "bmp") {
    Fluttertoast.showToast(msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
    return null;
  }
  return File(filePickerResult.files.first.path);
}

Future<List<File>> getImagesFromGallery() async {
  FilePickerResult filePickerResults = await FilePicker.platform.pickFiles(type: FileType.image, allowMultiple: true);
  List<File> files = [];
  if(filePickerResults == null) return null;
  for (int i = 0; i < filePickerResults.files.length; i++) {
    if (filePickerResults.files[i].extension == "bmp") {
      Fluttertoast.showToast(msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
      return null;
    } else {
      files.add(File(filePickerResults.files[i].path));
    }
  }
  return files;
}

Future<File> getVideoFromGallery() async{
  FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(type: FileType.video);
  if(!checkVideoAvailability(filePickerResult))
    return null;
  return filePickerResult == null ? null : File(filePickerResult.files.first.path);
}

Future<File> openGallery() async {
  var imageFile = await ImagePicker().getImage(source: ImageSource.gallery);
  return File(imageFile.path);
}

Future<File> openCamera() async {
  PermissionStatus status = await Permission.camera.status;
  if (!status.isAvailable()) {
    if (Platform.isAndroid) {
      await askCameraPermissionAndroid(status);
    }
    if (Platform.isIOS) {
      await askCameraPermissionIOS(status);
    }
  }

  PermissionStatus updatedStatus = await Permission.camera.status;
  if (!updatedStatus.isAvailable()) {
    return null;
  }

  return getImageFromCamera();
}

Future<File> openImagePicker() async {
  var status = await Permission.storage.status;
  if (!status.isAvailable()) {
    if (Platform.isAndroid) {
      await askStoragePermissionAndroid(status);
    }
    if (Platform.isIOS) {
      await askStoragePermissionIOS(status);
    }
  }
  PermissionStatus updatedStatus = await Permission.storage.status;
  if (!updatedStatus.isAvailable()) {
    return null;
  }

  return getImageFromGallery();
}

Future<List<File>> openImagesPicker() async {
  var status = await Permission.storage.status;
  if (!status.isAvailable()) {
    if (Platform.isAndroid) {
      await askStoragePermissionAndroid(status);
    }
    if (Platform.isIOS) {
      await askStoragePermissionIOS(status);
    }
  }
  PermissionStatus updatedStatus = await Permission.storage.status;
  if (!updatedStatus.isAvailable()) {
    return null;
  }

  return getImagesFromGallery();
}


Future<File> openVideoPicker() async {
  var status = await Permission.storage.status;
  if (!status.isAvailable()) {
    if (Platform.isAndroid) {
      await askStoragePermissionAndroid(status);
    }
    if (Platform.isIOS) {
      await askStoragePermissionIOS(status);
    }
  }
  PermissionStatus updatedStatus = await Permission.storage.status;
  if (!updatedStatus.isAvailable()) {
    return null;
  }
  return getVideoFromGallery();
}

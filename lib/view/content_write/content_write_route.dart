import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/content/content_write.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/view/content_write/content_write_body.dart';

import '../../colors.dart';

class ContentWriteRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _ContentWriteAppBar(),
      ),
      body: ContentWriteBody(),
    );
  }
}

class _ContentWriteAppBar extends StatelessWidget {
  ProgressDialog _pr;

  @override
  Widget build(BuildContext context) {
//    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
//      return _pr == null ? true : false;
//    });
    ContentWriteDataProvider _contentWriteDataProvider = Provider.of<ContentWriteDataProvider>(context, listen: false);
    _pr = ProgressDialog(
      context,
      showLogs: true,
      isDismissible: false,
      customBody: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 45,
              width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
              ),
            ),
            SizedBox(
              height: 15,
            ),
            Text(
              "업로드 중입니다.",
              style: TextStyle(
                  fontSize: 12,
                  color: kNegativeTextColor
              ),
            )
          ],
        ),
      )
    );
    _pr.style(
      backgroundColor: Colors.transparent,
      elevation: 0,
      padding: const EdgeInsets.all(0)
    );

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "콘텐츠 올리기",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: () async {
                        _contentWriteDataProvider.isUploadingVideo = true;
                        await _pr.show();
                        Future.delayed(Duration(milliseconds: 200));
                        if (_contentWriteDataProvider.text == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "빈 텍스트는 업로드할 수 없습니다.", gravity: ToastGravity.TOP);
                          return;
                        }
                        bool isSuccess = await httpContentWrite(
                            title: _contentWriteDataProvider.title,
                            context: context,
                            categoryId: _contentWriteDataProvider.categoryId,
                            text: _contentWriteDataProvider.text,
                            imageUrls: _contentWriteDataProvider.imageUrls,
                            videoUrl: _contentWriteDataProvider.videoUrl,
                            youtubeUrl: _contentWriteDataProvider.youtubeUrl,
                            accessLevel: _contentWriteDataProvider.tier
                        );
                        if (isSuccess) {
                          await _pr.hide();
                          Navigator.pop(context, true);
                        }
                        else {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "피드 업로드에 실패했습니다.", gravity: ToastGravity.TOP);
                        }
                        _contentWriteDataProvider.isUploadingVideo=false;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: kAppBarButtonColor,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}



import 'dart:async';
import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:in_app_purchase/store_kit_wrappers.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_membership_detail_dto.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';
import 'package:rf_tap_fanseem/http/service/service_get_membership_detail.dart';
import 'package:rf_tap_fanseem/http/service/service_get_user_membership.dart';
import 'package:rf_tap_fanseem/http/service/service_new_membership.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import '../../components/progress_dialog.dart';

import '../../routes.dart';

class MembershipDetailIOSBody extends StatefulWidget {

  const MembershipDetailIOSBody({
    Key key,
  }) : super(key: key);

  @override
  _MembershipDetailIOSBodyState createState() => _MembershipDetailIOSBodyState();
}

class _MembershipDetailIOSBodyState extends State<MembershipDetailIOSBody> {
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  ProgressDialog _pr;
  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<ProductDetails> _products = [];
  List<PurchaseDetails> _purchases = [];
  bool _isLoaded = false;
  HttpServiceGetMembershipDetailDto _membershipDetail = HttpServiceGetMembershipDetailDto();
  bool _isBuy = false;
  bool _isReset = false;

  @override
  void initState() {
    Stream purchaseUpdated = InAppPurchaseConnection.instance.purchaseUpdatedStream;
    _initStoreInfo();
    _initTransactions();
    _subscription = purchaseUpdated.listen((event) {
      _verifyPurchase(event);
    },
      onDone: () => _subscription.cancel(),
      onError: (e) => print(e.toString()),
    );
    super.initState();
  }

  Future<void> _initTransactions() async{
    var transactions = await SKPaymentQueueWrapper().transactions();
    for (final transaction in transactions) {
      if (transaction.transactionState != SKPaymentTransactionStateWrapper.purchasing) {
        await SKPaymentQueueWrapper().finishTransaction(transaction);
      }
    }
    _isReset=true;
  }
  Future<void> _initStoreInfo() async {
    final bool _isAvailable = await _connection.isAvailable();
    if(_isAvailable) {
      ProductDetailsResponse response = await _connection.queryProductDetails(
          [Provider.of<MembershipProvider>(context, listen: false).membershipId].toSet());
      setState((){
        _products = response.productDetails;
        _isLoaded = true;
      });
    }

//    final QueryPurchaseDetailsResponse purchaseResponse = await _connection.queryPastPurchases();
  }

  Future<void> _buyProduct(String productId) async {
      BackButtonInterceptor.removeByName("popMember");
      final PurchaseParam purchaseParam = PurchaseParam(
          productDetails: _products[0]);
      _isBuy = true;
      await _connection.buyNonConsumable(purchaseParam: purchaseParam);
  }


  void _verifyPurchase(List<PurchaseDetails> purchaseDetailsList) async {
    int userId = Provider.of<LoginVerifyProvider>(context,listen: false).userId;
    purchaseDetailsList.forEach((PurchaseDetails purchaseDetails) async {
      if (purchaseDetails.status == PurchaseStatus.pending) {
      } else {
        if (purchaseDetails.status == PurchaseStatus.error) {
          await InAppPurchaseConnection.instance.completePurchase(purchaseDetails);
          BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
            Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
            return true;
          }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
          await Future.delayed(Duration(seconds: 2));
          Fluttertoast.showToast(msg: "멤버쉽 구매에 문제가 발생하였습니다.", gravity: ToastGravity.TOP);
          await _hidePendingUI();
          Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
          return;
        } else if (purchaseDetails.status == PurchaseStatus.purchased) {
          setState(() {
            _purchases.add(purchaseDetails);
          });
        }
        if(_isBuy) {
            bool isVerified = await httpServiceVerifyReceipt(context, purchaseDetails.verificationData.serverVerificationData, userId);
            bool isCompleted = await httpServiceCompletePurchase(context, purchaseDetails.verificationData.serverVerificationData, userId);
            if (isVerified!=null&&isCompleted!=null&&isVerified==true&&isCompleted==true) {
              if(purchaseDetails.pendingCompletePurchase) {
                Fluttertoast.showToast(
                    msg: "멤버십 구매에 성공했습니다.", gravity: ToastGravity.TOP);
                await InAppPurchaseConnection.instance.completePurchase(
                    purchaseDetails);
              }
              else{
                Fluttertoast.showToast(msg: "서버에서 오류가 발생했습니다. 관리자에게 문의해 주세요.", gravity: ToastGravity.TOP);
                await Future.delayed(Duration(seconds: 2));
                await _hidePendingUI();
              }
              await Future.delayed(Duration(seconds: 2));
              await _hidePendingUI();
              Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
            }
            else{
              Fluttertoast.showToast(msg: "서버에서 오류가 발생했습니다. 관리자에게 문의해 주세요.", gravity: ToastGravity.TOP);
              await Future.delayed(Duration(seconds: 2));
              await _hidePendingUI();
            }
        }
      }
      BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
        Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
        return true;
      }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
    });
  }

  Future<void> _showPendingUI() async {
    await _pr.show();
  }

  Future<void> _hidePendingUI() async {
    await _pr.hide();
  }

  Future<bool> _getData() async {
    _membershipDetail = await httpServiceGetMembershipDetail(context: context, productId: Provider.of<MembershipProvider>(context, listen: false).membershipId);
    if (_membershipDetail == null) return false;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "멤버쉽 구매 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );
    return FutureBuilder<bool>(
        future: _getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          _MembershipInfo(membershipImageUrl: _membershipDetail.badgeImageUrl,),
                          _MembershipBenefits(benefitList: _membershipDetail.benefitDescriptionList == null ? [] : _membershipDetail.benefitDescriptionList,),
                          _MembershipNotice()
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: 70,
                    decoration: BoxDecoration(
                        color: Theme
                            .of(context)
                            .backgroundColor,
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6
                          )
                        ]
                    ),
                    child: GestureDetector(
                      onTap: _isLoaded? ()async {
                        int celebId = Provider
                            .of<SelectedCelebProvider>(context, listen: false)
                            .celebId;
                        HttpServiceGetUserMembershipDto result = await httpServiceGetUserMembership(context: context, celebId: celebId);
                        if (result.tier == null) {
                          await _showPendingUI();
                          await _buyProduct(Provider.of<MembershipProvider>(context, listen: false).membershipId);
                        }
                        else {
                          Fluttertoast.showToast(
                              msg: "이미 해당 셀럽의 멤버십을 이용중입니다. 한 셀럽당 최대 하나의 멤버십만 가입 가능합니다.");
                        }
                      }:null,
                      child: Center(
                        child: Container(
                          width: 320,
                          height: 48,
                          decoration: BoxDecoration(
                              color: kMainColor,
                              border: Border.all(
                                color: _isLoaded&&_isReset?kMainColor:kBorderColor,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                    offset: Offset(0, 3),
                                    blurRadius: 6
                                )
                              ]
                          ),
                          child: Center(
                            child: Text(
                              "월 "+((Provider.of<MembershipProvider>(context).membershipCost/1000).toInt()).toString()+","+(Provider.of<MembershipProvider>(context).membershipCost%1000==0?"000":Provider.of<MembershipProvider>(context).membershipCost%1000).toString()+"원으로 정기 구독하기",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: kMainBackgroundColor
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator(),);
          }
        }
    );
  }
}

class _MembershipInfo extends StatelessWidget {

  final String membershipImageUrl;

  const _MembershipInfo({Key key, this.membershipImageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 188,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  Provider.of<MembershipProvider>(context).membershipName,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              child: Container(
                child: CachedNetworkImage(
                  imageUrl: membershipImageUrl,
                  fit: BoxFit.fitHeight,
                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              )
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  "₩ ${Provider.of<MembershipProvider>(context).membershipCost} 원 / 월",
                  style: TextStyle(
                      color: kNegativeTextColor,
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 14.5,),
          Divider(
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
            color: kBorderColor,
          ),
        ],
      ),
    );
  }
}

class _MembershipBenefits extends StatelessWidget {

  final List<String> benefitList;

  const _MembershipBenefits({Key key, this.benefitList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 51.5,
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "가입 혜택",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: benefitList.map((e) => _MembershipBenefitItem(
              description: e,
            ))?.toList(),
          )
        ],
      ),
    );
  }
}

class _MembershipBenefitItem extends StatelessWidget {
  final String description;

  const _MembershipBenefitItem({Key key, this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 45),
      height: 70,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              blurRadius: 6,
              offset: Offset(0, 2),
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            description,
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }
}



class _MembershipNotice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              "멤버십 가입 안내 사항",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
              child: RichText(
                  text: TextSpan(
                      children: [
                        TextSpan(
                            style: TextStyle(
                              color: kSelectableTextColor,
                              fontSize: 13,
                            ),
                            text:
                            "아래의 멤버십 가입 안내사항을 모두 동의하였으며 구매를 진행합니다.\n\n"
                                "- 한 셀럽당 한개의 멤버십만 가입할 수 있습니다.\n"
                                "- 멤버십은 매월 자동 갱신되며, 결제 예정일 전 24시간 이내에 결제가 진행됩니다. 결제일 전 24시간 이내에는 자동결제를 해지해도 결제될 수 있습니다.\n"
                                "- 결제 금액에는 VAT가 포함되어 있습니다.\n"
                                "- 결제시 연결된 iTunes 계정으로 요금이 부과됩니다.\n"
                                "- 멤버십 결제 시 다른 아이폰, 아이패드에서도 동일한 ID로 멤버십 이용이 가능합니다.\n"
                                "- 이용권 구독 내역 확인은 iTunes의 [계정설정 > 구독관리]에서 가능합니다.\n"
                                "- 결제 예정일 24시간 전에 자동갱신을 해지한 경우 다음 결제일부터 추가 결제되지 않으며, 이미 이용 중인 해당 월에 대한 환불은 불가합니다.\n"
                                "- 멤버십의 변경은 기존에 가입한 멤버십이 만료된 이후에 가능합니다.\n"
                                "- 결제 취소는 결제 후 7일 내 서비스 미 이용 시 가능하며, 결제 후 7일 경과 또는 서비스 이용 시에는 환불이 불가능합니다.\n"
                                "- 상품 가격 및 프로모션은 내부 정책으로 인해 변경될 수 있습니다.\n"
                        ),
                        TextSpan(
                            style: TextStyle(
                              color: kSelectableTextColor,
                              fontSize: 12,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            ),
                            text: "\nApple의 표준 최종 사용자 사용권 계약(EULA)  확인하기",
                            recognizer: TapGestureRecognizer()
                              ..onTap = (){
                                launch("https://www.apple.com/legal/internet-services/itunes/dev/stdeula/");
                              }
                        ),
                        TextSpan(
                            style: TextStyle(
                              color: kSelectableTextColor,
                              fontSize: 11,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.underline,
                            ),
                            text: "\n\n이용 약관 / 개인정보 처리방침 / 유료서비스 이용약관 / 서비스 운영 정책",
                            recognizer: TapGestureRecognizer()
                              ..onTap = (){
                                launch("https://media.maum.ai/rftap/terms");
                              }
                        ),
                      ]
                  )
              )
          )
        ],
      ),
    );
  }
}


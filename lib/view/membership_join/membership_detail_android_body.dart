
import 'dart:async';
import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_membership_detail_dto.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';
import 'package:rf_tap_fanseem/http/service/service_get_membership_detail.dart';
import 'package:rf_tap_fanseem/http/service/service_get_user_membership.dart';
import 'package:rf_tap_fanseem/http/service/service_new_membership.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../routes.dart';

class MembershipDetailAndroidBody extends StatefulWidget {

  const MembershipDetailAndroidBody({
    Key key,
  }) : super(key: key);

  @override
  _MembershipDetailAndroidBodyState createState() => _MembershipDetailAndroidBodyState();
}

class _MembershipDetailAndroidBodyState extends State<MembershipDetailAndroidBody> {
  final InAppPurchaseConnection _connection = InAppPurchaseConnection.instance;
  StreamSubscription<List<PurchaseDetails>> _subscription;
  List<PurchaseDetails> _purchases = [];
  HttpServiceGetMembershipDetailDto _membershipDetail = HttpServiceGetMembershipDetailDto();
  ProgressDialog _pr;
  @override
  void initState() {
    super.initState();
    _initStoreInfo();
  }

  void _initStoreInfo() async {

    Stream purchaseUpdated = InAppPurchaseConnection.instance.purchaseUpdatedStream;
    _subscription = purchaseUpdated.listen((event) {
      _purchases.addAll(event);
      _verifyPurchase(Provider.of<MembershipProvider>(context, listen: false).membershipId);
    },
      onDone: () => _subscription.cancel(),
      onError: (e) => print(e.toString()),
    );

//    final QueryPurchaseDetailsResponse purchaseResponse = await _connection.queryPastPurchases();
  }

  Future<void> _buyProduct(String productId) async {
    await _showPendingUI();
    BackButtonInterceptor.removeByName("popMember");
    ProductDetailsResponse response = await _connection.queryProductDetails([productId].toSet());
    final PurchaseParam purchaseParam = PurchaseParam(productDetails: response.productDetails[0]);
    await _connection.buyNonConsumable(purchaseParam: purchaseParam);
  }

  Future<bool> _verifyPurchase(String productId) async {
    PurchaseDetails purchase = _purchases.firstWhere((element) => element.productID == productId, orElse: () => null);

    if (purchase != null && purchase?.status == PurchaseStatus.purchased) {
      bool ok = await httpServiceNewMembership(
          context: context,
          purchaseToken: purchase.billingClientPurchase.purchaseToken,
          productId: productId
      );
      if (ok != null) {
        if(ok){
          final pending = !purchase.billingClientPurchase.isAcknowledged;
          if(pending) {
            await _connection.completePurchase(purchase);
          }
          Fluttertoast.showToast(msg: "멤버십 구매에 성공했습니다.", gravity: ToastGravity.TOP);
        }
        else{
          Fluttertoast.showToast(msg: "서버에서 오류가 발생했습니다. 관리자에게 문의해 주세요.", gravity: ToastGravity.TOP);
        }
        BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
          Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
          return true;
        }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
        await _hidePendingUI();
        Navigator.pop(context);
      }
      else {
        BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
          Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
          return true;
        }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
        Fluttertoast.showToast(msg: "서버에서 오류가 발생했습니다. 관리자에게 문의해 주세요.", gravity: ToastGravity.TOP);
        await _hidePendingUI();
        Navigator.pop(context);
      }
    }
    else if (purchase != null && purchase?.status == PurchaseStatus.error) {
      BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
        Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
        return true;
      }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
      Fluttertoast.showToast(msg: "서버에서 오류가 발생했습니다. 관리자에게 문의해 주세요.", gravity: ToastGravity.TOP);
      _hidePendingUI();
      Navigator.pop(context);
    }
    return true;
  }

  Future<void> _showPendingUI() async {
    await _pr.show();
  }

  Future<void> _hidePendingUI() async {
    await _pr.hide();
  }

  Future<bool> _getData() async {
    _membershipDetail = await httpServiceGetMembershipDetail(context: context, productId: Provider.of<MembershipProvider>(context, listen: false).membershipId);
    if (_membershipDetail == null) return false;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "멤버쉽 구매 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );
    return FutureBuilder<bool>(
        future: _getData(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          _MembershipInfo(membershipImageUrl: _membershipDetail.badgeImageUrl,),
                          _MembershipBenefits(benefitList: _membershipDetail.benefitDescriptionList == null ? [] : _membershipDetail.benefitDescriptionList,),
                          _MembershipNotice()
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: 70,
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6
                          )
                        ]
                    ),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: 10, horizontal: MediaQuery.of(context).size.width * 0.55 / 2),
                      child: GestureDetector(
                        onTap: () async {
                          int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
                          HttpServiceGetUserMembershipDto result = await httpServiceGetUserMembership(context: context, celebId: celebId);
                          if (result.tier == null) {
                            _buyProduct(Provider.of<MembershipProvider>(context, listen: false).membershipId);
                          }
                          else {
                            Fluttertoast.showToast(
                                msg: "이미 해당 셀럽의 멤버십을 이용중입니다. 한 셀럽당 최대 하나의 멤버십만 가입 가능합니다.");
                          }
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.55,
                          decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              border: Border.all(
                                color: kMainColor,
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(12)),
                              boxShadow: [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                    offset: Offset(0, 3),
                                    blurRadius: 6
                                )
                              ]
                          ),
                          child: Center(
                            child: Text(
                              "구입",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: kMainColor
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator(),);
          }
        }
    );
  }
}

class _MembershipInfo extends StatelessWidget {

  final String membershipImageUrl;

  const _MembershipInfo({Key key, this.membershipImageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 188,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  Provider.of<MembershipProvider>(context).membershipName,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ],
            ),
          ),
          Expanded(
              child: Container(
                child: CachedNetworkImage(
                  imageUrl: membershipImageUrl,
                  fit: BoxFit.fitHeight,
                  placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              )
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  "₩ ${Provider.of<MembershipProvider>(context).membershipCost} 원 / 월",
                  style: TextStyle(
                      color: kNegativeTextColor,
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 14.5,),
          Divider(
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
            color: kBorderColor,
          ),
        ],
      ),
    );
  }
}

class _MembershipBenefits extends StatelessWidget {

  final List<String> benefitList;

  const _MembershipBenefits({Key key, this.benefitList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 51.5,
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "가입 혜택",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                  ),
                )
              ],
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: benefitList.map((e) => _MembershipBenefitItem(
              description: e,
            ))?.toList(),
          )
        ],
      ),
    );
  }
}

class _MembershipBenefitItem extends StatelessWidget {
  final String description;

  const _MembershipBenefitItem({Key key, this.description}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 45),
      height: 70,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              blurRadius: 6,
              offset: Offset(0, 2),
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(
            description,
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }
}



class _MembershipNotice extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Text(
              "안내 사항",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 15),
            child: RichText(
              text: TextSpan(
                children: [
                  TextSpan(
//              "본 상품은 월 구독형 정기결제 상품입니다.\n"
//              "멤버십을 가입하면 현시점부터 1달동안 멤버십 혜택이 적용되며, 가입 직후의 첫 결제에 대한 환불은 불가능합니다."
//                  "멤버십을 가입하면 만료일까지 다른 멤버십으로 가입 및 변경이 불가능합니다."
//                  "멤버십을 변경하시려면 기존 멤버십을 해지하고, 만료일 이후에 새로운 멤버십으로 가입해주세요."
//                  "멤버십을 해지해도 기존 혜택은 유지됩니다.",
                  text : "- 멤버십은 자동결제 방식의 월 구독형 서비스입니다.\n"
                      "- 멤버십을 가입하면 현시점부터 1달동안 멤버십 혜택이 적용되며, 인앱결제 특성상 원칙적으로 멤버십(정기결제)의 환불이 불가합니다. (문의메일: fanmeet@rftap.net)\n"
                      "- 멤버십을 변경하시려면 기존 멤버십을 해지하고, 만료일 이후에 새로운 멤버십으로 직접 가입하셔야 합니다.\n"
                      "- 멤버십(정기구독)을 해지해도 결제된 멤버십 기간(만료일)까지는 기존 멤버십 혜택이 유지됩니다.\n"
                      " (예외사항)가입 후 7일 이내 서비스 이용 내역이 없거나, 팬밋 서비스에 큰 문제가 발생한 경우에 한하여 환불이 가능합니다.",
                  style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 13,
                  ),
                ),
                  TextSpan(
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 11,
                        fontWeight: FontWeight.bold,
                        decoration: TextDecoration.underline,
                      ),
                      text: "\n\n이용 약관 / 개인정보 처리방침 / 유료서비스 이용약관 / 서비스 운영 정책",
                      recognizer: TapGestureRecognizer()
                        ..onTap = (){
                          launch("https://media.maum.ai/rftap/terms");
                        }
                  ),
              ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

import 'dart:async';
import 'dart:math';

import 'package:audioplayers/audioplayers.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/providers/tts_storage_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';

class TtsCelebList extends StatelessWidget {
  Future<bool> _getData(BuildContext context) async {
    if(Provider.of<MyPageViewProvider>(context, listen: false).celebViewData.isEmpty)
      return await httpMyPageCelebList(context: context);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getData(context),
        builder: (context, snapshot) {
          if(snapshot.hasData)
            return Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: Provider.of<MyPageViewProvider>(context).celebViewData.map((e) => GestureDetector(
                    onTap: () {
                      Provider.of<TtsStorageProvider>(context, listen: false).setTtsStorageCelebInfo(
                          celebId: e.celebId,
                          celebName: e.name,
                          profileImgUrl: e.profileImgUrl
                      );
                      Navigator.pushNamed(context, kRouteTtsStorage);
                    },
                    behavior: HitTestBehavior.translucent,
                    child: _CelebTtsListItem(
                      profileImgUrl: e.profileImgUrl,
                      celebName: e.name,
                      greeting: e.greeting,
                    ),
                  )).toList(),
                ),
              ),
            );
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
    );
  }
}

class _CelebTtsListItem extends StatelessWidget {
  final String celebName;
  final String greeting;
  final String profileImgUrl;

  const _CelebTtsListItem({
    Key key,
    this.celebName = "",
    this.greeting = "",
    this.profileImgUrl = ""
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 6,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: CelebListInfoItem(
              celebName: celebName,
              greeting: greeting,
              profileImgUrl: profileImgUrl,
            ),
          ),
        ],
      ),
    );
  }
}

class TtsRequestedList extends StatelessWidget {
  Future<bool> _getData(BuildContext context) async{
    return await httpMyPageRequestedTtsList(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getData(context),
        builder: (context, snapshot) {
          if(snapshot.hasData)
            return Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
//                  Container(
//                    padding: const EdgeInsets.symmetric(horizontal: 30),
//                    height: 63,
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.end,
//                      children: <Widget>[
//                        // TODO filter
//                        //                    Container(
//                        //                      width: 97,
//                        //                      height: 27,
//                        //                      decoration: BoxDecoration(
//                        //                        color: Theme.of(context).backgroundColor,
//                        //                        borderRadius: BorderRadius.circular(14),
//                        //                        border: Border.all(
//                        //                            color: kUnSelectedColor,
//                        //                            width: 1
//                        //                        ),
//                        //                      ),
//                        //                      child: Center(
//                        //                        child: Row(
//                        //                          mainAxisSize: MainAxisSize.min,
//                        //                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                        //                          children: <Widget>[
//                        //                            Text(
//                        //                              "셀럽 전체",
//                        //                              style: TextStyle(
//                        //                                  color: kSelectableTextColor,
//                        //                                  fontSize: 12
//                        //                              ),
//                        //                            ),
//                        //                            SizedBox(
//                        //                              width: 7.2,
//                        //                            ),
//                        //                            Image(
//                        //                              image: AssetImage("assets/button/down_button.png"),
//                        //                              fit: BoxFit.fitWidth,
//                        //                              width: 8.9,
//                        //                              height: 5.3,
//                        //                              color: kUnSelectedColor,
//                        //                              colorBlendMode: BlendMode.dst,
//                        //                            )
//                        //                          ],
//                        //                        ),
//                        //                      ),
//                        //                    )
//                      ],
//                    ),
//                  ),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: Provider.of<MyPageViewProvider>(context).requestedTtsList.map((e) => GestureDetector(
                          onTap: () => null,
                          child: _RequestTtsListItem(
                            celebId: e.celebId,
                            celebName: e.celebName,
                            text: e.text,
                            status: e.status,
                            timestamp: e.timestamp == null ? "" : e.timestamp,
                            ttsUrl: e.url,
                          ),
                        )).toList(),
                      ),
                    ),
                  ),
                ],
              ),
            );
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
    );
  }
}

class _RequestTtsListItem extends StatefulWidget {
  final int ttsId;
  final int celebId;
  final String celebName;
  final String text;
  final String timestamp;
  final String ttsUrl;
  final RequestedTtsStatus status;

  const _RequestTtsListItem({
    Key key,
    this.ttsId,
    this.celebId,
    this.celebName = "",
    this.text = "",
    this.timestamp = "06/25",
    this.ttsUrl,
    this.status = RequestedTtsStatus.waiting
  }) : super(key: key);

  @override
  __RequestTtsListItemState createState() => __RequestTtsListItemState();
}

class __RequestTtsListItemState extends State<_RequestTtsListItem> {
  bool _expanded = false;

  bool _isAudioPlaying = false;
  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  @override
  void initState() {
    super.initState();
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      Fluttertoast.showToast(msg: "TTS 듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 13),
      height: !_expanded ? 74 : 238,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 4,
                offset: Offset(0, 2)
            )
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2 - 30,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.celebName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                          ),
                        ),
                        SizedBox(width: 11,),
                        Container(
                          width: 100,
                          child: Text(
                            widget.timestamp,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: kNavigationPressableColor
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 6,),
                    Text(
                      _expanded ? "" : widget.text,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 13,
                      ),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    _getText(widget.status),
                    style: TextStyle(
                      color: _getColor(widget.status),
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(width: 16,),
                  GestureDetector(
                    onTap: () => setState(() => _expanded = !_expanded),
                    child: Container(
                      width: 40,
                      height: 40,
                      child: Transform.rotate(
                        angle: 90 * pi/180,
                        child: Icon(
                          _expanded ? Icons.arrow_back_ios : Icons.arrow_forward_ios,
                          color: kBorderColor,
                          size: 25,
                        ),
                      )
                    ),
                  )
                ],
              )
            ],
          ),
          _expanded ? Container(
//            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 32),
            height: 163,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Text(
                      widget.text,
                      style: TextStyle(
                          fontSize: 13
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 31.6,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 5,),
                            widget.ttsUrl == null ? Container() :
                            IconButton(
                              onPressed: () async {
                                this._isAudioPlaying = true;
                                this._play(widget.ttsUrl);
                                Fluttertoast.showToast(msg: "다음이 재생됩니다: ${widget.text}", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                              },
                              icon: Icon(FontAwesomeIcons.play),
                              iconSize: 17,
                              color: Color.fromRGBO(96, 96, 96, 1),
                            ),
                            SizedBox(width: 5,),
                            widget.status == RequestedTtsStatus.ok ? IconButton(
                              onPressed: () async {
                                var status = await Permission.storage.status;
                                if (!status.isGranted) {
                                  await Permission.storage.request();
                                }
                                var dir = await DownloadsPathProvider.downloadsDirectory;
                                final taskId = await FlutterDownloader.enqueue(
                                  url: widget.ttsUrl,
                                  savedDir: dir.absolute.path,
                                  showNotification: true, // show download progress in status bar (for Android)
                                  openFileFromNotification: true, // click on notification to open downloaded file (for Android)
                                );
                              },
                              icon: Icon(FontAwesomeIcons.download),
                              iconSize: 17,
                              color: Color.fromRGBO(96, 96, 96, 1),
                            ) : Container()
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
//                            GestureDetector(
//                              onTap: () {
//
//                              },
//                              behavior: HitTestBehavior.translucent,
//                              child: Container(
//                                width: 38.4,
//                                height: 31.6,
//                                decoration: BoxDecoration(
//                                    color: kMainBackgroundColor,
//                                    borderRadius: BorderRadius.circular(21),
//                                    boxShadow: [
//                                      BoxShadow(
//                                        color: Color.fromRGBO(0, 0, 0, 0.16),
//                                        blurRadius: 6,
//                                        offset: const Offset(0, 3),
//                                      )
//                                    ]
//                                ),
//                                child: Center(
//                                  child: Text(
//                                    "삭제",
//                                    style: TextStyle(
//                                      fontSize: 13,
//                                      color: kNavigationPressableColor,
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }

  String _getText(RequestedTtsStatus status){
    switch(status){
      case RequestedTtsStatus.ok:
        return "승인";
      case RequestedTtsStatus.waiting:
        return "대기중";
      case RequestedTtsStatus.canceled:
        return "취소됨";
      default:
        throw UnsupportedError("unsupported!");
    }
  }

  Color _getColor(RequestedTtsStatus status){
    switch(status){
      case RequestedTtsStatus.ok:
        return Color.fromRGBO(41, 165, 31, 1);
      case RequestedTtsStatus.waiting:
        return Color.fromRGBO(255, 177, 118, 1);
      case RequestedTtsStatus.canceled:
        return kNegativeTextColor;
      default:
        throw UnsupportedError("unsupported!");
    }
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    _playerErrorSubscription.cancel();
    _playerCompletionSubscription.cancel();
    super.dispose();
  }
}

class RequestedTtsData {
  final int ttsId;
  final int celebId;
  final String celebName;
  final String text;
  final String timestamp;
  final String url;
  final RequestedTtsStatus status;

  const RequestedTtsData({
    this.ttsId,
    this.celebId,
    this.celebName,
    this.text,
    this.timestamp,
    this.url,
    this.status
  });

  @override
  String toString() {
    return 'RequestedTtsData{ttsId: $ttsId, celebId: $celebId, celebName: $celebName, text: $text, timestamp: $timestamp, url: $url, status: $status}';
  }
}

enum RequestedTtsStatus{
  ok,
  waiting,
  canceled
}
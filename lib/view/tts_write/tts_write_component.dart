

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_tts_write.dart';
import 'package:rf_tap_fanseem/providers/tts_write_provider.dart';


class TtsWriteCelebDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 57,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "셀럽 선택",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 200,
              child: SingleChildScrollView(
                child: _TtsWriteCelebRadios(
                  ttsCelebList: Provider.of<TtsWriteProvider>(context).ttsCelebList,
                  initialSelected: 0,
                ),
              ),
            ),
            Container(
              height: 67,
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "닫기",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      // TODO set scope
                      onTap: () async {
                        await httpTtsWriteView(Provider.of<TtsWriteProvider>(context, listen: false).selectedCeleb.celebId, context: context);
                        Navigator.pop(context);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _TtsWriteCelebRadios extends StatefulWidget {
  final List<CelebData> ttsCelebList;
  final int initialSelected;

  const _TtsWriteCelebRadios({
    Key key,
    this.ttsCelebList,
    this.initialSelected
  }) : super(key: key);

  @override
  __TtsWriteCelebRadiosState createState() => __TtsWriteCelebRadiosState();
}

class __TtsWriteCelebRadiosState extends State<_TtsWriteCelebRadios> {
  CelebData _selected;
  List<CelebData> _ttsCelebList;

  @override
  void initState() {
    super.initState();
    _ttsCelebList = widget.ttsCelebList;
    _selected = _ttsCelebList[widget.initialSelected];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: _ttsCelebList.asMap().entries.map((e) => Container(
          padding: const EdgeInsets.only(left: 38, right: 46),
          height: 58,
          decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  blurRadius: 6,
                  offset: Offset(0, 1),
                )
              ]
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                e.value.name,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                    fontWeight: FontWeight.bold
                ),
              ),
              Radio(
                value: e.value,
                groupValue: _selected,
                onChanged: (value) async {
                  Provider.of<TtsWriteProvider>(context, listen: false).selectedCeleb = value;
                  setState(() => _selected = value);
                },
              )
            ],
          ),
        )).toList(),
      ),
    );
  }
}

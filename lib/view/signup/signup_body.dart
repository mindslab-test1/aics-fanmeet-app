
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_data_provider.dart';

import 'component/signup_category_view.dart';
import 'component/signup_nickname_view.dart';
import 'component/signup_picture_view.dart';

class SignUpBody extends StatelessWidget {
  Widget _signupBody(BuildContext context){
    switch(Provider.of<SignUpDataProvider>(context).currentStep){
      case SignUpSteps.pictures:
        return SignUpPicture();
      case SignUpSteps.nickname:
        return SignUpNickname();
      case SignUpSteps.categories:
        return SignUpCategory();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _StatusBar(),
          Expanded(
            child: _signupBody(context)
          )
        ],
      ),
    );
  }
}

class _StatusBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.05),
            blurRadius: 11,
            offset: Offset(0,3)
          )
        ]
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              child: Center(
                child: Text(
                  Provider.of<SignUpDataProvider>(context).title,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.pictures ?
                    Theme.of(context).primaryColor : Colors.transparent
                ),
                child: Center(
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.pictures ?
                          Colors.transparent : kNavigationPressableColor
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: Container(
                  width: 13,
                  child: Image.asset("assets/icon/dots.png", fit: BoxFit.fitWidth,),
                ),
              ),
              Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.nickname ?
                    Theme.of(context).primaryColor : Colors.transparent
                ),
                child: Center(
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.nickname ?
                        Colors.transparent : kNavigationPressableColor
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                child: Container(
                  width: 13,
                  child: Image.asset("assets/icon/dots.png", fit: BoxFit.fitWidth,),
                ),
              ),
              Container(
                width: 20,
                height: 20,decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.categories ?
                  Theme.of(context).primaryColor : Colors.transparent
              ),
                child: Center(
                  child: Container(
                    width: 15,
                    height: 15,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Provider.of<SignUpDataProvider>(context).currentStep == SignUpSteps.categories ?
                        Colors.transparent : kNavigationPressableColor
                    ),
                  ),
                ),
              )
            ],
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Center(
                child: Text(
                  "가입 완료",
                  style: TextStyle(
                    color: kNavigationPressableColor,
                    fontSize: 10,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

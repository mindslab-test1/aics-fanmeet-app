
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/signup/signup_body.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_data_provider.dart';

import '../../colors.dart';

class SignUpRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
        var provider = Provider.of<SignUpDataProvider>(context, listen: false);
        switch(provider.currentStep){
          case SignUpSteps.pictures:
            Fluttertoast.showToast(msg: "회원가입을 취소하고 돌아갑니다.", gravity: ToastGravity.TOP);
            Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
            break;
          case SignUpSteps.nickname:
            provider.setStep(SignUpSteps.pictures);
            break;
          case SignUpSteps.categories:
            provider.setStep(SignUpSteps.nickname);
            break;
        }
        return true;
      }, ifNotYetIntercepted: true, zIndex: 100);
    });

    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: _SignUpAppbar(),
      ),
      body: SignUpBody(),
      backgroundColor: Theme.of(context).backgroundColor,
    );
  }
}

class _SignUpAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 30,),
                  GestureDetector(
                    onTap: () {
                      var provider = Provider.of<SignUpDataProvider>(context, listen: false);
                      switch(provider.currentStep){
                        case SignUpSteps.pictures:
                          Fluttertoast.showToast(msg: "회원가입을 취소하고 돌아갑니다.", gravity: ToastGravity.TOP);
                          Navigator.pop(context,);
                          break;
                        case SignUpSteps.nickname:
                          provider.setStep(SignUpSteps.pictures);
                          break;
                        case SignUpSteps.categories:
                          provider.setStep(SignUpSteps.nickname);
                          break;
                      }
                    },
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                "기본 정보 입력",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          )
        ],
      ),
    );
  }
}

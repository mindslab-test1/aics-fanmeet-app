
import 'package:flutter/cupertino.dart';

class SignUpDataProvider extends ChangeNotifier {
  String _profileImagePath;
  String _coverImagePath;
  String _nickname = "";
  List<int> _interests = [];

  SignUpSteps _currentStep = SignUpSteps.pictures;
  String _title = "사진 선택";

  SignUpSteps get currentStep => _currentStep;
  String get title => _title;

  void setStep(SignUpSteps step){
    _currentStep = step;
    switch(_currentStep){
      case SignUpSteps.pictures:
        _title = "사진 선택";
        break;
      case SignUpSteps.nickname:
        _title = "닉네임";
        break;
      case SignUpSteps.categories:
        _title = "관심 분야";
        break;
    }
    notifyListeners();
  }


  String get profileImagePath => _profileImagePath;
  String get coverImagePath => _coverImagePath;
  void setImages({String profileImagePath, String coverImagePath}) {
    _profileImagePath = profileImagePath;
    _coverImagePath = coverImagePath;
    notifyListeners();
  }

  String get nickname => _nickname;
  set nickname(String value) {
    _nickname = value;
    notifyListeners();
  }

  List<int> get interests => _interests;
  set interests(List<int> value) {
    _interests = value;
    notifyListeners();
  }

}

enum SignUpSteps {
  pictures,
  nickname,
  categories
}
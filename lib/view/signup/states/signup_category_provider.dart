
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';

class CategoryProvider with ChangeNotifier{
  Set<CategoryEnum> _selected = Set();

  Set<CategoryEnum> get selected => _selected;

  void push(CategoryEnum categoryEnum){
    _selected.add(categoryEnum);
    notifyListeners();
  }

  void pop(CategoryEnum categoryEnum){
    _selected.remove(categoryEnum);
    notifyListeners();
  }
}
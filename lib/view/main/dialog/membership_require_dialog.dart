
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/community/community_celeb_membership_list.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_item_vo.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';

class MembershipPrompt extends StatelessWidget {
  final String membershipName;
  final int accessLevel;

  const MembershipPrompt({Key key, this.membershipName, this.accessLevel}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
//        height: 200,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: const Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 28, right: 28, top: 24, bottom: 16),
              child: Column(
                children: [
                  CircleAvatar(
                  backgroundColor: kMainColor,
                  child: Icon(FontAwesomeIcons.exclamation, color: Colors.white, size: 19,),
                  radius: 16,),
                  SizedBox(height: 24,),
                  RichText(
                    text: TextSpan(
                      style: const TextStyle(
                        fontSize: 15,
                        color: kSelectableTextColor,
                      ),
                      children: <TextSpan>[
                        new TextSpan(text: "해당 피드는 멤버십 전용 콘텐츠입니다.\n"),
                        new TextSpan(text:
                          membershipName,
                          style: const TextStyle(color: kMainColor)
                        ),
                        new TextSpan(text: " 이상 부터 이용 가능합니다."),
                    ]
                  )),
                  SizedBox(height: 16,),
                  FlatButton(
                    padding: EdgeInsets.zero,
                    onPressed: () async {
                      await httpCommunityCelebMembershipList(context: context, celebId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId, userId: Provider.of<LoginVerifyProvider>(context,listen: false).userId);
                      HttpCommunityCelebMembershipListItemVo thisMembershipItem = Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element) => element.membershipTier == this.accessLevel);
                      Provider.of<MembershipProvider>(context, listen: false).setMembershipInfo(
                          thisMembershipItem.productId,
                          thisMembershipItem.membershipName,
                          thisMembershipItem.price
                      );
                      Navigator.pushNamed(context, kRouteMembershipJoin, arguments: true);
                    },
                    child: Container(
                      height: 52,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        border: Border.all(
                          color: kMainColor
                        ),
                      ),
                      child: Row(
                        children: [
                          Expanded(
                            child: Container(), flex: 1,
                          ),
                          Expanded(
                            child: Center(
                                child: Text(
                                    "멤버십 가입하기",
                                    style: const TextStyle(
                                      fontSize: 13,
                                      color: kMainColor,
                                      fontFamily: "Roboto"
                                    ),
                                )
                            ),
                            flex: 3,
                          ),
                          Expanded(
                            child: SvgPicture.asset("resources/images/svg/ic_go.svg"),
                            flex: 1,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
            ),
            Container(
              height: 60,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: GestureDetector(
//                        onTap: () => Navigator.pop(context),
//                        behavior: HitTestBehavior.translucent,
//                        child: Center(
//                          child: Text(
//                            "취소",
//                            style: TextStyle(
//                              fontSize: 17,
//                              color: Colors.black,
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                    VerticalDivider(
//                      width: 0.5,
//                    ),
                  Expanded(
                    flex: 1,
                    child: FlatButton(
                      padding: EdgeInsets.zero,
                      onPressed: () => Navigator.pop(context),
                      child: Center(
                        child: Text(
                          "확인",
                          style: TextStyle(
                              fontSize: 17,
                              color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

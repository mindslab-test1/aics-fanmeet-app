import 'dart:io';
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:substring_highlight/substring_highlight.dart';
import 'package:url_launcher/url_launcher.dart';


class NewestVersionDialog extends StatelessWidget{
    @override
    Widget build(BuildContext context) {
        return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(26),
            ),
            child: Container(
                width: MediaQuery.of(context).size.width - 28,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.circular(26),
                    boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.64),
                            blurRadius: 14
                        )
                    ]
                ),
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                        Container(
                            padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
                            child: Center(
                                child : Directionality(
                                    child: SubstringHighlight(
                                        text: '최신 버전 업데이트 후 이용 가능합니다. \n업데이트로 이동합니다.', term: '최신 버전 업데이트',
                                        textStyleHighlight:
                                        TextStyle(              // highlight style
                                            color: kNegativeTextColor,
                                        )
                                    ),
                                    textDirection: TextDirection.ltr)
                            ),
                        ),
                        Container(
                            height: 55,
                            decoration: BoxDecoration(
                                border: Border(
                                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                                ),
                            ),
                            child: Row(
                                children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                            onTap: () => exit(0),
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                                child: Text(
                                                   "종료",
                                                    style: TextStyle(
                                                        fontSize: 17,
                                                        color: kNavigationPressableColor,
                                                    ),
                                                ),
                                            ),
                                        ),
                                    ),
                                    VerticalDivider(
                                        width: 0.5,
                                    ),
                                    Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                            onTap: () async{
                                                const url = 'https://play.google.com/store/apps/details?id=ai.maum.rf_tap_fanseem';
                                                if (await canLaunch(url)) {
                                                    await launch(url);
                                                } else {
                                                    developer.log('Could not launch $url');
                                                }
                                            },
                                            behavior: HitTestBehavior.translucent,
                                            child: Center(
                                                child: Text(
                                                    "이동",
                                                    style: TextStyle(
                                                        fontSize: 17,
                                                        color: kNegativeTextColor
                                                    ),
                                                ),
                                            ),
                                        ),
                                    )
                                ],
                            ),
                        )
                    ],
                ),
            ),
        );
    }
}
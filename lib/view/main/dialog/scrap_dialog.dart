
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/scrap/scrap_add.dart';

import '../../../colors.dart';
import '../../../colors.dart';
import '../../../http/mypage/mypage_view.dart';
import '../../../providers/scrap_board_provider.dart';

class ScrapDialog extends StatefulWidget {
  const ScrapDialog({
    Key key,
  }) : super(key: key);

  @override
  _ScrapDialogState createState() => _ScrapDialogState();
}

class _ScrapDialogState extends State<ScrapDialog> {
  TextEditingController _editingController = TextEditingController();
  List<int> _selectedBoards;
  bool _addBoard;

  @override
  void initState() {
    super.initState();
    _selectedBoards = [];
    _addBoard = false;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 57,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "스크랩하기",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: kBorderColor,
              thickness: 0.5,
              height: 0,
            ),
            Container(
              height: 58,
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "새 스크랩 보드 추가",
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(width: 10,),
                    Center(
                      child: GestureDetector(
                        onTap: () => setState(() => _addBoard = !_addBoard),
                        behavior: HitTestBehavior.translucent,
                        child: Icon(
                          _addBoard ? Icons.cancel : Icons.add_circle,
                          color: _addBoard ? Colors.red : Colors.black,
                          size: 26,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            _addBoard ? Container(
              padding: const EdgeInsets.only(left: 30, right: 11.5),
              height: 58,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      textAlign: TextAlign.start,
                      controller: _editingController,
                      decoration: InputDecoration.collapsed(
                        hintText: "스크랩 보드 이름을 입력해주세요.",
                        hintStyle: TextStyle(
                          color: kUnSelectedColor,
                          fontSize: 14,
                        )
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  GestureDetector(
                    onTap: () async {
                      if(_editingController.text == null || _editingController.text == ""){
                        Fluttertoast.showToast(msg: "이름을 입력해 주세요!", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      } else if (_editingController.text.length > 24){
                        Fluttertoast.showToast(msg: "스크랩 보드는 24자 이하의 이름을 가져야 합니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      } else {
                        bool result = await httpScrapAddBoard(context: context, boardName: _editingController.text);
                        if(result) {
                          _getBoards();
                          setState(() {
                            _addBoard = false;
                          });
                        }
                        else Fluttertoast.showToast(msg: "스크랩 보드를 추가하는데 실패하였습니다...", gravity: ToastGravity.TOP);
                      }
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                      width: 38.4,
                      height: 31.6,
                      decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        borderRadius: BorderRadius.circular(21),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.16),
                            blurRadius: 6,
                            offset: Offset(0, 3)
                          )
                        ]
                      ),
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ) : Container(),
            Container(
              height: 258,
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    blurRadius: 6,
                    offset: Offset(0, 1)
                  )
                ]
              ),
              child: FutureBuilder(
                future: _getBoards(),
                builder: (context, snapshot) {
                  return SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: Provider.of<ScrapBoardProvider>(context, listen: false)
                          .boardList.isNotEmpty ?
                      Provider.of<ScrapBoardProvider>(context, listen: false)
                          .boardList.asMap().entries.map((e) => GestureDetector(
                            onTap: () => setState(() => _selectedBoards.contains(e.value.boardId)
                                ? _selectedBoards.remove(e.value.boardId) : _selectedBoards.add(e.value.boardId)),
                            behavior: HitTestBehavior.translucent,
                            child: _ScrapBoardItem.buildWith(
                              e.value,
                              selected: _selectedBoards.contains(e.value.boardId)
                      ),)).toList() : [Container(
                        height: 258,
                        child: Center(
                          child: Text(
                            "스크랩 보드를 추가해 보세요!",
                            maxLines: 2,
                          ),
                        ),
                      )],
                    ),
                  );
                }
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "닫기",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context, _selectedBoards);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Future<bool> _getBoards() async {
    return await httpMyPageScrapBoardList(context: context);
  }
}

class _ScrapBoardItem extends StatelessWidget {
  final bool selected;
  final int boardId;
  final String boardName;

  const _ScrapBoardItem({
    Key key,
    this.selected = false,
    this.boardId,
    this.boardName
  }) : super(key: key);

  static _ScrapBoardItem buildWith(ScrapBoardData data, {bool selected = false}){
    return _ScrapBoardItem(
      selected: selected,
      boardId: data.boardId,
      boardName: data.boardName,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 30, right: 15),
      height: 58,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 6,
                offset: Offset(0, 1)
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            "# $boardName",
            style: TextStyle(
              color: Colors.black,
              fontSize: 12,
            ),
          ),
          Icon(
            FontAwesomeIcons.solidCheckCircle,
            color: selected ? kFunctionButtonColor : kBorderColor,
          )
        ],
      ),
    );
  }
}

class ScrapBoardData {
  final int boardId;
  final String boardName;

  const ScrapBoardData({
    this.boardId,
    this.boardName
  });
}


import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/community/community_celeb_membership_list.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_dto.dart';
import 'package:rf_tap_fanseem/providers/content_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_view_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';

const kDialogTextStyle = TextStyle(
  fontFamily: 'Roboto',
  fontWeight: FontWeight.w700
);

const allSvgIcon = "resources/images/svg/ic_all.svg";

int accessLevel;

class ScopeDialog extends StatelessWidget {
  final int celebId;
  final int userId;
  final bool isEdit;
  const ScopeDialog({Key key, this.celebId, this.userId, this.isEdit}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.black.withOpacity(0.7),
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width - 28,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 57,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "공개범위 설정",
                      style: kDialogTextStyle.copyWith(fontSize: 20,),
                    )
                  ],
                ),
              ),
              Divider(thickness: 1, indent: 14, endIndent: 14, color: kProfileButtonColor, height: 1,),
              _MembershipRadios(celebId: celebId, userId: userId,),
              Divider(thickness: 1, indent: 14, endIndent: 14, color: kProfileButtonColor, height: 1,),
              Container(
                height: 67,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        // TODO set scope
                        onTap: (){
                          if(isEdit){
                            if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.contents) accessLevel = Provider.of<ContentEditDataProvider>(context, listen: false).tier;
                            if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.celeb) accessLevel = Provider.of<FeedEditDataProvider>(context, listen: false).accessLevel;
                          }else{
                            accessLevel = 0;
                          }
                          Navigator.pop(context, );
                        },
                        behavior: HitTestBehavior.translucent,
                        child: Center(
                          child: Text(
                            "취소",
                            style: kDialogTextStyle.copyWith(fontSize: 17),
                          ),
                        ),
                      ),
                    ),
                    VerticalDivider(thickness: 1, color: kProfileButtonColor,),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        // TODO set scope
                        onTap: (){
                          if(Provider.of<FeedWriteViewProvider>(context, listen: false).writeType == FeedWriteType.contents) Provider.of<ContentWriteDataProvider>(context, listen: false).tier = accessLevel;
                          else if(Provider.of<FeedWriteViewProvider>(context, listen: false).writeType == FeedWriteType.celeb) Provider.of<FeedWriteDataProvider>(context, listen: false).accessLevel = accessLevel;
                          else if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.contents) Provider.of<ContentEditDataProvider>(context, listen: false).tier = accessLevel;
                          else if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.celeb) Provider.of<FeedEditDataProvider>(context, listen: false).accessLevel = accessLevel;
                          Provider.of<ContentWriteDataProvider>(context, listen: false).tier = accessLevel;
                          Provider.of<FeedWriteDataProvider>(context, listen: false).accessLevel = accessLevel;
                          Navigator.pop(context, );
                        },
                        behavior: HitTestBehavior.translucent,
                        child: Center(
                          child: Text(
                            "완료",
                            style: kDialogTextStyle.copyWith(fontSize: 17, color: kMainColor),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _MembershipRadios extends StatefulWidget {
  final int celebId;
  final int userId;
  final bool isEdit;

  const _MembershipRadios({
    Key key,
    this.celebId,
    this.userId,
    this.isEdit
  }) : super(key: key);
  @override
  __MembershipRadiosState createState() => __MembershipRadiosState();
}
class __MembershipRadiosState extends State<_MembershipRadios> {
  int _selected;
  HttpCommunityCelebMembershipListDto _data;

  Future<bool> _getData() async {
    _data = await httpCommunityCelebMembershipList(context: context, celebId: widget.celebId, userId: widget.userId);
    if (_data != null) return true;
    return false;
  }

  Widget radioMenu({String membershipIcon, bool isPublic, String membershipName, int membershipIndex}){
    return Container(
      child: Container(
        color: Colors.white,
        child: Container(
          margin: const EdgeInsets.only(left: 30, right: 30),
          height: 58,
          decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              border: Border(
                  top: BorderSide(width: 1, color: isPublic?Colors.transparent:kProfileButtonColor)
              )
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              isPublic?SvgPicture.asset(allSvgIcon)
                  :CachedNetworkImage(
                imageUrl: membershipIcon,
                placeholder: (context, url) => Center(child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),)),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              SizedBox(width: 8,),
              Text.rich(
                  TextSpan(
                      children: <InlineSpan>[
                        TextSpan(text: membershipName, style: isPublic ? kDialogTextStyle.copyWith(fontSize: 13) : kDialogTextStyle.copyWith(color: kMainColor)),
                        isPublic?TextSpan(text: ""):TextSpan(text: " 멤버십 이상 열람 가능", style: kDialogTextStyle.copyWith(color: kSelectableTextColor))
                      ]
                  )
              ),
              Expanded(child: Container()),
              CustomRadioWidget(
                width: 24,
                height: 24,
                value: membershipIndex,
                groupValue: _selected,
                onChanged: (value) {
                  // if(Provider.of<FeedWriteViewProvider>(context, listen: false).writeType == FeedWriteType.contents) Provider.of<ContentWriteDataProvider>(context, listen: false).tier = membershipIndex;
                  // else if(Provider.of<FeedWriteViewProvider>(context, listen: false).writeType == FeedWriteType.celeb) Provider.of<FeedWriteDataProvider>(context, listen: false).accessLevel = membershipIndex;
                  // else if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.contents) Provider.of<ContentEditDataProvider>(context, listen: false).tier = membershipIndex;
                  // else if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.celeb) Provider.of<FeedEditDataProvider>(context, listen: false).accessLevel = membershipIndex;
                  // Provider.of<ContentWriteDataProvider>(context, listen: false).tier = membershipIndex;
                  // Provider.of<FeedWriteDataProvider>(context, listen: false).accessLevel = membershipIndex;
                  accessLevel = value;
                  setState(() => _selected = value);
                },
              )
            ],
          ),
        ),
      ),
    );
  }


  @override
  void initState() {
    super.initState();
    accessLevel==null?_selected = 0:_selected=accessLevel;
    if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.contents) _selected = Provider.of<ContentEditDataProvider>(context, listen: false).tier;
    if(Provider.of<FeedEditViewProvider>(context, listen: false).editType == FeedEditType.celeb) _selected = Provider.of<FeedEditDataProvider>(context, listen: false).accessLevel;
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getData(),
      builder: (context, snapshot){
        if(snapshot.hasData&&snapshot.data){
          return Container(
            child: Container(
              color: Color(0xFFF7F7F7),
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  radioMenu(membershipIcon: allSvgIcon, isPublic: true, membershipName: "전체 공개", membershipIndex: 0),
                  ..._data.membershipList.asMap().entries.map((e) => radioMenu(membershipIcon: e.value.imageUrl, isPublic: false, membershipName: e.value.membershipName, membershipIndex: e.key+1)).toList(),
                ],
              ),
            ),
          );
        }else{
          return Container(
            child : Column(
              children: [
                radioMenu(membershipIcon: allSvgIcon, isPublic: true, membershipName: "전체 공개", membershipIndex: 0),
              ],
            )
          );
        }
      },
    );
  }
}

class CustomRadioWidget<T> extends StatelessWidget {
  final T value;
  final T groupValue;
  final ValueChanged<T> onChanged;
  final double width;
  final double height;
  const CustomRadioWidget({Key key, this.value, this.groupValue, this.onChanged, this.width, this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: () {
          onChanged(this.value);
        },
        child: Container(
          height: this.height,
          width: this.width,
          decoration: ShapeDecoration(
            shape: CircleBorder(),
            color: Color(0xFFE1E1E1),
          ),
          child: Center(
            child: Container(
              height: this.height - 4,
              width: this.width - 4,
              decoration: ShapeDecoration(
                  shape: CircleBorder(),
                  color: Colors.white
              ),
              child: Center(
                child: Container(
                  height: this.height - 8,
                  width: this.width - 8,
                  decoration: ShapeDecoration(
                      shape: CircleBorder(),
                      color: value==groupValue?kMainColor:Colors.white
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_delete.dart';
import 'package:rf_tap_fanseem/http/content/content_delete.dart';
import 'package:rf_tap_fanseem/http/feed/feed_delete.dart';
import 'package:rf_tap_fanseem/providers/content_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/report_dialog.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_view_provider.dart';
class FeedReportActionsDialog extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
                showGeneralDialog(
                  context: context,
                  pageBuilder: (context, animation, secondAnimation) => ReportDialog(),
                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                  barrierDismissible: false,
                  transitionDuration: const Duration(milliseconds: 150),
                );
              },
              behavior: HitTestBehavior.translucent,
              child: Container(
                height: 57,
                child: Center(
                  child: Text(
                    "신고",
                    style: TextStyle(
                        color: kNegativeTextColor,
                        fontSize: 17,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              thickness: 0.5,
              height: 5,
            ),
            GestureDetector(
              onTap: () => Fluttertoast.showToast(msg: "준비중입니다...", gravity: ToastGravity.TOP),
              behavior: HitTestBehavior.translucent,
              child: Container(
                height: 57,
                child: Center(
                  child: Text(
                    "차단",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              thickness: 0.5,
              height: 5,
            ),
            Container(
              height: 57,
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                behavior: HitTestBehavior.translucent,
                child: Center(
                  child: Text(
                    "닫기",
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class FeedEditActionsDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FeedData _feedData = Provider.of<FeedDataProvider>(context, listen: false).feedData;
    return Dialog(
      insetPadding: const EdgeInsets.only(left: 47,right: 47),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(14),
            boxShadow: [
              BoxShadow(
                  color: const Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 30
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () async{
                if(_feedData.isContent) {
                  Provider.of<FeedEditViewProvider>(context, listen: false).editType = FeedEditType.contents;
                  Provider.of<ContentEditDataProvider>(context, listen: false).setContentEditData(
                    text: _feedData.text,
                    pictures: _feedData.imgUrl,
                    imageUrls: _feedData.imgUrl,
                    videoUrl: _feedData.videoUrl,
                    contentId: _feedData.feedId,
                    title: _feedData.title,
                    youtubeUrl: _feedData.youtubeUrl,
                    categoryId: _feedData.categoryId,
                    tier: _feedData.accessLevel
                  );
                }
                else if(_feedData.isUserFeed){
                  Provider.of<FeedEditViewProvider>(context, listen: false).editType = FeedEditType.general;
                  Provider.of<FeedEditDataProvider>(context, listen: false).setFeedEditData(
                    text: _feedData.text,
                    pictures: _feedData.imgUrl,
                    imageUrls: _feedData.imgUrl,
                    videoUrl: _feedData.videoUrl,
                    feedId: _feedData.feedId,
                    youtubeUrl: _feedData.youtubeUrl,
                  );
                }
                else{
                  Provider.of<FeedEditViewProvider>(context, listen: false).editType = FeedEditType.celeb;
                  Provider.of<FeedEditDataProvider>(context, listen: false).setFeedEditData(
                    text: _feedData.text,
                    pictures: _feedData.imgUrl,
                    imageUrls: _feedData.imgUrl,
                    videoUrl: _feedData.videoUrl,
                    feedId: _feedData.feedId,
                    youtubeUrl: _feedData.youtubeUrl,
                    accessLevel: _feedData.accessLevel,
                  );
                }
                if(_feedData.isContent){
                  await Navigator.pushNamed(context, kRouteEditContent).then((value){
                    Navigator.pop(context, true);
                  });
                }
                else {
                  await Navigator.pushNamed(context, kRouteEditFeed).then((value) {
                    Navigator.pop(context, true);
                  });
                }
              },
              behavior: HitTestBehavior.translucent,
              child: Container(
                padding: const EdgeInsets.only(top:16),
                height: 68,
                width: 320,
                child: Center(
                  child: Text(
                    "수정",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
              thickness: 1,
              height: 5,
            ),
            GestureDetector(
              onTap: () => showGeneralDialog(
                  context: context,
                  pageBuilder: (context, animation, secondAnimation) => _FeedDeleteDialog(),
                  barrierColor: const Color.fromRGBO(254, 254, 254, 0.01),
                  barrierDismissible: false,
                  transitionDuration: const Duration(milliseconds: 150)
              ).then((value) => value ? Navigator.pop(context, value) : null),
              behavior: HitTestBehavior.translucent,
              child: Container(
                padding: const EdgeInsets.only(bottom:16),
                height: 68,
                width: 320,
                child: Center(
                  child: Text(
                    "삭제",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _FeedDeleteDialog extends StatelessWidget {
  final FeedData feedData;

  const _FeedDeleteDialog({Key key, this.feedData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,

        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
              child: Center(
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        style: TextStyle(
                            fontSize: 15,
                            color: kSelectableTextColor
                        ),
                        children: [
                          TextSpan(
                              text: "커뮤니티에서 게시물이 "
                          ),
                          TextSpan(
                            text: "영구삭제",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor
                            ),
                          ),
                          TextSpan(
                            text: " 됩니다.\n다른 유저의 스크랩보드에서도 삭제됩니다.\n피드를 삭제하시겠습니까?",
                          )
                        ]
                    ),
                  )
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context, false),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "취소",
                          style: TextStyle(
                            fontSize: 17,
                            color: kUnSelectedColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        bool isUser = Provider.of<FeedDataProvider>(context, listen: false).isUserFeed;
                        bool isContent = Provider.of<FeedDataProvider>(context, listen: false).isContent;
                        bool isCeleb = !isUser && !isContent;
                        int feedId = Provider.of<FeedDataProvider>(context, listen: false).feedData.feedId;
                        isCeleb ? httpCelebFeedDelete(context: context, feed: feedId).then((value) {
                          if(value) {
                            Fluttertoast.showToast(msg: "글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            Navigator.pop(context, true);
                          } else Fluttertoast.showToast(msg: "글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        }) : isContent ? httpContentDelete(context: context, content: feedId).then((value) {
                          if(value) {
                            Fluttertoast.showToast(msg: "글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            Navigator.pop(context, true);
                          } else Fluttertoast.showToast(msg: "글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        }) : httpFeedDelete(context : context, feed: feedId).then((value) {
                          if(value) {
                            Fluttertoast.showToast(msg: "글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            Navigator.pop(context, true);
                          } else Fluttertoast.showToast(msg: "글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        });
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "삭제",
                          style: TextStyle(
                              fontSize: 17,
                              color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


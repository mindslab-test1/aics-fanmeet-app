import 'dart:async';
import 'dart:developer' as developer;
import 'package:audioplayers/audioplayers.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_message/celeb_tts_sample.dart';
import 'package:rf_tap_fanseem/http/celeb_message/celeb_write_message.dart';
import 'package:rf_tap_fanseem/routes.dart';

import '../../../colors.dart';

class TtsMessageDialog extends StatefulWidget {

  const TtsMessageDialog({
    Key key,
  }) : super(key: key);


  _TtsMessageDialogState createState() => _TtsMessageDialogState();
}


class _TtsMessageDialogState extends State <TtsMessageDialog>{
  TextEditingController _messageController = TextEditingController();

  String _ttsUrl;
  int _ttsId;

  bool _pressed = false;

  bool _isAudioPlaying = false;
  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;
  ProgressDialog _pr;


  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
      Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
      return true;
    }, zIndex: 11, ifNotYetIntercepted: true);
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      developer.log(event);
      Fluttertoast.showToast(msg: "TTS 듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    this._audioPlayer?.dispose();
    this._playerCompletionSubscription?.cancel();
    this._playerErrorSubscription?.cancel();
    super.dispose();
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "미리듣기 준비중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "TTS 메세지 보내기",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Container(
                height: 70,
                width: MediaQuery.of(context).size.width - 88,
                decoration: BoxDecoration(
                    border: Border.all(color: kBorderColor),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.16),
                          blurRadius: 6,
                          offset: Offset(0, 3)
                      )
                    ]
                ),
                child: TextField(
                  controller: _messageController,
                  maxLines: 50,
                  maxLength: 300,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    hintText: "메세지를 입력해주세요",
                    hintStyle: TextStyle(fontSize: 14.0, color: Color.fromRGBO(112, 112, 112, 1)),
                    border: InputBorder.none,
                  ),
                  cursorColor: Colors.black,
                ),
              ),
            ),
            SizedBox(height: 20,),
            Container(
                height: 50,
                child: Center(
                  child: GestureDetector(
                    onTap: () async {
                      await _pr.show();
                      await Future.delayed(Duration(milliseconds: 200));
                      if (this._isAudioPlaying) {
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "TTS 미리듣기 중 입니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      }
                      if (_messageController.text.length < 1) {
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "빈 문장은 재생할 수 없습니다.", gravity: ToastGravity.TOP);
                      } else {
                        this._isAudioPlaying = true;
                        CelebTtsSampleDto response = await httpCelebTtsSample(context: context, text: _messageController.text);
                        if (response.statusCode != 200) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "미리듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          this._isAudioPlaying = false;
                          return;
                        }
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "다음이 재생됩니다: ${_messageController.text}", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        setState(() {
                          _ttsUrl = response.url;
                          _ttsId = response.ttsId;
                        });
                        this._play(_ttsUrl);
                      }
                    },
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            FontAwesomeIcons.play,
                            size: 22,
                            color: Color.fromRGBO(96, 96, 96, 1),
                          ),
                          InkWell(
                            child: Text(
                              '미리듣기',
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.grey,
                              ),
                            ),
                          )
                        ]
                    ),
                  ),
                )
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "취소",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () async {
                        await _pr.show();
                        await Future.delayed(Duration(milliseconds: 300));
                        if (_ttsId == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "미리듣기를 한 뒤 전송할 수 있습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        if (_pressed) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "메세지를 보내는 중입니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        _pressed = true;
                        if (_messageController.text == null ||
                            _messageController.text.length == 0) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "텍스트를 입력해주세요", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        } else
                          httpCelebSendMessage(context: context, ttsId: _ttsId, isTts: true).then((value) async {
                            if (value) {
                              await _pr.hide();
                              _messageController.clear();
                              Fluttertoast.showToast(msg: "메세지가 전송 되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                              Navigator.pop(context, true);
                            } else {
                              await _pr.hide();
                              Fluttertoast.showToast(msg: "메세지 전송에 실패했습니다..", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            }
                          });
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "전송",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
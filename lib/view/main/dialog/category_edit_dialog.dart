
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_edit_category.dart';
import 'package:rf_tap_fanseem/http/content/content_edit_category.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';

class CategoryEditDialog extends StatefulWidget {
  final List<CategoryData> categoryData;
  final bool isCelebFeedCategory;

  const CategoryEditDialog({
    Key key,
    this.categoryData,
    this.isCelebFeedCategory
  }) : super(key: key);

  @override
  _CategoryEditDialogState createState() => _CategoryEditDialogState();
}

class _CategoryEditDialogState extends State<CategoryEditDialog> {
  bool _addCategory;
  bool _isCelebFeedCategory;
  List<CategoryData> _categoryData;
  List<CategoryData> _categoryDataOrigin;

  TextEditingController _textEditingController = TextEditingController();
  
  @override
  void initState() {
    super.initState();
    _categoryData = widget.categoryData;
    _categoryDataOrigin = [];
    _categoryDataOrigin.addAll(widget.categoryData);
    _isCelebFeedCategory = widget.isCelebFeedCategory;
    _addCategory = false;
  }
  
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 57,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "항목 편집",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            Divider(
              color: kBorderColor,
              thickness: 0.5,
              height: 0,
            ),
            _categoryData.length >= 5 ? Container() : Container(
              height: 58,
              child: Center(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      "새 항목 추가(${_categoryData.length-1}/4)",
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 13,
                      ),
                    ),
                    SizedBox(width: 10,),
                    Center(
                      child: GestureDetector(
                        onTap: () => setState(() => _addCategory = !_addCategory),
                        behavior: HitTestBehavior.translucent,
                        child: Icon(
                          _addCategory ? Icons.cancel : Icons.add_circle,
                          color: _addCategory ? Colors.red : Colors.black,
                          size: 26,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            _addCategory ? Container(
              padding: const EdgeInsets.only(left: 30, right: 11.5),
              height: 58,
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      controller: _textEditingController,
                      textAlign: TextAlign.start,
                      decoration: InputDecoration.collapsed(
                          hintText: "항목 이름을 입력해주세요.",
                          hintStyle: TextStyle(
                            color: kUnSelectedColor,
                            fontSize: 14,
                          )
                      ),
                    ),
                  ),
                  SizedBox(width: 10,),
                  GestureDetector(
                    onTap: () {
                      if (_textEditingController.text == null || _textEditingController.text == "") {
                        Fluttertoast.showToast(msg: "카테고리 이름을 설정해 주세요.", gravity: ToastGravity.TOP);
                        return;
                      }
                      setState(() {
                        _categoryData.add(CategoryData(
                          categoryName: _textEditingController.text,
                          categoryId: -1
                        ));
                        _addCategory = false;
                        _textEditingController.clear();
                      });
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                      width: 38.4,
                      height: 31.6,
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(21),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.16),
                                blurRadius: 6,
                                offset: Offset(0, 3)
                            )
                          ]
                      ),
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: 13,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ) : Container(),
            Container(
              height: 258,
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    blurRadius: 6,
                    offset: Offset(0, 1)
                  )
                ]
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: _categoryData.map((e) => _CategoryItem(
                    categoryId: e.categoryId,
                    categoryName: e.categoryName,
                    onTapRemove: () {
                      setState(() {
                        _categoryData.removeWhere(
                                (element) => (element.categoryId == e.categoryId && element.categoryName == e.categoryName)
                        );
                      });
                    },
                  )).toList(),
                ),
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context, false),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "닫기",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () async {
                        List<String> added = [];
                        List<int> removed = [];
                        for (var iter in _categoryData) {
                          bool contains = false;
                          for (var it in _categoryDataOrigin) {
                            if (iter.categoryName == it.categoryName && iter.categoryId == it.categoryId) contains = true;
                          }
                          if (!contains) {
                            added.add(iter.categoryName);
                          }
                        }
                        for (var iter in _categoryDataOrigin) {
                          bool contains = false;
                          for (var it in _categoryData) {
                            if (iter.categoryName == it.categoryName && iter.categoryId == it.categoryId) contains = true;
                          }
                          if (!contains) {
                            removed.add(iter.categoryId);
                          }
                        }
                        bool ok = await (_isCelebFeedCategory ?
                          httpCelebFeedEditCategory(context: context, added: added, removed: removed) :
                          httpContentEditCategory(context: context, added: added, removed: removed)
                        );
                        if (!ok) {
                          Fluttertoast.showToast(msg: "카테고리 수정에 실패했습니다.", gravity: ToastGravity.TOP);
                          return;
                        }
                        Navigator.pop(context, true);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _CategoryItem extends StatelessWidget {
  final int categoryId;
  final String categoryName;
  final Function onTapRemove;

  const _CategoryItem({
    Key key,
    this.categoryId,
    this.categoryName,
    this.onTapRemove = dummyFunction
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 30, right: 15),
      height: 60,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 6,
                offset: Offset(0, 1)
            )
          ]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            categoryName,
            style: TextStyle(
              color: Colors.black,
              fontSize: 12,
            ),
          ),
          categoryId == 0 ? Container() : GestureDetector(
            onTap: onTapRemove,
            behavior: HitTestBehavior.translucent,
            child: Icon(
              Icons.cancel,
              color: kNegativeTextColor,
              size: 23,
            ),
          )
        ],
      ),
    );
  }
}

class CategoryData {
  final int categoryId;
  final String categoryName;

  const CategoryData({this.categoryId, this.categoryName});
}
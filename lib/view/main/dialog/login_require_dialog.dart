
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';

import '../../../routes.dart';

class LoginPrompt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,

        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(26),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.64),
              blurRadius: 14
            )
          ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
              child: Center(
                child: RichText(
                  textAlign: TextAlign.left,
                  text: TextSpan(
                    style: TextStyle(
                      fontSize: 15,
                      color: kSelectableTextColor
                    ),
                    children: [
                      TextSpan(
                        text: "로그인",
                        style: TextStyle(
                          color: Theme.of(context).primaryColor
                        ),
                      ),
                      TextSpan(
                        text: "이 필요한 서비스 입니다.\n지금 바로 로그인창으로 이동해볼까요?",
                      )
                    ]
                  ),
                )
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "취소",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        String routeName = Platform.isIOS ? kRouteIosLogin : kRouteLogin;
                        Navigator.pushNamed(context, routeName).then((value) => Navigator.pop(context));
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "이동",
                          style: TextStyle(
                            fontSize: 17,
                            color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:io';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/main.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_body.dart';
import 'package:rf_tap_fanseem/view/main/body/notice/notice_parting_body.dart';
import 'package:rf_tap_fanseem/view/main/body/search/search_body.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

import '../../providers/sub_view_controller_provider.dart';
import 'body/mypage/my_page_sub_routes.dart';
import 'body/mypage/mypage_parting_body.dart';
import 'body/mypage_celeb/my_page_celeb_body.dart';
import 'component/app_bar.dart';
import 'component/navigation_bar.dart';

class MainRoute extends StatelessWidget {
  final _bodyWidgets = [
    HomeBody(),
   NoticePartingBody(),
    // if(Platform.isAndroid) _TempGiftView(),
    MyPagePartingBody(),
//    MyPageBody()
//    MyPageCelebBody()
  ];
  DateTime _lastPressed;

  @override
  Widget build(BuildContext context) {
    final mainNavProv = Provider.of<MainNavigationProvider>(context);

    PageController pageController = PageController(
      initialPage: mainNavProv.currentIndex,

    );
    mainNavProv.pageController = pageController;
    return WillPopScope(
      onWillPop: () {
        if(mainNavProv.currentIndex != 0){
          mainNavProv.navigateRoot(0, context);
          pageController.jumpToPage(0);
        } else {
          if(_lastPressed == null){
            _lastPressed = DateTime.now();
            Fluttertoast.showToast(msg: "한 번 더 탭하면 앱이 종료됩니다.", gravity: ToastGravity.TOP);
          } else if (_lastPressed.difference(DateTime.now()).abs() < Duration(seconds: 1)){
            SystemChannels.platform.invokeMethod('SystemNavigator.pop');
          } else {
            _lastPressed = DateTime.now();
            Fluttertoast.showToast(msg: "한 번 더 탭하면 앱이 종료됩니다.", gravity: ToastGravity.TOP);
          }
        }

        return Future.value(false);
      },
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          titleSpacing: 0.0,
          automaticallyImplyLeading: false,
          elevation: 0,
          backgroundColor: kAppBarMainColor,
          title: MainRouteAppBar(),
        ),
        body: SafeArea(
          child: mainNavProv.searchState ?
          SearchBody() :
          PageView(
            controller: pageController,
            onPageChanged: (index) {
              if(!mainNavProv.navButtonHit)
                mainNavProv.navigateRoot(index, context);
            },
            children: this._bodyWidgets,
            physics: mainNavProv.mainViewScrollPhysics,
          )
        ),
        bottomNavigationBar: MainBotNavigationBar(pageController: pageController,),
        floatingActionButton: mainNavProv.floatingActionButton,
      ),
    );
  }
}

class _TempAlertView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: RaisedButton(
          child: Text("alert test button"),
          onPressed: () => null,
        ),
      ),
    );
  }
}

class _TempGiftView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Container(
            width: 94,
            height: 94,
            child: Image.asset("assets/icon/preparing.png", fit: BoxFit.cover),
          ),
          SizedBox(
            height: 15.4,
          ),
          Text(
            "서비스 준비중 입니다.\n조금만 기다려 주세요:-)",
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 13,
            ),
          ),
        ],
      ),
    );
  }
}

class _TempProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: RaisedButton(
          child: Text("profile test button"),
          onPressed: toDo,
        ),
      ),
    );
  }
}

import 'dart:convert';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:http/http.dart' as http;
import 'dart:developer' as developer;
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_home_response_dto.dart';
import 'package:rf_tap_fanseem/http/home/guest_home.dart';
import 'package:rf_tap_fanseem/http/home/login_home.dart';
import 'package:rf_tap_fanseem/main.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/util/firebase_listener.dart';
import 'package:rf_tap_fanseem/view/main/body/home/communitiy/home_community_body.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/home/subscribe/home_subscribes_body.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/view/main/dialog/login_require_dialog.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/view/main/dialog/newest_version_dialog.dart';

class HomeBody extends StatefulWidget {
  @override
  _HomeBodyState createState() => _HomeBodyState();
}

class _HomeBodyState extends State<HomeBody> {
  final PageController _pageController = PageController(
      keepPage: false
  );

  @override
  void initState(){
    super.initState();
    _checkVersion(context);
    WidgetsBinding.instance.addPostFrameCallback((_){
          if(FirebaseListener.isLaunch){
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail, arguments: FirebaseListener.arguments);
            FirebaseListener.isLaunch=false;
          }
    });
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<SubViewControllerProvider>(context, listen: false).add(
          HomeSubRoutes.homeRoot, _pageController
      );
    });

    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index){
        switch(HomeSubPages.values.elementAt(index)){
          case HomeSubPages.root:
            return HomeBodyView();
          case HomeSubPages.subscribing:
            return HomeSubsCelebBody();
          case HomeSubPages.community:
            return HomeCommunityBody(previousPage: "home",);
          default:
            throw UnsupportedError("unsupported view page!");
        }
      },
      itemCount: HomeSubPages.values.length,
    );
  }
}


class HomeBodyView extends StatelessWidget {

  HttpHomeResponseDto _data;
  bool _login;

  Future<bool> _getData(BuildContext context) async {
//    Map userMap = (await SharedPreferences.getInstance()).get("fanmeet_user_json");
//    print(userMap);

    _login = Provider.of<LoginVerifyProvider>(context).isLogin;
    _data = await (_login ? httpLoginHome(context: context) : httpGuestHome(context: context));
    return _data != null;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      MainNavigationProvider provider = Provider.of<MainNavigationProvider>(context, listen: false);
      provider.setAll(
          title: "홈",
          leftButton: BarLeftButton.login,
//          rightButton: BarRightButton.search,
          rightButton: BarRightButton.none,
          leftFunction: navigateToLogin(context),
//          rightFunction: () => provider.searchState = true,
          rightFunction: dummyFunction
      );
    });

    return FutureBuilder<bool>(
      future: _getData(context),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data == true) {
          return Container(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _HomeSubscribes(
                    login: _login,
                    celebs: _data?.subscribingCelebList?.map((e) => UserAvatarData(
                      userId: e.id,
                      userName: e.name,
                      imgUrl: e.profileImageUrl,
                      badgeUrl: e.badgeUrl
                    ))?.toList()
                ),
//            _HomeRecentFeeds(),
                Expanded(
                    child: HomeFeedCollection(
                        initialData: _data?.recentActivityList?.map((e) => FeedData(
                          userId: e.userId,
                          feedId: e.elementId,
                          userName: e.name,
                          profileImg: e.profileImageUrl,
                          postTimestamp: e.timestamp,
                          text: e.elementText,
                          imgUrl: [e.elementImageUrl],
                          isUserFeed: false,
                          isContent: (e.elementType == "content" ? true : false),
                        ))?.toList()
                    )
                ),
              ],
            ),
          );
        } else {
          return Container(
            color: Colors.white,
            child: Center(child: CircularProgressIndicator()),
          );
        }
      }
    );
  }
}

class _HomeSubscribes extends StatelessWidget {
  final bool login;
  final List<UserAvatarData> celebs;

  const _HomeSubscribes({
    Key key,
    this.login = false,
    this.celebs = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: !(!login || celebs.isEmpty) ? 166 : 90,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.05),
            blurRadius: 11,
            offset: Offset(0, 3),
          )
        ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 12,
          ),
          // Title and Button
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  "구독중",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
                GestureDetector(
                  onTap: login ? () {
                    Provider.of<SubViewControllerProvider>(context, listen: false)
                        .get(HomeSubRoutes.homeRoot)
                        .jumpToPage(HomeSubPages.subscribing.index);
                  }
                      : () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => LoginPrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),
                  ),
                  child: !login || celebs.isEmpty ? Image.asset(
                    "assets/button/all_button.png",
                    width: 63,
                    height: 23,
                    fit: BoxFit.cover,
                  ) : Image.asset(
                    "assets/button/more_button.png",
                    width: 63,
                    height: 23,
                    fit: BoxFit.cover,
                  ),
                )
              ],
            ),
          ),
          !(!login || celebs.isEmpty) ? Container(
            padding: EdgeInsets.only(top: 10),
            alignment: Alignment.centerLeft,
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: celebs.asMap().entries.map((e) {
                  return GestureDetector(
                    onTap: () {
                      Provider.of<MainNavigationProvider>(context, listen: false)
                          .setAll(
                            title: e.value.userName,
                            leftButton: BarLeftButton.back,
                            rightButton: BarRightButton.none,
                            rightFunction: dummyFunction,
                      );
                      Provider.of<SelectedCelebProvider>(context, listen: false).celebId = e.value.userId;
                      Provider.of<SelectedCelebProvider>(context, listen: false).celebName = e.value.userName;
                      Provider.of<SubViewControllerProvider>(context, listen: false)
                        .get(HomeSubRoutes.homeRoot)
                        .jumpToPage(HomeSubPages.community.index);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(top: 4, left: e.key == 0 ? 30 : 9.5, right: 9.5),
                      child: SubscribingCelebIcons.buildWith(e.value),
                    ),
                  );
                }).toList(),
              ),
            ),
          ) : Center(
            child: Text(
              "나만의 셀럽을 찾아 구독해보세요!",
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 14
              ),
            ),
          )
        ],
      )
    );
  }
}

bool Function(bool, RouteInfo) homeRootBackInterceptor(BuildContext context){
  return (pop, info) {
    if(info.currentRoute(context).settings.name != "/home") return false;
    PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false).get(HomeSubRoutes.homeRoot);
    // ignore: invalid_use_of_protected_member
    if(parentController.positions.isEmpty) return false;
    else if(parentController.page != 0.0){
      parentController.jumpToPage(0);
      return true;
    } else {
      return false;
    }
  };
}

VoidCallback homeLeftBackFunction(BuildContext context){
  return () => Provider.of<SubViewControllerProvider>(context, listen: false).get(HomeSubRoutes.homeRoot).jumpToPage(0);
}

enum HomeSubPages{
  root,
  subscribing,
  community
}

int _versionComp(String currentVersion, String recentVersion){
  print(currentVersion);
  print(recentVersion);
  if(recentVersion == null) return null;
  List<String> currentVersionSplit = currentVersion.split(".");
  List<int> currentVersionParsed = [];
  for(String versionString in currentVersionSplit){
    currentVersionParsed.add(int.tryParse(versionString));
  }
  if(currentVersionParsed.contains(null)){
    developer.log("current version parse failed");
    return null;
  }
  List<String> recentVersionSplit = recentVersion.split(".");
  List<int> recentVersionParsed = [];
  for(String versionString in recentVersionSplit){
    recentVersionParsed.add(int.tryParse(versionString));
  }
  if(recentVersionParsed.contains(null)){
    developer.log("recent version parse failed");
    return null;
  }

  for(int i = 0; i < 3; i++){
    if (currentVersionParsed[i] < recentVersionParsed[i]) return -1;
  }
  return 0;
}

_checkVersion(BuildContext context) async {
  final PackageInfo packageInfo = await PackageInfo.fromPlatform();
  final String currentVersion = packageInfo.version;
  await http.get("https://aics-stg/maum.ai/rftap/misc/version/check").then((response) {
    var serverVersion = json.decode(response.body);
    final String recentVersion = serverVersion["version"].toString();
    final int versionComp = _versionComp(currentVersion, recentVersion);
    if (response.statusCode == 200) {
      if (versionComp < 0) {
        showGeneralDialog(
          context: context,
          pageBuilder: (context, animation, secondAnimation){
            return WillPopScope(
              onWillPop: (){},
              child: NewestVersionDialog()
            );
          },
          barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 150),
        );
        developer.log("버전 업데이트가 필요합니다.");
      } else if (versionComp == 0) {
        developer.log("서버 버전과 기기 버전 정보랑 같습니다.");
      } else if (versionComp == null) {
        developer.log("서버 정보 비교에 실패했습니다.");
      }
//      else if (recentVersion < currentVersion) {
//        showGeneralDialog(
//          context: context,
//          pageBuilder: (context, animation, secondAnimation) => NewestVersionDialog(),
//          barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
//          barrierDismissible: false,
//          transitionDuration: const Duration(milliseconds: 150),
//        );
//        developer.log("버전 업데이트가 필요합니다.");
//      }
    } else {
      developer.log("서버 응답에 오류가 있습니다.");
    }
  });
}

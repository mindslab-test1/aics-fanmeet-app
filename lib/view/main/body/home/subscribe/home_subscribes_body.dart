
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_celeb_info_vo.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_home_celeb_list_dto.dart';
import 'package:rf_tap_fanseem/http/home/home_celeb_list.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_body.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

import '../../../../../colors.dart';

class HomeSubsCelebBody extends StatefulWidget {
  @override
  _HomeSubsCelebBodyState createState() => _HomeSubsCelebBodyState();
}

class _HomeSubsCelebBodyState extends State<HomeSubsCelebBody> {
  List<UserAvatarData> subscribingCelebList = List();
  List<UserAvatarData> allCelebList = List();

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(homeRootBackInterceptor(context), ifNotYetIntercepted: true);
  }
  @override
  void dispose() {
    BackButtonInterceptor.remove(homeRootBackInterceptor(context));
    super.dispose();
  }
  Future<bool> _getData(BuildContext context) async {
    HttpHomeCelebListDto _data = await httpHomeCelebList(context: context);

    subscribingCelebList = List();
    for (HttpCelebInfoVo iter in _data.subscribingCelebList) {
      subscribingCelebList.add(UserAvatarData(
        userId: iter.id,
        userName: iter.name,
        imgUrl: iter.profileImageUrl,
        badgeUrl: iter.badgeUrl
      ));
    }

    allCelebList = List();
    for (HttpCelebInfoVo iter in _data.allCelebList) {
      allCelebList.add(UserAvatarData(
        userId: iter.id,
        userName: iter.name,
        imgUrl: iter.profileImageUrl,
        badgeUrl: iter.badgeUrl
      ));
    }
    return _data != null;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<MainNavigationProvider>(context, listen: false).setAll(
          title: "셀럽 리스트",
          leftButton: BarLeftButton.back,
          rightButton: BarRightButton.none,
          leftFunction: homeLeftBackFunction(context),
          rightFunction: dummyFunction
      );
    });

    return FutureBuilder<bool>(
        future: _getData(context),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return Container(
              child: DefaultTabController(
                initialIndex: 0,
                length: 2,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: double.infinity,
                      height: 51,
                      child: TabBar(
                        indicatorColor: Theme
                            .of(context)
                            .primaryColor,
                        labelColor: Theme
                            .of(context)
                            .primaryColor,
                        labelStyle: TextStyle(
                            color: Theme
                                .of(context)
                                .primaryColor,
                            fontSize: 17,
                            fontWeight: FontWeight.bold
                        ),
                        unselectedLabelColor: kNavigationPressableColor,
                        unselectedLabelStyle: TextStyle(
                            fontSize: 15
                        ),
                        tabs: <Widget>[
                          Tab(
                            child: Text("구독중"),
                          ),
                          Tab(
                            child: Text("전체목록"),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                        child: TabBarView(
                          children: <Widget>[
                            _SubscribingCelebs(subscribes: subscribingCelebList,),
                            _SubscribingCelebs(subscribes: allCelebList,)
                          ],
                        )
                    )
                  ],
                ),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator(),);
          }
        }
    );
  }

}

class _SubscribingCelebs extends StatelessWidget {
  final List<UserAvatarData> subscribes;

  const _SubscribingCelebs({Key key, this.subscribes = const []}) : super(key: key);

  Function _onItemTap(BuildContext context, int userId, String userName) {
    Provider.of<SelectedCelebProvider>(context, listen: false)
      ..celebId = userId
      ..celebName = userName;
    Provider.of<MainNavigationProvider>(context, listen: false).title = userName;
    Provider.of<SubViewControllerProvider>(context, listen: false)
        .get(HomeSubRoutes.homeRoot)
        .jumpToPage(HomeSubPages.community.index);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: subscribes.isEmpty ? Center(
        child: Text("아직 구독중인 셀럽이 없습니다..."),
      ) : SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: subscribes.map((e) => GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){_onItemTap(context, e.userId, e.userName);},
              child: UserListItem(
                userName: e.userName,
                imgUrl: e.imgUrl,
                badgeUrl: e.badgeUrl,
                onIconTap: (){
                  _onItemTap(context, e.userId, e.userName);
                },)
          )).toList(),
        ),
      ),
    );
  }
}


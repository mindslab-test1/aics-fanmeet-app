
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_list.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_listcategory.dart';
import 'package:rf_tap_fanseem/http/community/community_celeb_membership_list.dart';
import 'package:rf_tap_fanseem/http/community/community_header.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_dto.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_header_dto.dart';
import 'package:rf_tap_fanseem/http/content/content_list.dart';
import 'package:rf_tap_fanseem/http/content/content_listcategory.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_list_dto.dart';
import 'package:rf_tap_fanseem/http/feed/feed_list.dart';
import 'package:rf_tap_fanseem/http/feed/feed_listcategory.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_subscribe.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_unsubscribe.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_view.dart';
import 'package:rf_tap_fanseem/http/profile/dto/http_profile_view_dto.dart';
import 'package:rf_tap_fanseem/providers/celeb_profile_provider.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/providers/tts_write_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/body/home/component/feed_tab_view.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_body.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/category_edit_dialog.dart';
import 'package:rf_tap_fanseem/view/style/text/default.dart';
import 'dart:io';
import '../../../../../providers/selected_celeb_provider.dart';
import '../../../../messaging/states/messaging_provider.dart';
import '../../mypage/my_page_body.dart';
import '../../mypage/subscribe/subscribers_body.dart';

class HomeCommunityBody extends StatefulWidget {
  final String previousPage;
  const HomeCommunityBody({this.previousPage}) : super();

  @override
  _HomeCommunityBodyState createState() => _HomeCommunityBodyState();
}

class _HomeCommunityBodyState extends State<HomeCommunityBody> {

  HttpCommunityHeaderDto _data;
  HttpProfileViewDto _profileData;

  bool _isUserFeed = true;
  bool _isContent = false;
  bool _loaded = false;
  int _selected = 0;
  List<CategoryData> _categories;
  String _lastCreated;
  List<FeedData> _feedData = [];
  List<ContentFeedData> _contentFeedData = [];
  List<Widget> categoryChildren = [];
  HttpCommunityCelebMembershipListDto _membershipData;

  Future<bool> _getData(int celebId) async {
    _data = await httpCommunityHeader(celebId: celebId);
    bool ok = await _getFeedTabViewData();
    _profileData = await httpCelebProfileView(context: context, userId: celebId);
    _membershipData = await httpCommunityCelebMembershipList(context: context, celebId: celebId, userId: Provider.of<LoginVerifyProvider>(context,listen: false).userId);
    if(_membershipData != null){
      Provider.of<MembershipProvider>(context, listen: false).membershipList = _membershipData.membershipList;
    }
    if (_profileData == null) return false;
    Provider.of<CelebProfileProvider>(context, listen: false).celeb = _profileData;
    return _data != null && _profileData !=null && ok == true;
  }

  void _getMoreFeeds(bool loading, Function setLoading) async {
    if (loading) return;
    setLoading(true);
    await Future.delayed(Duration(seconds: 1));
    int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
    HttpFeedListDto data;
    if (_isContent) {
      data = await httpContentList(context: context, celebId: celebId, howManyFeed: 10, lastCreated: _lastCreated, categoryId: _categories[_selected].categoryId);
    } else if (_isUserFeed) {
      data = await httpFeedList(context: context, celebId: celebId, howManyFeed: 10, lastCreated: _lastCreated, categoryId: _categories[_selected].categoryId);
    } else {
      data = await httpCelebFeedList(context: context, celebId: celebId, howManyFeed: 10, lastCreated: _lastCreated, categoryId: _categories[_selected].categoryId);
    }
    _lastCreated = data.tailCreated;
    List<FeedData> newData = [];
    List<ContentFeedData> newContentData = [];
    if(_isContent){
      for (HttpFeedItemVo iter in data.feeds){
        newContentData.add(ContentFeedData(
            feedId: iter.feedId,
            userId: iter.feedOwnerId,
            userName: iter.feedOwnerName,
            profileImg: iter.feedOwnerProfileImageUrl,
            postTimestamp: iter.updatedTime,
            category: iter.categoryName,
            imgUrl: iter.pictureUrls,
            videoUrl: iter.videoUrl,
            youtubeUrl: iter.youtubeUrl,
            membership: iter.badgeImageUrl,
            text: iter.text,
            scrapCount: iter.scrapCount,
            isScrap: iter.scraped,
            likeCount: iter.likeCount,
            isLike: iter.liked,
            commentCount: iter.commentCount,
            canView: !iter.restricted,
            title: iter.title,
            viewCount: iter.viewCount,
            isEdited: iter.isEdited,
            accessLevel: iter.accessLevel
        ));
      }
      _contentFeedData.addAll(newContentData);
    }
    else{
      for (HttpFeedItemVo iter in data.feeds) {
        newData.add(FeedData(
            feedId: iter.feedId,
            userId: iter.feedOwnerId,
            userName: iter.feedOwnerName,
            profileImg: iter.feedOwnerProfileImageUrl,
            membershipBadgeUrl: iter.badgeImageUrl,
            postTimestamp: iter.updatedTime,
            category: iter.categoryName,
            imgUrl: iter.pictureUrls,
            videoUrl: iter.videoUrl,
            youtubeUrl: iter.youtubeUrl,
            text: iter.text,
            scrapCount: iter.scrapCount,
            isScrap: iter.scraped,
            likeCount: iter.likeCount,
            isLike: iter.liked,
            commentCount: iter.commentCount,
            canView: !iter.restricted,
            isEdited: iter.isEdited,
            accessLevel: iter.accessLevel
        ));
      }
      _feedData.addAll(newData);
    }
    setLoading(false);
  }

  void _refreshFeeds(bool loading, Function setLoading) async {
    if (loading) return;
    _loaded = false;
    setLoading(true);
    await Future.delayed(Duration(seconds: 1));
    await _getFeedTabViewData();
    setLoading(false);
  }

  Future<void> _setCategory() async {
    _categories = List();
    _categories.add(CategoryData(categoryId: 0, categoryName: "전체"));
    int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;

    if (_isContent) {
      _categories.addAll(await httpContentListcategory(celebId).then(
              (value) => value.map(
                  (e) => CategoryData(
                  categoryId: e.id,
                  categoryName: e.name
              )
          ).toList()
      ));
    } else if (_isUserFeed) {
      _categories.addAll(await httpFeedListcategory().then(
              (value) => value.map(
                  (e) => CategoryData(
                  categoryId: e.id,
                  categoryName: e.name
              )
          ).toList()
      ));
    } else {
      _categories.addAll(await httpCelebFeedListcategory(celebId).then(
              (value) => value.map(
                  (e) => CategoryData(
                  categoryId: e.id,
                  categoryName: e.name
              )
          ).toList()
      ));
    }


    categoryChildren.clear();
    bool withEditButton = !_isUserFeed && (celebId == Provider.of<LoginVerifyProvider>(context, listen: false).userId);
    if (withEditButton) categoryChildren.add(Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: GestureDetector(
          onTap: () async {
            await showGeneralDialog(
              context: context,
              pageBuilder: (context, animation, secondAnimation) => CategoryEditDialog(
                categoryData: this._categories,
                isCelebFeedCategory: !_isContent,
              ),
              barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
              barrierDismissible: false,
              transitionDuration: const Duration(milliseconds: 150),
            );
            await _setCategory();
          },
          behavior: HitTestBehavior.translucent,
          child: _FeedCategoryEditTab()
      ),
    ));
    categoryChildren.addAll(this._categories.asMap().entries.map((e) => Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: GestureDetector(
        onTap: () => setState(() {
          this._selected = e.key;
          this._loaded = false;
        }),
        behavior: HitTestBehavior.translucent,
        child: FeedTabCategoryTab(
          name: e.value.categoryName,
          selected: e.key == this._selected,
        ),
      ),
    )).toList());
  }

  Future<bool> _getFeedTabViewData() async {
    if (_loaded) return true;

    int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
    if (_isContent) {
      try {
        await _setCategory();
        HttpFeedListDto data = await httpContentList(context: context,
            celebId: celebId,
            howManyFeed: 10,
            categoryId: _categories[_selected].categoryId
        );
        _lastCreated = data.tailCreated;
        _contentFeedData.clear();
        for (HttpFeedItemVo iter in data.feeds) {
          if (!(_selected == 0 ||
              _categories[_selected].categoryId == iter.categoryId)) continue;
          _contentFeedData.add(ContentFeedData(
              feedId: iter.feedId,
              userId: iter.feedOwnerId,
              userName: iter.feedOwnerName,
              profileImg: iter.feedOwnerProfileImageUrl,
              postTimestamp: iter.updatedTime,
              category: iter.categoryName,
              imgUrl: iter.pictureUrls,
              videoUrl: iter.videoUrl,
              youtubeUrl: iter.youtubeUrl,
              membership: iter.badgeImageUrl,
              text: iter.text,
              scrapCount: iter.scrapCount,
              isScrap: iter.scraped,
              likeCount: iter.likeCount,
              isLike: iter.liked,
              commentCount: iter.commentCount,
              canView: !iter.restricted,
              title: iter.title,
              viewCount: iter.viewCount,
              categoryId: iter.categoryId,
              accessLevel: iter.accessLevel,
              isEdited: iter.isEdited
          ));
        }
      } catch (e) {
        print(e.toString());
        return false;
      }
    } else if (_isUserFeed) {
      try {
        await _setCategory();

        HttpFeedListDto data = await httpFeedList(context: context, celebId: celebId, howManyFeed: 10, categoryId: _categories[_selected].categoryId);
        _lastCreated = data.tailCreated;
        _feedData.clear();
        for (HttpFeedItemVo iter in data.feeds) {
          if (!(_selected == 0 || _categories[_selected].categoryId == iter.categoryId)) continue;
          _feedData.add(FeedData(
              feedId: iter.feedId,
              userId: iter.feedOwnerId,
              userName: iter.feedOwnerName,
              profileImg: iter.feedOwnerProfileImageUrl,
              membershipBadgeUrl: iter.badgeImageUrl,
              postTimestamp: iter.updatedTime,
              category: iter.categoryName,
              imgUrl: iter.pictureUrls,
              videoUrl: iter.videoUrl,
              youtubeUrl: iter.youtubeUrl,
              text: iter.text,
              scrapCount: iter.scrapCount,
              isScrap: iter.scraped,
              likeCount: iter.likeCount,
              isLike: iter.liked,
              commentCount: iter.commentCount,
              canView: !iter.restricted,
              isEdited: iter.isEdited
          ));
        }
      } catch (e) {
        print(e.toString());
        return false;
      }
    } else {
      try {
        await _setCategory();

        HttpFeedListDto data = await httpCelebFeedList(context: context,
            celebId: celebId,
            howManyFeed: 10,
            categoryId: _categories[_selected].categoryId);

        _lastCreated = data.tailCreated;
        _feedData.clear();
        for (HttpFeedItemVo iter in data.feeds) {
          if (!(_selected == 0 ||
              _categories[_selected].categoryId == iter.categoryId)) continue;
          _feedData.add(FeedData(
              feedId: iter.feedId,
              userName: iter.feedOwnerName,
              userId: iter.feedOwnerId,
              profileImg: iter.feedOwnerProfileImageUrl,
              membershipBadgeUrl: iter.badgeImageUrl,
              postTimestamp: iter.updatedTime,
              category: iter.categoryName,
              imgUrl: iter.pictureUrls,
              videoUrl: iter.videoUrl,
              youtubeUrl: iter.youtubeUrl,
              text: iter.text,
              scrapCount: iter.scrapCount,
              isScrap: iter.scraped,
              likeCount: iter.likeCount,
              isLike: iter.liked,
              commentCount: iter.commentCount,
              canView: !iter.restricted,
              isUserFeed: false,
              isEdited: iter.isEdited,
              accessLevel: iter.accessLevel
          ));
        }
      } catch (e) {
        print(e.toString());
        return false;
      }
    }
    _loaded = true;
    return true;
  }

  @override
  void initState(){
    super.initState();
    switch(widget.previousPage){
      case "home":
        BackButtonInterceptor.add(homeRootBackInterceptor(context), name: "community", zIndex: 3);
        break;
      case "myPage":
        BackButtonInterceptor.add(myPageRootBackInterceptor(context), name: "community", zIndex: 3);
        break;
      case "subscribe":
        BackButtonInterceptor.add(mySubRootBackInterceptor(context), name: "community", zIndex: 3);
        break;
      default:
        print("error");
    }

  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("community");
    super.dispose();
  }
  Future<bool> _setWriteFeedType() async {
    int userId = Provider.of<LoginVerifyProvider>(context, listen: false).userId;
    int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
    try {
      bool isUserFeedWrite = (userId != celebId);
      _categories = List();
      _categories = await (isUserFeedWrite ? httpFeedListcategory() : httpCelebFeedListcategory(celebId)).then(
              (value) => value.map(
                  (e) => CategoryData(
                  categoryId: e.id,
                  categoryName: e.name
              )
          ).toList()
      );
      Provider.of<FeedWriteDataProvider>(context, listen: false).categoryId=_categories[_selected].categoryId;
    } catch (e) {
      print(e.toString());
      return false;
    }

    return true;
  }

  ScrollController _scrollController;
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    LoginVerifyProvider loginVerifyProvider = Provider.of<LoginVerifyProvider>(context, listen: false);
    SelectedCelebProvider selectedCelebProvider = Provider.of<SelectedCelebProvider>(context, listen: false);

    Widget floatingActionButton = Container();
    if (_isUserFeed && loginVerifyProvider.userId != selectedCelebProvider.celebId) {
      floatingActionButton = GestureDetector(
        onTap: () async {
//          bool success = await _setWriteFeedType();
//          if(!success) return;
          Provider.of<FeedWriteViewProvider>(context, listen: false).writeType = FeedWriteType.general;
          Provider.of<FeedWriteDataProvider>(context, listen: false)
            ..text=""
            ..imageUrls=[]
            ..videoUrl=null
            ..youtubeUrl=null
            ..ttsId=null
            ..categoryId=null;
          var result = await Navigator.pushNamed(context, kRouteWriteFeed);
          if (result == true) {
            setState(() {
              _loaded = false;
            });
          }
        },
        behavior: HitTestBehavior.translucent,
        child: Container(
          width: 44,
          height: 44,
          decoration: BoxDecoration(
              color: kFunctionButtonColor,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.39),
                  offset: Offset(1, 2),
                )
              ]
          ),
          child: Icon(
            FontAwesomeIcons.pen,
            size: 21,
            color: Theme.of(context).backgroundColor,
          ),
        ),
      );
    }
    else if (_isContent && loginVerifyProvider.userId == selectedCelebProvider.celebId) {
      floatingActionButton = GestureDetector(
        onTap: () async {
          Provider.of<FeedWriteViewProvider>(context, listen: false).writeType = FeedWriteType.contents;
          Provider.of<ContentWriteDataProvider>(context, listen: false)
            ..title=""
            ..text=""
            ..imageUrls=[]
            ..videoUrl=null
            ..youtubeUrl=null
            ..ttsId=null
            ..categoryId=null;
          var result = await Navigator.pushNamed(context, kRouteWriteContent);
          if (result == true) {
            setState(() {
              _loaded = false;
            });
          }
        },
        behavior: HitTestBehavior.translucent,
        child: Container(
          width: 44,
          height: 44,
          decoration: BoxDecoration(
              color: kFunctionButtonColor,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.39),
                  offset: Offset(1, 2),
                )
              ]
          ),
          child: Icon(
            FontAwesomeIcons.pen,
            size: 21,
            color: Theme.of(context).backgroundColor,
          ),
        ),
      );
    }
    else if (!_isContent && !_isUserFeed && loginVerifyProvider.userId == selectedCelebProvider.celebId) {
      floatingActionButton = GestureDetector(
        onTap: () async {
          Provider.of<FeedWriteViewProvider>(context, listen: false).writeType = FeedWriteType.celeb;
          Provider.of<FeedWriteDataProvider>(context, listen: false)
            ..text=""
            ..imageUrls=[]
            ..videoUrl=null
            ..youtubeUrl=null
            ..ttsId=null
            ..categoryId=null;
          var result = await Navigator.pushNamed(context, kRouteWriteFeed);
          if (result == true) {
            setState(() {
              _loaded = false;
            });
          }
        },
        behavior: HitTestBehavior.translucent,
        child: Container(
          width: 44,
          height: 44,
          decoration: BoxDecoration(
              color: kFunctionButtonColor,
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.39),
                  offset: Offset(1, 2),
                )
              ]
          ),
          child: Icon(
            FontAwesomeIcons.pen,
            size: 21,
            color: Theme.of(context).backgroundColor,
          ),
        ),
      );
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      switch(widget.previousPage){
        case "home":
          Provider.of<MainNavigationProvider>(context, listen: false).leftButtonFunction = homeLeftBackFunction(context);
          break;
        case "myPage":
          Provider.of<MainNavigationProvider>(context, listen: false).leftButtonFunction = myPageLeftBackButton(context);
          break;
        case "subscribe":
          Provider.of<MainNavigationProvider>(context, listen: false).leftButtonFunction = mySubLeftBackButton(context);
          break;
        default:
          print("error");
      }
      Provider.of<MainNavigationProvider>(context, listen: false).floatingActionButton = floatingActionButton;
    });

    return FutureBuilder<bool>(
        future: _getData(Provider.of<SelectedCelebProvider>(context, listen: false).celebId),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data) {
            return DefaultTabController(
                length: 4,
                child: Scaffold(
                    body: NestedScrollView(
                      controller: _scrollController,
                      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                        return <Widget>[
                          SliverOverlapAbsorber(
                            handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                            sliver: SliverAppBar(
                              backgroundColor: Colors.white, // app bar color
                              flexibleSpace: FlexibleSpaceBar(
                                collapseMode: CollapseMode.pin,
                                centerTitle: true,
                                background: Column(
                                  children: <Widget>[
                                    _CelebCommunityProfile(
                                      profileImgUrl: _data.profileImageUrl,
                                      subscriberCount: _data.subscriberCount,
                                      feedCount: _data.feedCount,
                                      description: _data.introduction,
                                      isSubscribing: Provider.of<CelebProfileProvider>(context, listen: false).celeb.isSubscribing,
                                      profileData: _profileData,
                                      onTap: (){
                                        setState(() {});
                                    }
                                    ),
                                    SizedBox(height: 16,),
                                    Divider(
                                      height: 0.5,
                                      thickness: 1,
                                      indent: 14,
                                      endIndent: 14,
                                    ),
                                  ],
                                ),
                              ),
                              leading: Container(),
                              expandedHeight: 166,
                              pinned: true,
                              floating: true,
                              forceElevated: innerBoxIsScrolled,
                              bottom: TabBar(
                                indicatorColor: Theme.of(context).primaryColor,
                                labelColor: Theme.of(context).primaryColor,
                                labelStyle: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold
                                ),
                                unselectedLabelColor: kNavigationPressableColor,
                                unselectedLabelStyle: TextStyle(fontSize: 15),
                                tabs: <Tab>[
                                  Tab(child: Text("피드")),
                                  Tab(child: Text("셀럽")),
                                  Tab(child: Text("콘텐츠")),
                                  Tab(child: Text("서비스")),
                                ],
                              onTap: (tabIndex) {
                                _feedData.clear();
                                _contentFeedData.clear();
                                _categories.clear();
                                categoryChildren.clear();
                                switch (tabIndex) {
                                  case 0:
                                    _isUserFeed = true;
                                    _isContent = false;
                                    break;
                                  case 1:
                                    _isUserFeed = false;
                                    _isContent = false;
                                    break;
                                  case 2:
                                    _isUserFeed = false;
                                    _isContent = true;
                                    break;
                                }
                                _loaded = false;
                                _getFeedTabViewData().then((value) {
                                  if (value) setState(() {});
                                });
                              },
                              controller: _tabController,
                              ),
                            ),
                          ),
                        ];
                      },
                      body:TabBarView(
                          controller: _tabController,
                          physics: NeverScrollableScrollPhysics(),
                          children: [
                            FeedTabView(
                              pageKey: "피드",
                              isUserFeed: true,
                              getMoreFeeds: _getMoreFeeds,
                              refreshFeeds: _refreshFeeds,
                              feedData: _feedData,
                              categoryChildren: categoryChildren,
                            ),
                            FeedTabView(
                              pageKey: "셀럽",
                              getMoreFeeds: _getMoreFeeds,
                              refreshFeeds: _refreshFeeds,
                              feedData: _feedData,
                              withEditButton: true,
                              categoryChildren: categoryChildren,
                            ),
                            FeedTabView(
                              pageKey: "콘텐츠",
                              getMoreFeeds: _getMoreFeeds,
                              refreshFeeds: _refreshFeeds,
                              isContent: true,
                              contentFeedData: _contentFeedData,
                              withEditButton: true,
                              categoryChildren: categoryChildren,
                            ),
                            _ServiceTab(),
                          ],
                        ),
                      ),
                    )
                );
          } else {
            return Container(
              color: Colors.white,
              child: Center(child: CircularProgressIndicator()),
            );
          }
        });
  }
}


class _CelebCommunityProfile extends StatefulWidget {
  final int subscriberCount;
  final int feedCount;
  final String description;
  final String profileImgUrl;
  final bool isSubscribing;
  final Function onTap;
  final HttpProfileViewDto profileData;

  const _CelebCommunityProfile({
    Key key,
    this.subscriberCount = 0,
    this.feedCount = 0,
    this.description = "",
    this.profileImgUrl,
    this.isSubscribing,
    this.onTap,
    this.profileData
  }) : super(key: key);
  @override
  __CelebCommunityProfileState createState() => __CelebCommunityProfileState();
}

class __CelebCommunityProfileState extends State<_CelebCommunityProfile> {

  @override
  Widget build(BuildContext context) {
    RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
    Function mathFunc = (Match match) => '${match[1]},';

    String subsCountStr = widget.subscriberCount.toString().replaceAllMapped(reg, mathFunc);
    String feedCountStr = widget.feedCount.toString().replaceAllMapped(reg, mathFunc);

    return Column(
      children: [
        Container(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child:Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: Center(
                  child: GestureDetector(
                    onTap: (){
                      var provider = Provider.of<SelectedCelebProvider>(context, listen: false);
                      Provider.of<CelebProfileProvider>(context, listen: false).setCelebProfile(
                          celebName: provider.celebName,
                          celebId: provider.celebId
                      );
                      Navigator.pushNamed(context, kRouteCelebProfile).then((value){
                        if(value??false)setState(() {});
                      });},
                    child: Container(
                      padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                      child: FittedBox(
                        fit: BoxFit.fill,
                        child: CircleAvatar(
                          radius: 72,
                          backgroundColor: kMainColor,
                          backgroundImage: widget.profileImgUrl == null || widget.profileImgUrl == ""
                              ? AssetImage(
                            "assets/icon/seem_off_3x.png",
                          )
                              : NetworkImage(
                            widget.profileImgUrl,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                  child: Center(
                    child: Column(
                      children: [
                        Text("구독자", style: kProfileInfoTextStyle),
                        SizedBox(height: 4,),
                        Text(subsCountStr, style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w700),)
                      ],
                    ),
                  )),
              Expanded(
                child: Center(
                  child: Column(
                    children: [
                      Text("피드", style: kProfileInfoTextStyle,),
                      SizedBox(height: 4,),
                      Text(feedCountStr, style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w700),)
                    ],
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 13.0, vertical: 14.0),
                child: IntrinsicWidth(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      GestureDetector(
                        onTap: () async {
                          bool ok = await (Provider.of<CelebProfileProvider>(context, listen: false).celeb.isSubscribing ? httpCelebProfileUnsubscribe(context: context, whomId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId) : httpCelebProfileSubscribe(context: context, whomId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId));
                          if (!ok) {
                            Fluttertoast.showToast(msg: "구독${Provider.of<CelebProfileProvider>(context, listen: false).celeb.isSubscribing ? " 취소" : ""}에 실패했습니다.", gravity: ToastGravity.TOP);
                          }
                          else {
                            widget.onTap();
                          }
                        },
                        child: SubscribeButton(
                          isSubscribing: Provider.of<CelebProfileProvider>(context, listen: false).celeb.isSubscribing,
                          celebId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId,
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                      ProfileButton(
                        name: "프로필",
                        buttonColor: Colors.white,
                        borderColor: kProfileButtonColor,
                        textColor: kProfileButtonTextColor,
                        onTap: () async{
                          var provider = Provider.of<SelectedCelebProvider>(context, listen: false);
                          Provider.of<CelebProfileProvider>(context, listen: false).setCelebProfile(
                              celebName: provider.celebName,
                              celebId: provider.celebId
                          );
                          await Navigator.pushNamed(context, kRouteCelebProfile).then((value){
                            if(value??false) setState(() {
                            });
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ),
        // widget.feedCount ==0
        // ?Container(
        //   width: double.infinity,
        //   padding: const EdgeInsets.only(left: 16.0, right: 16.0),
        //   child: Container(
        //     padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 16.0),
        //     child: Column(
        //       crossAxisAlignment: CrossAxisAlignment.start,
        //       children: [
        //         Text("INSTA : ${widget.profileData.instagramUrl}", style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w400),),
        //         Text("YOUTUBE : ${widget.profileData.instagramUrl}", style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w400)),
        //         Text("FACEBOOK : ${widget.profileData.facebookUrl}", style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w400)),
        //         Text("TWITTER : ${widget.profileData.twitterUrl}", style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w400)),
        //         Text("${widget.profileData.greeting}", style: kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w400)),
        //       ],
        //     ),
        //   ),
        // ):Container()
      ]
    );
  }
}

class SubscribeButton extends StatelessWidget {
  final bool isSubscribing;
  final int celebId;
  final Function onTap;

  const SubscribeButton({Key key, this.celebId, this.onTap, this.isSubscribing}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              color: isSubscribing?Colors.white:kMainColor,
              border: Border.all(
                  color: isSubscribing?kMainColor:Colors.transparent,
                  width: 1
              )
          ),
          child: Container(
            width: 80,
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Center(
              child: Text(isSubscribing?"구독중":"구독하기",
                style : kProfileInfoTextStyle.copyWith(fontWeight: FontWeight.w700, color: isSubscribing?kMainColor:Colors.white, fontSize: 12))
            ),
          )
      );
  }
}


class ProfileButton extends StatelessWidget {
  final String name;
  final Color buttonColor;
  final Color textColor;
  final Color borderColor;
  final Function onTap;

  const ProfileButton({Key key, this.name, this.buttonColor, this.textColor, this.borderColor, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              color: buttonColor,
              border: Border.all(
                color: borderColor,
                width: 1
              )
          ),
          child: Container(
            width: 80,
            padding: const EdgeInsets.symmetric(vertical: 8),
            child: Center(
              child: Text(name,
                style: kProfileInfoTextStyle.copyWith(color: textColor, fontSize: 12))
            ),
          )
      ),
    );
  }
}


class _ServiceTab extends StatelessWidget {

  Future<Map> _getData(BuildContext context) async {
    Map response = await httpMyPageRootViewRepo(context: context);
    return response;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _getData(context),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
              Provider.of<MyPageViewProvider>(context, listen: false).setMyPageInfo(
                  name: snapshot.data["user"]["name"],
                  profileImgUrl: snapshot.data["user"]["profileImageUrl"],
                  bannerImgUrl: snapshot.data["user"]["bannerImageUrl"],
                  celebList: snapshot.data["celebList"],
                  scrapBoardData: snapshot.data["boardList"]
              );
            });
            return Column(
              children: [
                Container(width: double.infinity, height: 60),
                Container(
                  padding: const EdgeInsets.only(top: 20),
                  child: SingleChildScrollView(
                    child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: _CommunityOption.values.map((e) {
                          return GestureDetector(
                            onTap: () {
                              switch(e){
                                case _CommunityOption.tts:
                                  Provider.of<TtsWriteProvider>(context, listen: false).setFromCommunity();
                                  break;
                                case _CommunityOption.message:
                                  Provider.of<MessagingDisplayProvider>(context, listen: false).setMessagingView(MessagingType.getCeleb,
                                      Provider.of<SelectedCelebProvider>(context, listen: false).celebName,
                                      Provider.of<SelectedCelebProvider>(context, listen: false).celebId);
                                  break;
                                case _CommunityOption.membership:
                                  break;
                              }
                              return Navigator.pushNamed(
                                  context, _getServiceRouteName(e))
                                  .then((value) {
                                _reset(e, context);
                              });
                            },
                            behavior: HitTestBehavior.translucent,
                            child: _ServiceOption(
                              option: e,
                            ),
                          );}).toList()
                    ),
                  ),
                ),
              ]
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        }
    );
  }


  void _reset(_CommunityOption value, BuildContext context){
    if(value == _CommunityOption.message){
      Provider.of<MessagingDisplayProvider>(context, listen: false).reset();
    }
    if(value == _CommunityOption.tts){
      Provider.of<TtsWriteProvider>(context, listen: false).reset();
      Provider.of<TtsWriteProvider>(context, listen: false).resetTtsWrite();
    }
  }

  String _getServiceRouteName(_CommunityOption value){
    switch(value){
      case _CommunityOption.tts:
        return kRouteTtsService;
      case _CommunityOption.message:
        return kRouteMessaging;
      case _CommunityOption.membership:
        return kRouteMembershipJoin;
      default:
        throw UnimplementedError("unimplemented!");
    }
  }
}

class _ServiceOption extends StatelessWidget {
  final _CommunityOption option;
  final int count;

  const _ServiceOption({
    Key key,
    this.option,
    this.count = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border.all(
              color: kBorderColor,
              width: 0.5
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.09),
              blurRadius: 8,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 72,
                ),
                Container(
                  width: 27,
                  child: Center(
                    child: _communityOptionWidget(option),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
              child: Text(
                _communityOptionName(option),
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 13,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  width: 20,
                  height: 20,
                  decoration: count == 0 ? null : BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    shape: BoxShape.circle,
                  ),
                  child: Center(
                    child: Text(
                      "$count",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 10,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Container(
                  width: 12,
                  child: Image.asset("assets/icon/right_3x.png"),
                ),
                SizedBox(
                  width: 30,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

enum _CommunityOption {
  tts,
  message,
  membership
}

String _communityOptionName(_CommunityOption option){
  switch(option){
    case _CommunityOption.tts:
      return "TTS 콘텐츠 응모";
    case _CommunityOption.message:
      return "메세지";
    case _CommunityOption.membership:
      return "멤버십";
    default:
      throw UnimplementedError();
  }
}

Widget _communityOptionWidget(_CommunityOption option){
  switch(option){
    case _CommunityOption.tts:
      return Icon(
        Icons.settings_voice,
        color: Color.fromRGBO(165, 165, 165, 1),
      );
    case _CommunityOption.message:
      return Icon(
        Icons.mail,
        color: Color.fromRGBO(165, 165, 165, 1),
      );
    case _CommunityOption.membership:
      return Icon(
        FontAwesomeIcons.gift,
        color: Color.fromRGBO(165, 165, 165, 1),
      );
    default:
      throw UnimplementedError();
  }
}



class _FeedCategoryEditTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 27,
      width: 66,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(14),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.edit,
            color: Theme.of(context).backgroundColor,
            size: 12,
          ),
          SizedBox(
            width: 6.5,
          ),
          Text(
            "편집",
            style: TextStyle(
                fontSize: 12,
                color: Theme.of(context).backgroundColor
            ),
          )
        ],
      ),
    );
  }
}
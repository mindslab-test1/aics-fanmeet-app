
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/message/dto/message_celeb_list_dto.dart';
import 'package:rf_tap_fanseem/http/message/message_celeb_list.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';
import 'dart:io';
import '../../../../colors.dart';
import 'component/notice_component.dart';

class NoticeBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: DefaultTabController(
        initialIndex: 0,
        length: Platform.isAndroid?2:1,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 53,
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                border: Border(bottom: BorderSide(
                  color: Color.fromRGBO(0, 0, 0, 0.1)
                )),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.05),
                    blurRadius: 11,
                    offset: Offset(0, 3),
                  )
                ]
              ),
              child: TabBar(
                indicatorColor: Theme.of(context).primaryColor,
                labelColor: Theme.of(context).primaryColor,
                labelStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 17,
                    fontWeight: FontWeight.bold
                ),
                unselectedLabelColor: kNavigationPressableColor,
                unselectedLabelStyle: TextStyle(
                    fontSize: 15
                ),
                tabs: <Widget>[
                  // if(Platform.isAndroid) Tab(child: Text("셀럽 메세지")),
                  if(Platform.isAndroid) Tab(child: Text("놓친 알림")),
                  Tab(child: Text("셀럽 메세지")),
//                  Tab(child: Text("유저 메세지")),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  if(Platform.isAndroid)_MissedNoticesTab(),
                  _MessageTab( isCelebMsg: true,),
//                   if(Platform.isAndroid)_MissedNoticesTab(
// //                    notices: _testNotices,
//                   ),

//                  _MessageTab(
//                    isCelebMsg: false,
//                    messages: _testUserMessages,
//                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _MissedNoticesTab extends StatelessWidget {
  final List<NoticeData> notices;

  const _MissedNoticesTab({
    Key key,
    this.notices = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text("활동 알림 서비스 준비중입니다. 조금만 기다려주세요!"),
      ),
    );
//      SingleChildScrollView(
//      child: Column(
//        mainAxisSize: MainAxisSize.min,
//        mainAxisAlignment: MainAxisAlignment.start,
//        children: <Widget>[
//          Container(
//            padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 31),
//            height: 54,
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                // TODO filter
//                Container(
//                  width: 97,
//                  height: 27,
//                  decoration: BoxDecoration(
//                    color: Theme.of(context).backgroundColor,
//                    borderRadius: BorderRadius.circular(14),
//                    border: Border.all(
//                        color: kUnSelectedColor,
//                        width: 1
//                    ),
//                  ),
//                  child: Center(
//                    child: Row(
//                      mainAxisSize: MainAxisSize.min,
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text(
//                          "셀럽 전체",
//                          style: TextStyle(
//                              color: kSelectableTextColor,
//                              fontSize: 12
//                          ),
//                        ),
//                        SizedBox(
//                          width: 7.2,
//                        ),
//                        Image(
//                          image: AssetImage("assets/button/down_button.png"),
//                          fit: BoxFit.fitWidth,
//                          width: 8.9,
//                          height: 5.3,
//                          color: kUnSelectedColor,
//                          colorBlendMode: BlendMode.dst,
//                        )
//                      ],
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          Container(
//            child: notices.isNotEmpty ? Column(
//              mainAxisSize: MainAxisSize.min,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: notices.map((e) => GestureDetector(
//                // TODO get feed data before pushing
//                // onTap: () => Navigator.pushNamed(context, kRouteFeedDetail),
//                onTap: () => Fluttertoast.showToast(msg: "TODO", toastLength: Toast.LENGTH_SHORT),
//                behavior: HitTestBehavior.translucent,
//                child: NoticeElement.buildWith(e),
//              )).toList(),
//            ) : Container(
//              height: 200,
//              child: Center(
//                child: Text("놓친 알림이 없습니다."),
//              ),
//            ),
//          )
//        ],
//      ),
//    );
  }
}

class _MessageTab extends StatelessWidget {
  final bool isCelebMsg;
  List<MessageData> messages = [];

  _MessageTab({
    Key key,
    this.isCelebMsg = false,
  }) : super(key: key);

  Future<bool> _getData(BuildContext context) async {
    List<HttpMessageCelebListDto> response = await httpMessageCelebList(context: context);
    if (response == null) return false;
    messages = [];
    for (var iter in response) {
      messages.add(MessageData(
        celebId: iter.celebId,
        userName: iter.name,
        profileImg: iter.profileImageUrl,
        text: iter.text,
        timestamp: iter.createdTime,
        count: 0
      ));
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getData(context),
      builder: (context, snapshot) {
        return SingleChildScrollView(
          child: messages.isNotEmpty ? Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: messages.map((e) => GestureDetector(
              onTap: () {
                Provider.of<MessagingDisplayProvider>(context, listen: false).setMessagingView(
                  isCelebMsg ? MessagingType.getCeleb : MessagingType.getUser,
                  e.userName,
                  e.celebId
                );
                Navigator.pushNamed(context, kRouteMessaging).then((value) {
                  var provider = Provider.of<MessagingDisplayProvider>(context, listen: false);
                  provider.reset();
                });
              },
              child: MessageElement.buildWith(e)
            )).toList(),
          ) : Container(
            height: 200,
            child: Center(
              child: Text("메세지가 없습니다."),
            ),
          ),
        );
      }
    );
  }
}

List<MessageData> _testUserMessages = [
  MessageData(
      userName: "Lorem Ipsum",
      profileImg: null,
      text: "안녕하세요! OO님, 구독해주셔서 감사합니다!",
      timestamp: "오전 10:35",
      count: 20
  ),
  MessageData(
      userName: "Nino Maximus Keyser Soze",
      profileImg: null,
      text: "안녕하세요! OO님, 구독해주셔서 감사합니다!",
      timestamp: "오전 10:35",
      count: 8
  ),
  MessageData(
      userName: "정형돈",
      profileImg: null,
      text: "TTS 메세지가 도착했습니다.",
      timestamp: "오전 10:35",
      count: 1
  ),
];


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/notice/notice_body.dart';
import 'package:rf_tap_fanseem/view/main/body/require_login_body.dart';

class NoticePartingBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isLogin = Provider.of<LoginVerifyProvider>(context).isLogin;
    if(isLogin == null ? false : isLogin) return NoticeBody();
    else return RequireLoginBody();
  }
}

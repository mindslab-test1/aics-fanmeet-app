import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/login/login_function.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_utlis.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/setting_page_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/main.dart';

class SettingRootView extends StatelessWidget {

  const SettingRootView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<MainNavigationProvider>(context, listen: false).setAll(
          title: "설정",
          leftButton: BarLeftButton.back,
          rightButton: BarRightButton.none,
          leftFunction: myPageLeftBackButton(context),
          rightFunction: dummyFunction
      );
    });

    return Container(
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: Settings.values.map((settingEnum) => HeaderAndBodyComponent(
            headerTitle: getSettingName(settingEnum),
            child: NextPageBarHolder(
              childrenValues: getDetailSettingNames(settingEnum)
                  .asMap().entries.map((nameEntry) => NextPageBarData(
                  onTap: () async {
                    if(nameEntry.value == "로그아웃") {
                      bool isSuccess = await logout(FanseemApp.navigatorKey.currentContext);
                      if (isSuccess) {
                        Provider.of<MainNavigationProvider>(FanseemApp.navigatorKey.currentContext, listen: false).navigateRoot(0, FanseemApp.navigatorKey.currentContext);
                        Fluttertoast.showToast(msg: "로그아웃 되었습니다.", gravity: ToastGravity.TOP);
                        Navigator.pushNamedAndRemoveUntil(FanseemApp.navigatorKey.currentContext, kRouteSplash, (route) => false);
                      }
                      else {
                        Fluttertoast.showToast(msg: "로그아웃에 실패했습니다.", gravity: ToastGravity.TOP);
                      }
                    }
                    else{
                      if(nameEntry.value == "푸시 설정") {
                        await Navigator.pushNamed(context, kRouteSettingsPush);
                      }
                      else{
                        Provider.of<SettingPageProvider>(context, listen: false).setPage(nameEntry.key, settingEnum);
                        Provider.of<SubViewControllerProvider>(context, listen: false)
                            .get(MyPageSubRoutes.myPageSettings)
                            .animateToPage(1, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
                      }
                    }
                  },
                  text: nameEntry.value,
                  textStyle: nameEntry.value == "로그아웃" ? TextStyle(
                      fontSize: 13,
                      color:  kNegativeTextColor
                  ) : null
              )).toList(),
            ),
          )).toList(),
        ),
      ),
    );
  }
}

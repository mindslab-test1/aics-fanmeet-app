
enum Settings {
  user,
//  alert,
//  cc
}

String getSettingName(Settings settings){
  switch(settings){
    case Settings.user:
      return "계정";
//    case Settings.alert:
//      return "알림 설정";
//    case Settings.cc:
//      return "고객센터";
    default:
      throw UnimplementedError("unimplemented!");
  }
}

List<String> getDetailSettingNames(Settings settings){
  switch(settings){
    case Settings.user:
      return ["로그아웃"];
//    case Settings.alert:
//      return ["푸시 설정"];
//    case Settings.cc:
//      return ["FAQ", "차단한 유저"];
    default:
      throw UnimplementedError("unimplemented!");
  }
}

String getDetailSettingName(Settings settings, int index){
  return getDetailSettingNames(settings)[index];
}
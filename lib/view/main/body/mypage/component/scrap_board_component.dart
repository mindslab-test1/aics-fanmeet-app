
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/scrap_board_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/dialog/scrap_delete_dialog.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';

class ScrapBoardGrid extends StatelessWidget {
  final List<ScrapBoardCardData> dataList;
  final ScrollPhysics scrollPhysics;
  final bool editState;
  final void Function() onTapOverride;

  const ScrapBoardGrid({
    Key key,
    this.dataList,
    this.scrollPhysics = const NeverScrollableScrollPhysics(),
    this.editState,
    this.onTapOverride
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 3,
                offset: Offset(0, 2)
            )
          ]
      ),
      child: GridView.count(
        shrinkWrap: true,
        primary: true,
        crossAxisCount: 2,
        crossAxisSpacing: 24,
        childAspectRatio: 165 / 274,
        physics: scrollPhysics,
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        children: dataList.map((e)
            => ScrapBoardCard.buildWith(e, editState: editState, onTapOverride: onTapOverride))
          .toList(),
      ),
    );
  }
}


class ScrapBoardCard extends StatelessWidget {
  final int boardId;
  final String name;
  final String datetimeString;
  final int feedCnt;
  final String imgUrl;
  final bool editState;
  final void Function() onTap;

  const ScrapBoardCard({
    Key key,
    this.boardId,
    this.name,
    this.datetimeString,
    this.feedCnt = 0,
    this.imgUrl,
    this.editState = false,
    this.onTap,
  }) : super(key: key);

  static ScrapBoardCard buildWith(ScrapBoardCardData data, {bool editState, void Function() onTapOverride}){
    return ScrapBoardCard(
      boardId: data.boardId,
      name: data.name,
      datetimeString: data.datetimeString,
      feedCnt: data.feedCnt,
      imgUrl: data.imgUrl,
      editState: editState == null ? false : editState,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap != null ? onTap : () {
        if(!editState) {
          Provider.of<ScrapBoardProvider>(context, listen: false).setBoard(
            boardId: boardId,
            boardName: name
          );
          Navigator.pushNamed(context, kRouteScrapBoard);
        }
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        width: 165,
        height: 254,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 165,
              height: 181,
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 2,
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          height: 152,
                          child: imgUrl == null ? Image.asset(
                            "assets/samples/sample_scrapbook.jpg",
                            fit: BoxFit.fitHeight,
                          ) : CachedNetworkImage(
                            imageUrl: imgUrl,
                            fit : BoxFit.fitHeight,
                            placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                            errorWidget: (context, url, error) => Icon(Icons.error),
                          ),
                        ),
                        Container(
                          height: 29,
                          padding: EdgeInsets.only(left: 10, top: 8, bottom: 8),
                          child: Text(
                            "총 피드: $feedCnt",
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 9,
                              color: kSelectableTextColor
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Positioned(
                    top: 4,
                    right: 4,
                    child: this.editState ? GestureDetector(
                      onTap: () => showGeneralDialog(
                        context: context,
                        pageBuilder: (context, animation, secondAnimation) => DeleteScrapDialog(
                          boardId: boardId,
                          boardName: name,
                        ),
                        barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                        barrierDismissible: false,
                        transitionDuration: const Duration(milliseconds: 150),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).backgroundColor,
                          boxShadow: [
                            BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.36),
                              blurRadius: 8,
                              offset: Offset(0, 2)
                            )
                          ]
                        ),
                        child: Icon(
                          Icons.cancel,
                          color: Colors.red,
                        ),
                      ),
                    ) : Container(),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
//            this.editState ? RichText(
//              maxLines: 2,
//              overflow: TextOverflow.ellipsis,
//              text: TextSpan(
//                style: TextStyle(
//                    fontSize: 15,
//                    fontWeight: FontWeight.bold,
//                    color: Colors.black,
//                    height: 1.3
//                ),
//                children: [
//                  WidgetSpan(
//                    alignment: PlaceholderAlignment.middle,
//                    child: GestureDetector(
//                      onTap: dummyFunction,
//                      child: Icon(
//                        FontAwesomeIcons.pen,
//                        size: 15,
//                        color: kSelectableTextColor,
//                      ),
//                    ),
//                  ),
//                  WidgetSpan(
//                    child: SizedBox(width: 5,)
//                  ),
//                  TextSpan(
//                    text: name,
//                  ),
//                ],
//              ),
//            ) :
            Text(
              name,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: Colors.black,
                height: 1.3
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Text(
              datetimeString,
              style: TextStyle(
                  fontSize: 10,
                  color: kSelectableTextColor
              ),
            )
          ],
        ),
      ),
    );
  }
}

class ScrapBoardCardData {
  final int boardId;
  final String name;
  final String datetimeString;
  final int feedCnt;
  final String imgUrl;
  final void Function() onTap;

  ScrapBoardCardData({
    this.boardId,
    this.name,
    this.datetimeString,
    this.feedCnt = 0,
    this.imgUrl,
    this.onTap = dummyFunction,
  });
}
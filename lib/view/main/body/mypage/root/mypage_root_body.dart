
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_view.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/component/scrap_board_component.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/view/main/dialog/notice_dialog.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/view/main/dialog/scrap_dialog.dart';

import 'dart:io';
import '../../../../../colors.dart';
import '../my_page_body.dart';
import '../my_page_sub_routes.dart';

class MyPageRootBody extends StatefulWidget {
  @override
  _MyPageRootBodyState createState() => _MyPageRootBodyState();
}

class _MyPageRootBodyState extends State<MyPageRootBody> {


  Future<Map> _getData(BuildContext context) async {
    Map response = await httpMyPageRootViewRepo(context: context);
    return response;
  }

  @override
  void initState() {
    BackButtonInterceptor.add(myPageRootBackInterceptor(context), name: "home",zIndex: 1);
    super.initState();
  }
  @override
  void dispose() {
    BackButtonInterceptor.removeByName("home");
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      MainNavigationProvider provider = Provider.of<MainNavigationProvider>(context, listen: false);
      provider.setAll(
          title: "내 정보",
          leftButton: BarLeftButton.back,
          leftFunction: () {
            provider.navigateRoot(0, context);
            provider.pageController.jumpToPage(0);
          },
          rightButton: BarRightButton.settings,
          rightFunction: () => Provider.of<SubViewControllerProvider>(context, listen: false)
              .get(MyPageSubRoutes.myPageRoot)
              .jumpToPage(MyPageSubPages.values.indexOf(MyPageSubPages.settings)),
      );
    });
    return FutureBuilder(
      future: _getData(context),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            Provider.of<MyPageViewProvider>(context, listen: false).setMyPageInfo(
                name: snapshot.data["user"]["name"],
                profileImgUrl: snapshot.data["user"]["profileImageUrl"],
                bannerImgUrl: snapshot.data["user"]["bannerImageUrl"],
                celebList: snapshot.data["celebList"],
                scrapBoardData: snapshot.data["boardList"]
            );
          });
          return Container(
              color: Color.fromRGBO(247, 247, 247, 1),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _MyPageProfileStack(
                      description: "",
                    ),
                    _MyPageSubcribingCelebs(),
                    Divider(
                      height: 0.1,
                      indent: 14.5,
                      endIndent: 14.5,
                      thickness: 0.5,
                    ),
                    _ScrapBoardStubDisplay(),
                    _UsingServicesDisplay()
                  ],
                ),
              )
          );
        } else {
          return Container(
            color: Colors.white,
            child: Center(child: CircularProgressIndicator(),),
          );
        }
      }
    );
  }
}

class _MyPageProfileStack extends StatelessWidget {
  final String userName;
  final String profileImgUrl;
  final String coverImgUrl;
  final int followerCnt;
  final int followingCnt;
  final int level;
  final String description;

  const _MyPageProfileStack({
    Key key,
    this.followerCnt = 0,
    this.followingCnt = 0,
    this.level = 1,
    this.userName,
    this.profileImgUrl,
    this.coverImgUrl,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 287.0 - 42,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  blurRadius: 3,
                  offset: Offset(0, 2)
              )
            ]
        ),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            // info & cover image
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: Colors.grey,
                  height: 120,
                  width: double.infinity,
                  child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                        Color.fromRGBO(0, 0, 0, 0.55),
                        BlendMode.darken
                    ),
                    child: Provider.of<MyPageViewProvider>(context).bannerImgUrl == null ? Container(
                      decoration: BoxDecoration(
                        color: kMainColor
                      ),
                      child: Image.asset(
                        "assets/logo/splash_X3.png",
                        fit: BoxFit.fitWidth,
                      ),
                    ) : CachedNetworkImage(
                      imageUrl: Provider.of<MyPageViewProvider>(context).bannerImgUrl,
                      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                      fit: BoxFit.fitWidth,
                    )//Image.network(Provider.of<MyPageViewProvider>(context).bannerImgUrl , fit: BoxFit.fitWidth),
                  ),
                ),
                SizedBox(height: 36,),
                Container(
                  height: 25,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 25,
                        child: Text(
                          "${Provider.of<MyPageViewProvider>(context).name}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      )
                    ],
                  ),
                ),
//                SizedBox(
//                  height: 15,
//                ),
//                Container(
//                  height: 42,
//                  child: Row(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      GestureDetector(
//                        onTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
//                            .get(MyPageSubRoutes.myPageRoot)
//                            .jumpToPage(MyPageSubPages.myFollowers.index),
//                        behavior: HitTestBehavior.translucent,
//                        child: Text(
//                          "팔로워\n$followerCnt",
//                          textAlign: TextAlign.center,
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 15,
//                              fontWeight: FontWeight.bold
//                          ),
//                        ),
//                      ),
//                      Text(
//                        "팔로우\n$followingCnt",
//                        textAlign: TextAlign.center,
//                        style: TextStyle(
//                            color: Colors.black,
//                            fontSize: 15,
//                            fontWeight: FontWeight.bold
//                        ),
//                      ),
//                      GestureDetector(
//                        onTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
//                            .get(MyPageSubRoutes.myPageRoot)
//                            .jumpToPage(MyPageSubPages.myLevel.index),
//                        behavior: HitTestBehavior.translucent,
//                        child: RichText(
//                          textAlign: TextAlign.center,
//                          text: TextSpan(
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 15,
//                                  fontWeight: FontWeight.bold
//                              ),
//                              children: [
//                                TextSpan(text: "LEVEL\n"),
//                                TextSpan(
//                                    text: "$level",
//                                    style: TextStyle(
//                                      color: Theme.of(context).primaryColor,
//                                    )
//                                )
//                              ]
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
                Container(
                  height: 49,
                  child: Center(
                    child: Text(
                      description,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 13,
                          color: kSelectableTextColor
                      ),
                    ),
                  ),
                )
              ],
            ),
            // CircleAvatar
            Positioned(
              top: 46,
              left: MediaQuery.of(context).size.width / 2 - 50,
              child: Align(
                child: Container(
                  width: 100,
                  height: 100,
                  child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Theme.of(context).backgroundColor,
                      child: CircleAvatar(
                        radius: 49,
                        backgroundColor: kMainColor,
                        backgroundImage: Provider.of<MyPageViewProvider>(context).profileImgUrl == null ? AssetImage(
                          "assets/icon/seem_off_3x.png",
                        ) : CachedNetworkImageProvider(
                          Provider.of<MyPageViewProvider>(context).profileImgUrl,
                        ) //NetworkImage(Provider.of<MyPageViewProvider>(context).profileImgUrl),
                      )
                  ),
                ),
              ),
            ),
            // Edit Profile Button
            Positioned(
              top: 15,
              right: 17,
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, kRouteEditProfile).then((value) {
                    Provider.of<MyPageEditProvider>(context, listen: false).reset();
                    return httpMyPageRootViewRepo(context: context);
                  });
                },
                behavior: HitTestBehavior.translucent,
                child: Container(
                  width: 88,
                  height: 23,
                  child: Image.asset(
                    "assets/button/edit_profile.png",
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            )
          ],
        )
    );
  }
}

class _MyPageSubcribingCelebs extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return HeaderAndBodyComponent(
      headerTitle: "구독중인 셀럽",
      addMoreButton: true,
      moreButtonTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
          .get(MyPageSubRoutes.myPageRoot)
          .jumpToPage(MyPageSubPages.mySubscribes.index),
      child: Container(
        height: 116,
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: () {
              List<Widget> children = [];
              children.add(SizedBox(width: 32,));
              children.addAll(Provider.of<MyPageViewProvider>(context).celebList?.map((e) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.25),
                child: GestureDetector(
                  onTap:(){
                    Provider.of<MainNavigationProvider>(context,listen: false)
                        .setAll(
                      title: e.name,
                      leftButton: BarLeftButton.back,
                      rightButton: BarRightButton.none,
                      rightFunction: dummyFunction,
                    );
                    Provider.of<SelectedCelebProvider>(context, listen: false).celebId = e.celebId;
                    Provider.of<SelectedCelebProvider>(context, listen: false).celebName = e.name;
                    Provider.of<SubViewControllerProvider>(context, listen: false).get(MyPageSubRoutes.myPageRoot).jumpToPage(MyPageSubPages.community.index);
                  },
                  child: SubscribingCelebIcons.buildWith(
                      UserAvatarData(
                          userName: e.name,
                          imgUrl: e.profileImgUrl,
                          badgeUrl: e.badgeUrl
                      )
                  ),
                ),
              ))?.toList());
              children.add(SizedBox(width: 32,));
              return children;
            }.call(),
//            children: <Widget>[
//              SizedBox(
//                width: 47.1,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 14.5,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 14.5,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 14.5,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 14.5,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 14.5,
//              ),
//              SubscribingCelebIcons.buildWith(UserAvatarData(userName: "윤도현")),
//              SizedBox(
//                width: 47.1,
//              ),
//            ],
          ),
        ),
      ),
    );
  }
}

class _ScrapBoardStubDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HeaderAndBodyComponent(
        headerTitle: "나의 스크랩",
        addMoreButton: true,
        moreButtonTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
            .get(MyPageSubRoutes.myPageRoot)
            .jumpToPage(MyPageSubPages.myScrap.index),
        child: ScrapBoardGrid(
          dataList: Provider.of<MyPageViewProvider>(context).boardData == null ? [] : Provider.of<MyPageViewProvider>(context).boardData
//          dataList: [
//            ScrapBoardCardData(
//              name: "윤도현 사진 모음",
//              datetimeString: "2020.05.31",
//            ),
//            ScrapBoardCardData(
//              name: "윤도현 사진 모음 윤도현 사진 모음 ",
//              datetimeString: "2020.06.01",
//            )
//          ],
        )
    );
  }
}

class _UsingServicesDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HeaderAndBodyComponent(
        headerTitle: "이용중인 서비스",
        child: NextPageBarHolder(
          childrenValues: [
//            NextPageBarData(
//                text: "모닝콜",
//                onTap: () => showGeneralDialog(
//                  context: context,
//                  pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
//                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
//                  barrierDismissible: false,
//                  transitionDuration: const Duration(milliseconds: 150),)
//            ),
            NextPageBarData(
              text: "TTS",
              onTap: () => Navigator.pushNamed(context, kRouteTtsService)
            ),
            NextPageBarData(
                text: "맴버십",
                onTap: () => Navigator.pushNamed(context, kRouteMembershipManage)
//                    showGeneralDialog(
//                  context: context,
//                  pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
//                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
//                  barrierDismissible: false,
//                  transitionDuration: const Duration(milliseconds: 150),)
            )
          ],
        )
    );
  }
}


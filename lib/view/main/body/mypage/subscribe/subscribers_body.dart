

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/home/communitiy/home_community_body.dart';
import '../my_page_sub_routes.dart';
class MySubscribes extends StatefulWidget {
  @override
  _MySubscribesState createState() => _MySubscribesState();
}

class _MySubscribesState extends State<MySubscribes> {
  final PageController _pageController = PageController(
      keepPage: false
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<SubViewControllerProvider>(context, listen: false).add(
          MyPageSubRoutes.myPageSubscribes, _pageController
      );
    });
    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        switch(MyPageSubscribeSubPages.values.elementAt(index)){
          case MyPageSubscribeSubPages.root:
            return SubMySubScribes();
          case MyPageSubscribeSubPages.community:
            return HomeCommunityBody(previousPage: "subscribe");
          default :
            throw UnsupportedError("unsupported view page!");
        }
      },
      itemCount: MyPageSubPages.values.length,
    );
  }
}


class SubMySubScribes extends StatefulWidget {
  @override
  _SubMySubScribesState createState() => _SubMySubScribesState();
}

class _SubMySubScribesState extends State<SubMySubScribes> {
  Future<bool> _getData() async {
    return await httpMyPageCelebList(context: context);
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((e) =>
        Provider.of<MainNavigationProvider>(context, listen: false).setAll(
            title: "셀럽 리스트",
            leftButton: BarLeftButton.back,
            rightButton: BarRightButton.none,
            leftFunction: mySubLeftBackButton(context),
            rightFunction: dummyFunction
        )
    );
    BackButtonInterceptor.add(mySubRootBackInterceptor(context), name:"subscribers", zIndex: 2);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );

        return Container(
          child: Provider.of<MyPageViewProvider>(context).celebViewData.isEmpty ? Container(child: Center(child: Text("나만의 셀럽을 찾아 구독해보세요!"),),) : SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                HeaderAndBodyComponent(
                  headerTitle: "구독중",
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 2),
                            color: Color.fromRGBO(0, 0, 0, 0.09),
                            blurRadius: 8,
                          )
                        ]
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: Provider.of<MyPageViewProvider>(context).celebViewData.map((e) => GestureDetector(
                        onTap: () {
                          Provider.of<MainNavigationProvider>(context, listen: false)
                              .setAll(
                            title: e.name,
                            leftButton: BarLeftButton.back,
                            rightButton: BarRightButton.none,
                            rightFunction: dummyFunction,
                          );
                          Provider.of<SelectedCelebProvider>(context, listen: false).celebId = e.celebId;
                          Provider.of<SelectedCelebProvider>(context, listen: false).celebName = e.name;
                          Provider.of<SubViewControllerProvider>(context, listen: false)
                              .get(MyPageSubRoutes.myPageSubscribes)
                              .jumpToPage(MyPageSubscribeSubPages.community.index);
                        },
                        child: UserListItem(
                          userName: e.name, imgUrl: e.profileImgUrl, badgeUrl: e.badgeUrl,
                        ),
                      )).toList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("subscribers");
    super.dispose();
  }
}
enum MyPageSubscribeSubPages{
  root,
  community
}
bool Function(bool, RouteInfo) mySubRootBackInterceptor(BuildContext context){
  return (pop, _) {
    if(pop) return false;
    PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false).get(MyPageSubRoutes.myPageSubscribes);
    // ignore: invalid_use_of_protected_member
    if(parentController.positions.isEmpty) return false;
    else if(parentController.page != 0.0){
      parentController.jumpToPage(0);
      return true;
    } else{
      Provider.of<SubViewControllerProvider>(context, listen: false).get(MyPageSubRoutes.myPageRoot).jumpToPage(0);
      return true;
    }
  };
}

VoidCallback mySubLeftBackButton(BuildContext context) {
  return () {
    PageController parentController = Provider.of<SubViewControllerProvider>(
        context, listen: false).get(MyPageSubRoutes.myPageSubscribes);
    // ignore: invalid_use_of_protected_member
    if (parentController.positions.isEmpty)
      return false;
    else if (parentController.page != 0.0) {
      parentController.jumpToPage(0);
      return true;
    } else {
      Provider.of<SubViewControllerProvider>(context, listen: false).get(
          MyPageSubRoutes.myPageRoot).jumpToPage(0);
      return true;
    }
  };
}
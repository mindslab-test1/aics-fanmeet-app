
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/main/body/home/communitiy/home_community_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/followers/my_followers_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/root/mypage_root_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/scrap/scrap_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/subscribe/subscribers_body.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'subscribe/subscribers_body.dart';

class MyPageBody extends StatelessWidget {
  final PageController _pageController = PageController(
    keepPage: false
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<SubViewControllerProvider>(context, listen: false).add(
          MyPageSubRoutes.myPageRoot, _pageController
      );
    });

    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        switch(MyPageSubPages.values.elementAt(index)){
          case MyPageSubPages.root:
            return MyPageRootBody();
          case MyPageSubPages.settings:
            return SettingBody();
//          case MyPageSubPages.editProfile:
//            return _TempProfileEdit();
          case MyPageSubPages.myFollowers:
            return MyFollowersBody();
//          case MyPageSubPages.myLevel:
//            return _TempMyLevel();
          case MyPageSubPages.mySubscribes:
            return MySubscribes();
//          case MyPageSubPages.myActivity:
//            return _TempMyActivities();
          case MyPageSubPages.myScrap:
            return MyScrapBoardBody();
          case MyPageSubPages.community:
            return HomeCommunityBody(previousPage: "myPage");
//          case MyPageSubPages.tts:
//            return _TempMyTtsService();
          default:
            throw UnsupportedError("unsupported view page!");
        }
      },
      itemCount: MyPageSubPages.values.length,
    );
  }
}

//class _TempProfileEdit extends StatelessWidget {
//
//  @override
//  Widget build(BuildContext context) {
//    BackButtonInterceptor.add((pop) {
//      if(pop) return false;
//      PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
//          .get(MyPageSubRoutes.myPageRoot);
//      // ignore: invalid_use_of_protected_member
//      if(parentController.positions.isEmpty) return false;
//      else if(parentController.page != 0.0){
//        parentController.jumpToPage(0);
//        return true;
//      } else return false;
//    });
//
//    return Container(
//      child: Center(
//        child: Text("temp edit profile"),
//      ),
//    );
//  }
//}
//
//class _TempMyFollowers extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    BackButtonInterceptor.add((pop) {
//      if(pop) return false;
//      PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
//          .get(MyPageSubRoutes.myPageRoot);
//      // ignore: invalid_use_of_protected_member
//      if(parentController.positions.isEmpty) return false;
//      else if(parentController.page != 0.0){
//        parentController.jumpToPage(0);
//        return true;
//      } else return false;
//    });
//
//    return Container(
//      child: Center(
//        child: Text("temp followers"),
//      ),
//    );
//  }
//}
//
//class _TempMyLevel extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    BackButtonInterceptor.add((pop) {
//      if(pop) return false;
//      PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
//          .get(MyPageSubRoutes.myPageRoot);
//      // ignore: invalid_use_of_protected_member
//      if(parentController.positions.isEmpty) return false;
//      else if(parentController.page != 0.0){
//        parentController.jumpToPage(0);
//        return true;
//      } else return false;
//    });
//
//    return Container(
//      child: Center(
//        child: Text("temp level"),
//      ),
//    );
//  }
//}
//
//class _TempMyTtsService extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    BackButtonInterceptor.add((pop) {
//      if(pop) return false;
//      PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
//          .get(MyPageSubRoutes.myPageRoot);
//      // ignore: invalid_use_of_protected_member
//      if(parentController.positions.isEmpty) return false;
//      else if(parentController.page != 0.0){
//        parentController.jumpToPage(0);
//        return true;
//      } else return false;
//    });
//
//    return Container(
//      child: Center(
//        child: Text("temp tts service"),
//      ),
//    );
//  }
//}
//
//class _TempMyActivities extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    BackButtonInterceptor.add((pop) {
//      if(pop) return false;
//      PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
//          .get(MyPageSubRoutes.myPageRoot);
//      // ignore: invalid_use_of_protected_member
//      if(parentController.positions.isEmpty) return false;
//      else if(parentController.page != 0.0){
//        parentController.jumpToPage(0);
//        return true;
//      } else return false;
//    });
//
//    return Container(
//      child: Center(
//        child: Text("temp my activities"),
//      ),
//    );
//  }
//}

bool Function(bool, RouteInfo) myPageRootBackInterceptor(BuildContext context){
  return (pop, _) {
    if(pop) return false;
    PageController parentController = Provider.of<SubViewControllerProvider>(context, listen: false)
        .get(MyPageSubRoutes.myPageRoot);
    // ignore: invalid_use_of_protected_member
    if(parentController.positions.isEmpty) return false;
    else if(parentController.page != 0.0){
      parentController.jumpToPage(0);
      return true;
    } else return false;
  };
}

VoidCallback myPageLeftBackButton(BuildContext context){
  return () => Provider.of<SubViewControllerProvider>(context, listen: false)
      .get(MyPageSubRoutes.myPageRoot).jumpToPage(0);
}

enum MyPageSubPages{
  root,
//  editProfile,
  settings,
  myFollowers,
//  myLevel,
  mySubscribes,
//  myActivity,
  myScrap,
//  tts
  community
}
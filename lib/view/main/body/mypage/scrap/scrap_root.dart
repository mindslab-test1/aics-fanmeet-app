

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/component/scrap_board_component.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

import '../my_page_body.dart';

class ScrapRootView extends StatefulWidget {
  final List<ScrapBoardCardData> dataList;

  const ScrapRootView({Key key, this.dataList}) : super(key: key);

  @override
  _ScrapRootViewState createState() => _ScrapRootViewState();
}

class _ScrapRootViewState extends State<ScrapRootView> {
  bool _isEditState;

  @override
  void initState() {
    super.initState();
    _isEditState = false;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      MainNavigationProvider mainNavProv = Provider.of<MainNavigationProvider>(context, listen: false);

      mainNavProv.setAll(
          title: "나의 스크랩",
          leftButton: BarLeftButton.back,
          rightButton: _isEditState ? BarRightButton.done : BarRightButton.edit,
          leftFunction: myPageLeftBackButton(context),
          rightFunction: () => setState(() => _isEditState = !_isEditState)
      );
    });

    return Container(
      child: ScrapBoardGrid(
        editState: _isEditState,
        scrollPhysics: ClampingScrollPhysics(),
        dataList: widget.dataList,
      )
    );
  }
}

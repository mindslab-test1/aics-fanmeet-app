

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

import '../../../../main.dart';

class SearchBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      MainNavigationProvider mainNavProv = Provider.of<MainNavigationProvider>(context, listen: false);
      mainNavProv.setAll(
        title: "",
        leftButton: BarLeftButton.none,
        rightButton: BarRightButton.none,
        leftFunction: () => mainNavProv.searchState = false,
        rightFunction: toDo
      );
    });

    return Container(
      child: Center(
          child: Text("search temp view")
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/celeb_my_page_tts_list_approved.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/celeb_my_page_tts_list_received.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_tts_list_approved_item_dto.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_tts_list_received_item_dto.dart';
import 'package:rf_tap_fanseem/providers/celeb_tts_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_requested_view.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_accept_view.dart';

class TtsVoiceBody extends StatelessWidget {
  Future<bool> _getData(BuildContext context) async {


    List<HttpCelebMyPageTtsListReceivedItemDto> requestedDataRaw = await httpCelebMyPageTtsListReceived(context: context);
    List<HttpCelebMyPageTtsListApprovedItemDto> acceptedDataRaw = await httpCelebMyPageTtsListApproved(context: context);

    List<RequestedCelebTtsData> requestedData = [];
    List<AcceptedCelebTtsData> acceptedData = [];

    requestedDataRaw.forEach((element) {
      requestedData.add(RequestedCelebTtsData(
        tts: element.ttsId,
        subscriberName: element.subscriberName,
        text: element.text,
        timestamp: element.timestamp,
        ttsUrl: element.ttsUrl
      ));
    });

    acceptedDataRaw.forEach((element) {
      acceptedData.add(AcceptedCelebTtsData(
        tts: element.tts,
        subscriberName: element.subscriberName,
        text: element.text,
        timestamp: element.timestamp,
        ttsUrl: element.ttsUrl
      ));
    });


    Provider.of<CelebTtsProvider>(context, listen: false).setDisplayList(requestedData, acceptedData);

    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
          future: _getData(context),
          builder: (context, snapshot) {
            if(snapshot.hasData)
              return DefaultTabController(
                initialIndex: 0,
                length: 2,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.05),
                                blurRadius: 11,
                                offset: Offset(0, 3)
                            )
                          ]
                      ),
                      child: TabBar(
                        indicatorColor: Theme.of(context).primaryColor,
                        labelColor: Theme.of(context).primaryColor,
                        labelStyle: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontSize: 17,
                            fontWeight: FontWeight.bold
                        ),
                        unselectedLabelColor: kNavigationPressableColor,
                        unselectedLabelStyle: TextStyle(
                            fontSize: 15
                        ),
                        tabs: <Widget>[
                          Tab(child: Text("요청 받은 목록")),
                          Tab(child: Text("승인한 TTS 목록")),
                        ],
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        physics: const NeverScrollableScrollPhysics(),
                        children: <Widget>[
                          TtsRequestedList(
                          ),
                          TtsAcceptedList(
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            else if (snapshot.hasError) {
              return Center(
                child: Text(
                  '문제가 발생하였습니다...',
                  style: TextStyle(fontSize: 15),
                ),
              );
            }
            else
              return Center(child: CircularProgressIndicator());
          }
      ),
    );
  }
}

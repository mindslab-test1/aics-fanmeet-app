import 'package:flutter/material.dart';
import 'dart:io';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/login/login_function.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/notice_dialog.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';

class MyPageCelebRootBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      MainNavigationProvider provider = Provider.of<MainNavigationProvider>(context, listen: false);
      provider.setAll(
        title: "Celeb No.1" + "님",
        leftButton: BarLeftButton.back,
        leftFunction: () {
          provider.navigateRoot(0, context);
          provider.pageController.jumpToPage(0);
        },
      );
    });

    return Container(
        color: Color.fromRGBO(247, 247, 247, 1),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              _MyPageProfileStack(
                userName:
                Provider.of<MyPageEditProvider>(context).name == null ?
                Provider.of<MyPageViewProvider>(context).name : Provider.of<MyPageEditProvider>(context).name,
              ),
              Divider(
                height: 0.1,
                indent: 14.5,
                endIndent: 14.5,
                thickness: 0.5,
              ),
              _MyInformationDisplay(),
              _ServiceManagementDisplay()
            ],
          ),
        )
    );
  }
}

class _MyPageProfileStack extends StatelessWidget {
  final String userName;
  final String profileImgUrl;
  final String coverImgUrl;
  final int followerCnt;
  final int followingCnt;
  final int level;

  const _MyPageProfileStack({
    Key key,
    this.followerCnt = 0,
    this.followingCnt = 0,
    this.level = 1,
    this.userName,
    this.profileImgUrl,
    this.coverImgUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 195,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.1),
                  blurRadius: 3,
                  offset: Offset(0, 2)
              )
            ]
        ),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            // info & cover image
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 120,
                  width: double.infinity,
                  child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                        Color.fromRGBO(0, 0, 0, 0.55),
                        BlendMode.darken
                    ),
                    child: coverImgUrl == null ? Container(
                      decoration: BoxDecoration(
                          color: kMainColor
                      ),
                      child: Image.asset(
                        "assets/logo/splash_X3.png",
                        fit: BoxFit.fitWidth,
                      ),
                    ) : Image.network(coverImgUrl, fit: BoxFit.fitWidth),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 50,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "구독자",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                              ),
                            ),
                            Text(
                              "$followerCnt",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 50,
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    height: 20,
                                    child: Text(
                                      userName,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 17,
                                          fontWeight: FontWeight.bold
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "피드",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15
                              ),
                            ),
                            Text(
                              "$followerCnt",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
            // CircleAvatar
            Positioned(
              top: 46,
              left: MediaQuery.of(context).size.width / 2 - 50,
              child: Align(
                child: Container(
                  width: 100,
                  height: 100,
                  child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Theme.of(context).backgroundColor,
                      child: CircleAvatar(
                        radius: 49,
                        backgroundColor: kMainColor,
                        backgroundImage: profileImgUrl == null ? AssetImage(
                          "assets/icon/seem_off_3x.png",
                        ) : NetworkImage(profileImgUrl),
                      )
                  ),
                ),
              ),
            ),
            // Edit Profile Button
            Positioned(
              top: 15,
              right: 17,
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, kRouteEditProfile).then((value) {
                    Provider.of<MyPageEditProvider>(context, listen: false).reset();
                    return httpMyPageRootViewRepo(context: context);
                  });
                },
                behavior: HitTestBehavior.translucent,
                child: Container(
                  width: 88,
                  height: 23,
                  child: Image.asset(
                    "assets/button/edit_profile.png",
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
            )
          ],
        )
    );
  }
}

class _MyInformationDisplay extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return HeaderAndBodyComponent(
        headerTitle: "내 정보",
        child: NextPageBarHolder(
          childrenValues: [
            if(Platform.isAndroid)NextPageBarData(
                text: "내 프로필",
                onTap: () => Navigator.pushNamed(context, kRouteCelebProfile)
            ),
            NextPageBarData(
                text: "멤버십",
                onTap: () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(microseconds: 150)
                )
            ),
            NextPageBarData(
                text: "로그아웃",
                onTap: () async {
                  bool isSuccess = await logout(context);
                  if (isSuccess) {
                    Provider.of<MainNavigationProvider>(context, listen: false).navigateRoot(0, context);
                    Fluttertoast.showToast(msg: "로그아웃 되었습니다.", gravity: ToastGravity.TOP);
                    Navigator.pushNamedAndRemoveUntil(context, kRouteSplash, (route) => false);
                  }
                  else {
                    Fluttertoast.showToast(msg: "로그아웃에 실패했습니다.", gravity: ToastGravity.TOP);
                  }
                },
                textStyle: TextStyle(
                    fontSize: 13,
                    color:  kNegativeTextColor
                )
            )
          ],
        )
    );
  }
}

class _ServiceManagementDisplay extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return HeaderAndBodyComponent(
        headerTitle: "제공 서비스 관리",
        child: NextPageBarHolder(
          childrenValues: [
            if(Platform.isAndroid) NextPageBarData(
                text: "모닝콜",
                onTap: () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150)
                )
            ),
            NextPageBarData(
                text: "TTS 보이스",
                onTap: () => Navigator.pushNamed(context, kRouteTtsAuth)
            ),
            NextPageBarData(
                text: "메세지 보내기",
                onTap: () async {
                  Provider.of<MessagingDisplayProvider>(context, listen: false).reset();
                  Provider.of<MessagingDisplayProvider>(context, listen: false).setMessagingView(
                      MessagingType.celebSend,
                      "메세지 보내기",
                      Provider.of<LoginVerifyProvider>(context, listen: false).userId
                  );
                  await Navigator.pushNamed(context, kRouteMessaging);
                }
//                    showGeneralDialog(
//                    context: context,
//                    pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
//                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
//                    barrierDismissible: false,
//                    transitionDuration: const Duration(microseconds: 150)
//                )
            ),
            if(Platform.isAndroid) NextPageBarData(
                text: "커뮤니티",
                onTap: () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => NoticePrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(microseconds: 150)
                )
            )
          ],
        )
    );
  }
}
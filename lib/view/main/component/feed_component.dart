
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_like.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_unlike.dart';
import 'package:rf_tap_fanseem/http/content/content_like.dart';
import 'package:rf_tap_fanseem/http/content/content_unlike.dart';
import 'package:rf_tap_fanseem/http/feed/feed_like.dart';
import 'package:rf_tap_fanseem/http/feed/feed_unlike.dart';
import 'package:rf_tap_fanseem/http/scrap/scrap_feed.dart';
import 'package:rf_tap_fanseem/http/scrap/scrap_view.dart';
import 'package:rf_tap_fanseem/providers/comment_reply_provider.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_body.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/dialog/login_require_dialog.dart';

import 'package:rf_tap_fanseem/view/main/dialog/membership_require_dialog.dart';
import 'package:rf_tap_fanseem/view/main/dialog/scrap_dialog.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

import '../../../colors.dart';
import '../dialog/membership_require_dialog.dart';

class FeedData {
  final int userId;
  final int feedId;
  final String userName;
  final String profileImg;
  final String membershipBadgeUrl;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final bool isUserFeed;
  final bool isContent;
  final String title;
  final int viewCount;
  final String membership;
  final int accessLevel;
  final bool isEdited;
  final int categoryId;

  const FeedData({
    this.userId,
    this.feedId,
    this.userName = "Lorem Ipsum",
    this.profileImg,
    this.membershipBadgeUrl,
    this.postTimestamp = "1/01 AM 00:00",
    this.category = "일상",
    this.imgUrl = const [],
    this.videoUrl,
    this.youtubeUrl,
    this.text = "",
    this.scrapCount = 0,
    this.isScrap = false,
    this.likeCount = 0,
    this.isLike = false,
    this.commentCount = 0,
    this.canView = true,
    this.isUserFeed = true,
    this.isContent = false,
    this.title = "",
    this.viewCount = 0,
    this.membership = "",
    this.accessLevel = 0,
    this.isEdited = false,
    this.categoryId = 0
  });

  @override
  String toString() {
    return 'FeedData{userId: $userId, feedId: $feedId, userName: $userName, profileImg: $profileImg, membershipBadgeUrl: $membershipBadgeUrl, postTimestamp: $postTimestamp, category: $category, imgUrl: $imgUrl, videoUrl: $videoUrl, youtubeUrl: $youtubeUrl, text: $text, scrapCount: $scrapCount, isScrap: $isScrap, likeCount: $likeCount, isLike: $isLike, commentCount: $commentCount, canView: $canView, isUserFeed: $isUserFeed, isContent: $isContent, isEdited: $isEdited, accessLevel: $accessLevel}';
  }
}

class ContentFeedData{
  @override
  String toString() {
    return 'ContentFeedData{userId: $userId, feedId: $feedId, title: $title, viewCount: $viewCount, userName: $userName, profileImg: $profileImg, membership: $membership, postTimestamp: $postTimestamp, category: $category, imgUrl: $imgUrl, videoUrl: $videoUrl, text: $text, scrapCount: $scrapCount, isScrap: $isScrap, likeCount: $likeCount, isLike: $isLike, commentCount: $commentCount, canView: $canView}';
  }

  final int userId;
  final int feedId;
  final String title;
  final int viewCount;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final int accessLevel;
  final bool isEdited;
  final int categoryId;

  const ContentFeedData({
    Key key,
    this.userId,
    this.feedId,
    this.title,
    this.viewCount,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl = const [],
    this.videoUrl,
    this.youtubeUrl,
    this.text = "",
    this.scrapCount = 0,
    this.isScrap = false,
    this.likeCount = 0,
    this.isLike = false,
    this.commentCount = 0,
    this.canView = true,
    this.isEdited = false,
    this.accessLevel = 0,
    this.categoryId = 0
  });
}

class ScrapFeedData{
  final int userId;
  final int feedId;
  final String title;
  final int viewCount;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final ScrapKind scrapKind;
  final int accessLevel;
  final bool isEdited;

  const ScrapFeedData({
    Key key,
    this.userId,
    this.feedId,
    this.title,
    this.viewCount,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl = const [],
    this.videoUrl,
    this.youtubeUrl,
    this.text = "",
    this.scrapCount = 0,
    this.isScrap = false,
    this.likeCount = 0,
    this.isLike = false,
    this.commentCount = 0,
    this.canView = true,
    this.scrapKind,
    this.accessLevel = 0,
    this.isEdited = false,
  });
}

class ContentFeedElement extends StatelessWidget{
  final int userId;
  final int feedId;
  final String title;
  final int viewCount;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final int accessLevel;
  final bool isEdited;
  final int categoryId;

  const ContentFeedElement({
    Key key,
    this.userId,
    this.feedId,
    this.title="",
    this.viewCount,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl,
    this.videoUrl,
    this.youtubeUrl,
    this.text,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.canView,
    this.accessLevel,
    this.isEdited,
    this.categoryId
  }) : super(key: key);

  static ContentFeedElement buildWith(ContentFeedData data){
    return ContentFeedElement(
      userId: data.userId,
      feedId: data.feedId,
      title: data.title,
      viewCount: data.viewCount,
      userName: data.userName,
      profileImg: data.profileImg,
      membership: data.membership,
      postTimestamp: data.postTimestamp,
      category: data.category,
      imgUrl: data.imgUrl,
      videoUrl: data.videoUrl,
      youtubeUrl: data.youtubeUrl,
      text: data.text,
      scrapCount: data.scrapCount,
      isScrap: data.isScrap,
      likeCount: data.likeCount,
      isLike: data.isLike,
      commentCount: data.commentCount,
      canView: data.canView,
      accessLevel: data.accessLevel,
      isEdited: data.isEdited,
      categoryId: data.categoryId,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.circular(12),
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    blurRadius: 2,
                    offset: Offset(0, 2),
                  )
                ]
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      if(canView){
                        Provider.of<FeedDataProvider>(context, listen: false).feedData = FeedData(
                            feedId: this.feedId,
                            userId: this.userId,
                            title: this.title,
                            userName: this.userName,
                            profileImg: this.profileImg,
                            membershipBadgeUrl: this.membership,
                            postTimestamp: this.postTimestamp,
                            category: this.category,
                            imgUrl: this.imgUrl,
                            videoUrl: this.videoUrl,
                            youtubeUrl: this.youtubeUrl,
                            text: this.text,
                            scrapCount: this.scrapCount,
                            isScrap: this.isScrap,
                            likeCount: this.likeCount,
                            isLike: this.isLike,
                            commentCount: this.commentCount,
                            isContent: true,
                            isUserFeed: false,
                            isEdited: this.isEdited,
                            accessLevel: this.accessLevel,
                            categoryId: this.categoryId
                        );
                        Provider.of<FeedDataProvider>(context, listen: false).isContent = true;
                        Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = false;
                        Navigator.pushNamed(context, kRouteFeedDetail).then((value) {
                          Provider.of<FeedDataProvider>(context, listen: false).isContent = false;
                          Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = true;
                          Provider.of<CommentReplyProvider>(context, listen: false).reset();
                        });
                      }
                      else showGeneralDialog(
                        context: context,
                        pageBuilder: (context, animation, secondAnimation) => MembershipPrompt(
                          membershipName: Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                            return element.membershipTier == this.accessLevel;
                          }).membershipName,
                          accessLevel: this.accessLevel,
                        ),
                        barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                        barrierDismissible: false,
                        transitionDuration: const Duration(milliseconds: 150),
                      );
                    },
                    child: canView ? Container(
                      width: double.infinity,
                      height: 150,
                      child: (imgUrl == null || imgUrl?.length == 0) ? Container(
                        decoration: BoxDecoration(
                            color: kMainColor
                        ),
                        child: Center(
                          child: Image.asset(
                            "assets/logo/splash_X3.png",
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ) : Center(
                        child: CachedNetworkImage(
                          imageUrl: this.imgUrl[0],
                          fit: BoxFit.fitWidth,
                          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                      ),
                    ) : Container(
                      height: 80,
                      decoration: BoxDecoration(
                          color: kMainBackgroundColor
                      ),
                      child: Center(
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.network(
                              Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                                return element.membershipTier == this.accessLevel;
                              }).imageUrl,
                              fit: BoxFit.cover,
                              width: 40,
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Text(
                                Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                                  return element.membershipTier == this.accessLevel;
                                }).membershipName,
                                style: TextStyle(
                                    color: kMainColor,
                                    fontSize: 14,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.w700
                                )
                            ),
                            Text(
                                " 멤버십 이상 열람 가능",
                                style: TextStyle(
                                    color: kSelectableTextColor,
                                    fontSize: 14,
                                    fontFamily: "Roboto",
                                    fontWeight: FontWeight.w700
                                )
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    height: 29,
                    padding: EdgeInsets.only(left: 10, top: 8, bottom: 8),
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "조회수 ${viewCount == null ? 0 : viewCount}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 9,
                              color: kSelectableTextColor
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          "좋아요 ${likeCount == null ? 0 : likeCount}",
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 9,
                              color: kSelectableTextColor
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            title,
            textAlign: TextAlign.left,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                userName,
                style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 10
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                "$postTimestamp",
                style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 10
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class FeedElement extends StatelessWidget {
  final int userId;
  final int feedId;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final int accessLevel;
  final bool isEdited;

  const FeedElement({
    Key key,
    this.userId,
    this.feedId,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl,
    this.videoUrl,
    this.youtubeUrl,
    this.text,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.canView,
    this.accessLevel,
    this.isEdited
  }) : super(key: key);

  static FeedElement buildWith(FeedData data){
    return FeedElement(
      userId: data.userId,
      feedId: data.feedId,
      userName: data.userName,
      profileImg: data.profileImg,
      membership: data.membershipBadgeUrl,
      postTimestamp: data.postTimestamp,
      category: data.category,
      imgUrl: data.imgUrl,
      videoUrl: data.videoUrl,
      youtubeUrl: data.youtubeUrl,
      text: data.text,
      scrapCount: data.scrapCount,
      isScrap: data.isScrap,
      likeCount: data.likeCount,
      isLike: data.isLike,
      commentCount: data.commentCount,
      canView: data.canView,
      isEdited: data.isEdited,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 15),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border.all(
            color: Color.fromRGBO(228, 228, 228, 1),
            width: 0.5,
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.09),
              blurRadius: 8,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                UserFeedProfileLayer(
                  userName: this.userName,
                  profileImg: this.profileImg,
                  badgeUrl: this.membership,
                  category: this.category,
                  timestamp: this.postTimestamp,
                  isEdited: this.isEdited
                ),
                UserFeedGeneralMetaData(
                  category: this.category,
                ),
              ],
            ),
          ),
          SizedBox(height: 13,),
          canView ? GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Provider.of<FeedDataProvider>(context, listen: false).feedData = FeedData(
                  feedId: this.feedId,
                  userId: this.userId,
                  userName: this.userName,
                  profileImg: this.profileImg,
                  membershipBadgeUrl: this.membership,
                  postTimestamp: this.postTimestamp,
                  category: this.category,
                  imgUrl: this.imgUrl,
                  videoUrl: this.videoUrl,
                  youtubeUrl: this.youtubeUrl,
                  text: this.text,
                  scrapCount: this.scrapCount,
                  isScrap: this.isScrap,
                  likeCount: this.likeCount,
                  isLike: this.isLike,
                  commentCount: this.commentCount
              );
              Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = true;
              Navigator.pushNamed(context, kRouteFeedDetail).then((value) => Provider.of<CommentReplyProvider>(context, listen: false).reset());
            },
            child: _FeedContent(
              imgUrl: this.imgUrl,
              videoUrl: this.videoUrl,
              youtubeUrl: this.youtubeUrl,
              text: this.text,
            ),
          ) : Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Color.fromRGBO(228, 228, 228, 1),
                  height: 0.5,
                  thickness: 0.5,
                  indent: 14.5,
                  endIndent: 14.5,
                ),
                SizedBox(height: 14.5,),
                GestureDetector(
                  onTap: () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => MembershipPrompt(
                      membershipName: Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                        return element.membershipTier == this.accessLevel;
                      }).membershipName,
                      accessLevel: this.accessLevel,
                    ),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(width: 24,),
                        Container(
                          width: 15.8,
                          height: 18.1,
                          child: Icon(
                            FontAwesomeIcons.lock,
                            color: Colors.white,
                            size: 16,
                          ),
                        ),
                        SizedBox(
                          width: 9.3,
                        ),
                        Text(
                          "멤버십 전용",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    ),
                    width: 136,
                    height: 40,
                    decoration: BoxDecoration(
                        color: kFunctionButtonColor,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6,
                              offset: Offset(0, 3)
                          )
                        ]
                    ),
                  ),
                ),
                SizedBox(height: 14.5,)
              ],
            ),
          ),
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          FeedButtons(
            scrapCount: this.scrapCount,
            isScrap: this.isScrap,
            likeCount: this.likeCount,
            isLike: this.isLike,
            commentCount: this.commentCount,
            feedId: this.feedId,
            canView: this.canView,
            accessLevel: this.accessLevel,
          )
        ],
      ),
    );
  }
}

class CelebFeedElement extends StatelessWidget {
  final int userId;
  final int feedId;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final int accessLevel;
  final bool isEdited;

  const CelebFeedElement({
    Key key,
    this.userId,
    this.feedId,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl,
    this.videoUrl,
    this.youtubeUrl,
    this.text,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.canView,
    this.accessLevel,
    this.isEdited
  }) : super(key: key);

  static CelebFeedElement buildWith(FeedData data){
    return CelebFeedElement(
      userId: data.userId,
      feedId: data.feedId,
      userName: data.userName,
      profileImg: data.profileImg,
      membership: data.membershipBadgeUrl,
      postTimestamp: data.postTimestamp,
      category: data.category,
      imgUrl: data.imgUrl,
      videoUrl: data.videoUrl,
      youtubeUrl: data.youtubeUrl,
      text: data.text,
      scrapCount: data.scrapCount,
      isScrap: data.isScrap,
      likeCount: data.likeCount,
      isLike: data.isLike,
      commentCount: data.commentCount,
      canView: data.canView,
      accessLevel: data.accessLevel,
      isEdited: data.isEdited,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 15),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border.all(
            color: Color.fromRGBO(228, 228, 228, 1),
            width: 0.5,
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.09),
              blurRadius: 8,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                UserFeedProfileLayer(
                    userName: this.userName,
                    profileImg: this.profileImg,
                    badgeUrl: this.membership,
                    category: this.category,
                    timestamp: this.postTimestamp,
                    isEdited: this.isEdited
                ),
                UserFeedGeneralMetaData(
                  category: this.category,
                ),
              ],
            ),
          ),
          SizedBox(height: 13,),
          canView ? GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Provider.of<FeedDataProvider>(context, listen: false).feedData = FeedData(
                  feedId: this.feedId,
                  userId: this.userId,
                  userName: this.userName,
                  profileImg: this.profileImg,
                  membershipBadgeUrl: this.membership,
                  postTimestamp: this.postTimestamp,
                  category: this.category,
                  imgUrl: this.imgUrl,
                  videoUrl: this.videoUrl,
                  youtubeUrl: this.youtubeUrl,
                  text: this.text,
                  scrapCount: this.scrapCount,
                  isScrap: this.isScrap,
                  likeCount: this.likeCount,
                  isLike: this.isLike,
                  commentCount: this.commentCount,
                  isUserFeed: false,
                  isEdited: this.isEdited,
                  accessLevel: this.accessLevel,
              );
              Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = false;
              Navigator.pushNamed(context, kRouteFeedDetail).then((value) {
                Provider.of<CommentReplyProvider>(context, listen: false).reset();
                Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = true;
              });
            },
            child: _FeedContent(
              imgUrl: this.imgUrl,
              videoUrl: this.videoUrl,
              youtubeUrl: this.youtubeUrl,
              text: this.text,
            ),
          ) : Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Color.fromRGBO(228, 228, 228, 1),
                  height: 0.5,
                  thickness: 0.5,
                  indent: 14.5,
                  endIndent: 14.5,
                ),
                SizedBox(height: 14.5,),
                GestureDetector(
                  onTap: () => showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => MembershipPrompt(
                      membershipName: Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                        return element.membershipTier == this.accessLevel;
                      }).membershipName,
                      accessLevel: this.accessLevel,),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),),
                  behavior: HitTestBehavior.translucent,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.network(
                        Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                          return element.membershipTier == this.accessLevel;
                        }).imageUrl,
                        fit: BoxFit.cover,
                        width: 40,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                          return element.membershipTier == this.accessLevel;
                        }).membershipName,
                        style: TextStyle(
                          color: kMainColor,
                          fontSize: 14,
                          fontFamily: "Roboto",
                          fontWeight: FontWeight.w700
                        )
                      ),
                      Text(
                          " 멤버십 이상 열람 가능",
                          style: TextStyle(
                              color: kSelectableTextColor,
                              fontSize: 14,
                              fontFamily: "Roboto",
                              fontWeight: FontWeight.w700
                          )
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 14.5,)
              ],
            ),
          ),
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          CelebFeedButtons(
            scrapCount: this.scrapCount,
            isScrap: this.isScrap,
            likeCount: this.likeCount,
            isLike: this.isLike,
            commentCount: this.commentCount,
            feedId: this.feedId,
            canView: this.canView,
            accessLevel: this.accessLevel,
          )
        ],
      ),
    );
  }
}

class ScrapFeedElement extends StatelessWidget {
  final String title;
  final int feedId;
  final String userName;
  final String profileImg;
  final String membership;
  final String postTimestamp;
  final String category;
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final bool canView;
  final ScrapKind scrapKind;
  final int accessLevel;
  final bool isEdited;

  const ScrapFeedElement({
    Key key,
    this.title,
    this.feedId,
    this.userName,
    this.profileImg,
    this.membership,
    this.postTimestamp,
    this.category,
    this.imgUrl,
    this.videoUrl,
    this.youtubeUrl,
    this.text,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.canView,
    this.scrapKind,
    this.accessLevel,
    this.isEdited
  }) : super(key: key);

  static ScrapFeedElement buildWith(ScrapFeedData data){
    return ScrapFeedElement(
        title: data.title,
        feedId: data.feedId,
        userName: data.userName,
        profileImg: data.profileImg,
        membership: data.membership,
        postTimestamp: data.postTimestamp,
        category: data.category,
        imgUrl: data.imgUrl,
        videoUrl: data.videoUrl,
        youtubeUrl: data.youtubeUrl,
        text: data.text,
        scrapCount: data.scrapCount,
        isScrap: data.isScrap,
        likeCount: data.likeCount,
        isLike: data.isLike,
        commentCount: data.commentCount,
        canView: data.canView,
        scrapKind: data.scrapKind,
        accessLevel: data.accessLevel,
        isEdited: data.isEdited,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 15),
      width: double.infinity,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border: Border.all(
            color: Color.fromRGBO(228, 228, 228, 1),
            width: 0.5,
          ),
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.09),
              blurRadius: 8,
              offset: Offset(0, 2),
            ),
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FeedProfileLayer(
                  userName: this.userName == null ? "" : this.userName,
                  profileImg: this.profileImg,
                  badgeUrl: this.membership,
                ),
                FeedGeneralMetaData(
                  timestamp: this.postTimestamp,
                  category: this.category,
                ),
              ],
            ),
          ),
          title != null ? Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ) : Container(),
          title != null ? SizedBox(height: 13,) : Container(),
          title != null ? Container(
            padding: const EdgeInsets.symmetric(horizontal: 45),
            child: Text(
              title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
            ),
          ) : Container(),
          SizedBox(height: 13,),
          canView ? GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Provider.of<FeedDataProvider>(context, listen: false).feedData = FeedData(
                  feedId: this.feedId,
                  userName: this.userName,
                  profileImg: this.profileImg,
                  membershipBadgeUrl: this.membership,
                  postTimestamp: this.postTimestamp,
                  category: this.category,
                  imgUrl: this.imgUrl,
                  videoUrl: this.videoUrl,
                  youtubeUrl: this.youtubeUrl,
                  text: this.text,
                  scrapCount: this.scrapCount,
                  isScrap: this.isScrap,
                  likeCount: this.likeCount,
                  isLike: this.isLike,
                  commentCount: this.commentCount,
                  accessLevel: this.accessLevel,
                  isEdited: this.isEdited

              );
              switch(this.scrapKind){
                case ScrapKind.userFeed:
                  Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = true;
                  Provider.of<FeedDataProvider>(context, listen: false).isContent = false;
                  break;
                case ScrapKind.celebFeed:
                  Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = false;
                  Provider.of<FeedDataProvider>(context, listen: false).isContent = false;
                  break;
                case ScrapKind.content:
                  Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = false;
                  Provider.of<FeedDataProvider>(context, listen: false).isContent = true;
                  break;
              }
              Navigator.pushNamed(context, kRouteFeedDetail).then((value) {
                Provider.of<CommentReplyProvider>(context, listen: false).reset();
                Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = true;
                Provider.of<FeedDataProvider>(context, listen: false).isContent = false;
              });
            },
            child: _FeedContent(
              imgUrl: this.imgUrl,
              videoUrl: this.videoUrl,
              youtubeUrl: this.youtubeUrl,
              text: this.text,
            ),
          ) : Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Divider(
                  color: Color.fromRGBO(228, 228, 228, 1),
                  height: 0.5,
                  thickness: 0.5,
                  indent: 14.5,
                  endIndent: 14.5,
                ),
                SizedBox(height: 14.5,),
                GestureDetector(
//                  onTap: () => Fluttertoast.showToast(msg: "TODO", toastLength: Toast.LENGTH_SHORT),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(width: 24,),
                        Container(
                          width: 15.8,
                          height: 18.1,
                          child: Icon(
                            FontAwesomeIcons.lock,
                            color: Colors.white,
                            size: 16,
                          ),
                        ),
                        SizedBox(
                          width: 9.3,
                        ),
                        Text(
                          "멤버십 전용",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 12,
                              fontWeight: FontWeight.bold
                          ),
                        )
                      ],
                    ),
                    width: 136,
                    height: 40,
                    decoration: BoxDecoration(
                        color: kFunctionButtonColor,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6,
                              offset: Offset(0, 3)
                          )
                        ]
                    ),
                  ),
                ),
                SizedBox(height: 14.5,)
              ],
            ),
          ),
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          FeedButtons(
            scrapCount: this.scrapCount,
            isScrap: this.isScrap,
            likeCount: this.likeCount,
            isLike: this.isLike,
            commentCount: this.commentCount,
            canView: this.canView,
            accessLevel: this.accessLevel,
          )
        ],
      ),
    );
  }
}
class UserFeedProfileLayer extends StatelessWidget {
  final bool isRestrict;
  final String userName;
  final String profileImg;
  final String badgeUrl;
  final String category;
  final String timestamp;
  final bool isEdited;

  const UserFeedProfileLayer({
    Key key,
    this.isRestrict = false,
    this.userName,
    this.profileImg,
    this.badgeUrl,
    this.category,
    this.timestamp,
    this.isEdited
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 17.5,
            backgroundColor: kMainColor,
            backgroundImage: profileImg == null || profileImg == "" ?
            AssetImage("assets/icon/seem_off_3x.png",) :
            CachedNetworkImageProvider(profileImg,),
          ),
          SizedBox(width: 10,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
            children:[
              Row(
                children:[
                  Text(
                    userName,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                    textAlign: TextAlign.start,
                  ),
                  SizedBox(width: 4,),
                  badgeUrl == null ? Container() :
                  Container(
                      width: 17,
                      height: 17,
                      child: Image.network(badgeUrl, fit: BoxFit.contain,)
                  ),
                ]
              ),
              Text(
                formattedTime(timestamp)+(isEdited?" ( 수정됨 )":""),
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 12,
                  fontWeight: FontWeight.w500
                ),
                textAlign: TextAlign.start,
              )
            ]
          ),
        ],
      ),
    );
  }
}

class FeedProfileLayer extends StatelessWidget {
  final bool isRestrict;
  final String userName;
  final String profileImg;
  final String badgeUrl;
  final String category;

  const FeedProfileLayer({
    Key key,
    this.isRestrict = false,
    this.userName,
    this.profileImg,
    this.badgeUrl,
    this.category
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 17.5,
            backgroundColor: kMainColor,
            backgroundImage: profileImg == null || profileImg == "" ?
            AssetImage("assets/icon/seem_off_3x.png",) :
            CachedNetworkImageProvider(profileImg,),
          ),
          SizedBox(width: 10,),
          Container(
            width: 183,
            child: Text(
              userName,
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(width: 10,),
          badgeUrl == null ? Container() :
          Container(
              width: 17,
              height: 17,
              child: Image.network(badgeUrl, fit: BoxFit.contain,)
          ),
          isRestrict ? SizedBox(width: 5,) : Container(),
          (badgeUrl != null && isRestrict) ? Container(
            child: Text(
              "ONLY",
              style: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.bold,
                  color: Theme.of(context).primaryColor
              ),
            ),
          ) : Container()
        ],
      ),
    );
  }
}

class FeedGeneralMetaData extends StatelessWidget {
  final String timestamp;
  final String category;

  const FeedGeneralMetaData({
    Key key,
    this.timestamp = "1/01 AM 00:00",
    this.category = "일상"
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 36,
      width: 84,
      child: Text(
        "${formattedTime(timestamp)}\n#$category",
        maxLines: 2,
        textAlign: TextAlign.end,
        style: TextStyle(
            color: kNavigationPressableColor,
            fontSize: 12
        ),
      ),
    );
  }
}

class UserFeedGeneralMetaData extends StatelessWidget {
  final String category;

  const UserFeedGeneralMetaData({
    Key key,
    this.category = "일상"
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 31,
      child: Text(
        "#$category",
        maxLines: 2,
        textAlign: TextAlign.end,
        style: TextStyle(
            color: kNavigationPressableColor,
            fontSize: 12
        ),
      ),
    );
  }
}

class _FeedContent extends StatelessWidget {
  final List<String> imgUrl;
  final String videoUrl;
  final String youtubeUrl;
  final String text;

  const _FeedContent({
    Key key,
    this.imgUrl,
    this.videoUrl,
    this.youtubeUrl,
    this.text
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget display;
    if(imgUrl != null && imgUrl?.length != 0) display = Container(
      padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
      height: 161,
//      width: MediaQuery.of(context).size.width - 60,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          CachedNetworkImage(
            imageUrl: imgUrl[0],
            fit: BoxFit.fitWidth,
            placeholder: (context, url) => Center(child: CircularProgressIndicator()),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          imgUrl.length > 1 ? Positioned(
            bottom: 10,
            right: 10,
            child: Text(
              "+ ${imgUrl.length - 1}",
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ) : Container()
        ],
      ),
    );
    else if(videoUrl != null) display = Container(
      padding: const EdgeInsets.only(top: 15, left: 30, right: 30),
      height: 161,
      decoration: BoxDecoration(
          color: Colors.black
      ),
      child: Center(child: Icon(
        Icons.play_circle_outline,
        size: 50,
        color: Colors.white,
      ),),
    );
    else if(youtubeUrl != null) display = Container(
      padding: const EdgeInsets.only(top:15 ,left: 30, right: 30),
      height: 161,
//      decoration: BoxDecoration(
//          color: Colors.black
//      ),
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Center(child: Image.network(
            YoutubePlayer.getThumbnail(
                videoId: YoutubePlayer.convertUrlToId(youtubeUrl)
            ),
            fit: BoxFit.fill,
          )),
          Center(child: Icon(
            FontAwesomeIcons.youtube,
            size: 50,
            color: Colors.white,
          ),),
        ],
      ),
    );
    else display = Container();
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          Center(child: display),
          Container(
            padding: const EdgeInsets.symmetric(
                horizontal: 45,
                vertical: 15
            ),
            child: Text(
              text == null ? "" : text,
              textAlign: TextAlign.left,
              overflow: TextOverflow.ellipsis,
//              softWrap: true,
              maxLines: 3,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 13
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class FeedButtons extends StatefulWidget {
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final int feedId;
  final bool canView;
  final int accessLevel;

  const FeedButtons({
    Key key,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.feedId,
    this.canView, this.accessLevel
  }) : super(key: key);

  @override
  _FeedButtonsState createState() => _FeedButtonsState();
}

class _FeedButtonsState extends State<FeedButtons> {
  int _scrapCount;
  bool _isScrap;
  int _likeCount;
  bool _isLike;
  int _commentCount;
  int _feedId;
  bool _canView;
  int _accessLevel;

  @override
  void initState() {
    super.initState();
    this._scrapCount = widget.scrapCount;
    this._isScrap = widget.isScrap;
    this._likeCount = widget.likeCount;
    this._isLike = widget.isLike;
    this._commentCount = widget.commentCount;
    this._feedId = widget.feedId;
    this._canView = widget.canView;
    this._accessLevel = widget.accessLevel;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      height: 48,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () => _canView ? showGeneralDialog(
                  context: context,
                  pageBuilder: (context, animation, secondAnimation) => ScrapDialog(),
                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                  barrierDismissible: false,
                  transitionDuration: const Duration(milliseconds: 150),
                ).then((value) {
                  if(value != null){
                    for(int i in value){
                      if(Provider.of<FeedDataProvider>(context, listen: false).isContent) {
                        httpContentFeedScrap(
                            context: context,
                            feedId: _feedId,
                            boardId: i
                        );
                      } else {
                        httpFeedScrap(
                            context: context,
                            feedId: _feedId,
                            boardId: i
                        );
                      }
                    }
                  }
                }) : showGeneralDialog(
                  context: context,
                  pageBuilder: (context, animation, secondAnimation) => MembershipPrompt(
                    membershipName: Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                      return element.membershipTier == _accessLevel;
                    }).membershipName,
                    accessLevel: _accessLevel,
                  ),
                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                  barrierDismissible: false,
                  transitionDuration: const Duration(milliseconds: 150),
                ),
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/seem_${this._isScrap ? "on" : "off"}_3x.png",
                        height: 15.6,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "스크랩",
                      count: this._scrapCount,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () async {
                  bool ok = await (this._isLike ? httpFeedUnlike(context: context, feedId: _feedId) : httpFeedLike(context: context, feedId: _feedId));
                  if (ok) {
                    setState(() {
                      this._isLike = !this._isLike;
                      this._likeCount += this._isLike ? 1 : -1;
                    });
                  }
                  else {
                    Fluttertoast.showToast(msg: "좋아요를 남기는 데 실패했습니다.", gravity: ToastGravity.TOP);
                  }
                },
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/like_${this._isLike ? "on" : "off"}_3x.png",
                        height: 15.8,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "좋아요",
                      count: this._likeCount,
                    ),
                  ],
                ),
              ),
            )
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){print("todo");},
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/comment_icon_3x.png",
                        height: 18,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "댓글",
                      count: this._commentCount,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class CelebFeedButtons extends StatefulWidget {
  final int scrapCount;
  final bool isScrap;
  final int likeCount;
  final bool isLike;
  final int commentCount;
  final int feedId;
  final bool canView;
  final bool isContent;
  final int accessLevel;

  const CelebFeedButtons({
    Key key,
    this.scrapCount,
    this.isScrap,
    this.likeCount,
    this.isLike,
    this.commentCount,
    this.feedId,
    this.canView,
    this.isContent = false,
    this.accessLevel
  }) : super(key: key);

  @override
  _CelebFeedButtonsState createState() => _CelebFeedButtonsState();
}

class _CelebFeedButtonsState extends State<CelebFeedButtons> {
  int _scrapCount;
  bool _isScrap;
  int _likeCount;
  bool _isLike;
  int _commentCount;
  int _feedId;
  bool _canView;
  int _accessLevel;

  @override
  void initState() {
    super.initState();
    this._scrapCount = widget.scrapCount;
    this._isScrap = widget.isScrap;
    this._likeCount = widget.likeCount;
    this._isLike = widget.isLike;
    this._commentCount = widget.commentCount;
    this._feedId = widget.feedId;
    this._canView = widget.canView;
    this._accessLevel = widget.accessLevel;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      height: 48,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () {
                if(!Provider.of<LoginVerifyProvider>(context, listen: false).isLogin)
                  showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => LoginPrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),
                  );
                else
                  _canView ? showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => ScrapDialog(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),
                  ).then((value) {
                    if(value != null){
                      for(int i in value){
                        widget.isContent ? httpContentFeedScrap(
                            context: context,
                            feedId: _feedId,
                            boardId: i
                        ): httpCelebFeedScrap(
                            context: context,
                            feedId: _feedId,
                            boardId: i
                        );
                      }
                    }
                  }) : showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => MembershipPrompt(
                      membershipName: Provider.of<MembershipProvider>(context, listen: false).membershipList.firstWhere((element){
                        return element.membershipTier == _accessLevel;
                      }).membershipName,
                      accessLevel: _accessLevel,
                    ),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),
                  );
              },
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/seem_${this._isScrap ? "on" : "off"}_3x.png",
                        height: 15.6,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "스크랩",
                      count: this._scrapCount,
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () async {
                if(!Provider.of<LoginVerifyProvider>(context, listen: false).isLogin)
                  showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => LoginPrompt(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150),
                  );
                else{
                  bool ok;
                  if(widget.isContent) ok = await (this._isLike ?
                  httpContentUnlike(context: context, contentId: _feedId) :
                  httpContentLike(context: context, contentId: _feedId));
                  else ok = await (this._isLike ?
                  httpCelebFeedUnlike(context: context, feedId: _feedId) :
                  httpCelebFeedLike(context: context, feedId: _feedId));
                  if (ok) {
                    setState(() {
                      this._isLike = !this._isLike;
                      this._likeCount += this._isLike ? 1 : -1;
                    });
                  }
                  else {
                    Fluttertoast.showToast(msg: "좋아요를 남기는 데 실패했습니다.", gravity: ToastGravity.TOP);
                  }
                }
              },
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/like_${this._isLike ? "on" : "off"}_3x.png",
                        height: 15.8,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "좋아요",
                      count: this._likeCount,
                    ),
                  ],
                ),
              ),
            )
          ),
          SizedBox(
            width: 11,
          ),
          Expanded(
            child: GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: (){print("todo");},
              child: Container(
                height: 48,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    _FeedSingularButton(
                      leading: Image.asset(
                        "assets/icon/comment_icon_3x.png",
                        height: 18,
                        fit: BoxFit.fitHeight,
                      ),
                      text: "댓글",
                      count: this._commentCount,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _FeedSingularButton extends StatelessWidget {
  final Widget leading;
  final String text;
  final int count;

  const _FeedSingularButton({
    Key key,
    this.leading,
    this.text,
    this.count}
      ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          leading,
          SizedBox(
            width: 10,
          ),
          Text(
            "$text $count",
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 12,
            ),
          )
        ],
      ),
    );
  }
}

class ContentFeedListView extends StatelessWidget {
  final List<ContentFeedData> feedData;

  const ContentFeedListView({
    Key key,
    this.feedData = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: feedData.isEmpty ? Container(
        height: 200,
        child: Center(
          child: Text(
              "아직 피드가 없어요..."
          ),
        ),
      ) : Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: feedData.map((e) => Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: ContentFeedElement.buildWith(e),
        )).toList(),
      ),
    );
  }
}

class FeedListView extends StatelessWidget {
  final List<FeedData> feedData;
  final bool isMoreLoading;
  final bool isRefreshLoading;
  final bool isUserFeed;

  const FeedListView({
    Key key,
    this.feedData = const [],
    this.isMoreLoading = false,
    this.isRefreshLoading = false,
    this.isUserFeed = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> children = feedData.map((e) => Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: isUserFeed ? FeedElement.buildWith(e) : CelebFeedElement.buildWith(e),
    )).toList();

    return Container(
      child: feedData.isEmpty ? Container(
        height: 200,
        child: Center(
          child: Text(
              "아직 피드가 없어요..."
          ),
        ),
      ) : Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: children,
      ),
    );
  }
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      child: feedData.isEmpty ? Container(
//        height: 200,
//        child: Center(
//          child: Text(
//              "아직 피드가 없어요..."
//          ),
//        ),
//      ) : ListView.separated(
//        separatorBuilder: (context, index) => Container(),
//        physics: NeverScrollableScrollPhysics(),
//        shrinkWrap: true,
//        itemCount: feedData.length,
//        itemBuilder: (context, index) => Padding(
//          padding: const EdgeInsets.only(bottom: 15),
//          child: FeedElement.buildWith(feedData[index]),
//        ),
//      ),
//    );
//  }
}

class ScrapFeedListView extends StatelessWidget {
  final List<ScrapFeedData> feedData;

  const ScrapFeedListView({
    Key key,
    this.feedData = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: feedData.isEmpty ? Container(
        height: 200,
        child: Center(
          child: Text(
              "아직 피드가 없어요..."
          ),
        ),
      ) : Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: feedData.map((e) => Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: ScrapFeedElement.buildWith(e),
        )).toList(),
      ),
    );
  }
}


class HomeFeedCollection extends StatefulWidget {
  final List<FeedData> initialData;

  const HomeFeedCollection({
    Key key,
    this.initialData = const []
  }) : super(key: key);

  @override
  _HomeFeedCollectionState createState() => _HomeFeedCollectionState();
}

class _HomeFeedCollectionState extends State<HomeFeedCollection> {
  int _heightDiff = 0;
  bool _loading = false;
  ScrollController _scrollController = ScrollController();

  List<FeedData> _leftDisplay;
  List<FeedData> _rightDisplay;

  @override
  void initState() {
    super.initState();
    _leftDisplay = [];
    _rightDisplay = [];
    for(FeedData data in widget.initialData){
      if(this._heightDiff <= 0){
        this._heightDiff += data.imgUrl[0] == null ? 1 : 2;
        _leftDisplay.add(data);
      } else {
        this._heightDiff -= data.imgUrl[0] == null ? 1 : 2;
        _rightDisplay.add(data);
      }
    }
    _scrollController.addListener(() {
      if(_scrollController.position.pixels >= _scrollController.position.maxScrollExtent &&
          !this._loading){
//        _getMoreData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: _scrollController,
      physics: BouncingScrollPhysics(),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
        color: Color.fromRGBO(247, 247, 247, 1),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: _leftDisplay.asMap().entries.map((e) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: _HomeFeedElement.buildWith(
                          data: e.value,
                          colorIndex: e.key
                      ),
                    )).toList(),
                  ),
                ),
                SizedBox(width: 24,),
                Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: _rightDisplay.asMap().entries.map((e) => Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10),
                      child: _HomeFeedElement.buildWith(
                          data: e.value,
                          colorIndex: e.key
                      ),
                    )).toList(),
                  ),
                )
              ],
            ),
            _loading ? Container(
              height: 150,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ) : Container()
          ],
        ),
      ),
    );
  }
}

class _HomeFeedElement extends StatelessWidget {
  final int userId;
  final int feedId;
  final String userName;
  final String profileImg;
  final String timestamp;
  final String text;
  final String contentImg;
  final int colorIndex;
  final bool isUserFeed;
  final bool isContent;

  const _HomeFeedElement({
    Key key,
    this.userId,
    this.feedId,
    this.userName,
    this.profileImg,
    this.timestamp,
    this.text,
    this.contentImg,
    this.colorIndex,
    this.isUserFeed,
    this.isContent
  }) : super(key: key);

  static _HomeFeedElement buildWith({FeedData data, int colorIndex}){
    return _HomeFeedElement(
      userId: data.userId,
      feedId: data.feedId,
      userName: data.userName,
      profileImg: data.profileImg,
      timestamp: data.postTimestamp,
      text: data.text,
      contentImg: data.imgUrl.isEmpty ? null : data.imgUrl[0],
      colorIndex: colorIndex,
      isUserFeed: data.isUserFeed,
      isContent: data.isContent,
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Provider.of<SelectedCelebProvider>(context, listen: false).celebId = userId;
        Provider.of<SelectedCelebProvider>(context, listen: false).celebName = userName;
        Provider.of<FeedDataProvider>(context, listen: false).feedData = FeedData(
            feedId: feedId,
            userId: userId,
            isUserFeed: isUserFeed,
            isContent: isContent,
            canView: true
        );
        Provider.of<FeedDataProvider>(context, listen: false).isUserFeed = isUserFeed;
        Provider.of<FeedDataProvider>(context, listen: false).isContent = isContent;
        Navigator.pushNamed(context, kRouteFeedDetail).then((value) => Provider.of<CommentReplyProvider>(context, listen: false).reset());
      },
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10),
        width: double.infinity,
        height: contentImg == null ? 130 : 280,
        decoration: BoxDecoration(
            image: contentImg == null ? null : DecorationImage(
                image: CachedNetworkImageProvider(
                  contentImg
                ),
//                NetworkImage(contentImg),
                fit: BoxFit.fitHeight,
                colorFilter: ColorFilter.mode(
                    Color.fromRGBO(0, 0, 0, 0.13),
                    BlendMode.darken
                )
            ),
            color: contentImg != null ? null : kHomeFeedColors[colorIndex % kHomeFeedColors.length],
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.13),
                  blurRadius: 8,
                  offset: Offset(0, 3)
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 6,),
                GestureDetector(
                  onTap: (){
                    Provider.of<MainNavigationProvider>(context, listen: false)
                        .setAll(
                      title: userName,
                      leftButton: BarLeftButton.back,
                      rightButton: BarRightButton.none,
                      rightFunction: dummyFunction,
                    );
                    Provider.of<SelectedCelebProvider>(context, listen: false).celebId = userId;
                    Provider.of<SelectedCelebProvider>(context, listen: false).celebName = userName;

                    Provider.of<SubViewControllerProvider>(context, listen: false)
                        .get(HomeSubRoutes.homeRoot)
                        .jumpToPage(HomeSubPages.community.index);

                  },
                  child: CircleAvatar(
                    radius: 22.5,
                    backgroundColor: Theme.of(context).backgroundColor,
                    child: CircleAvatar(
                      radius: 22,
                      backgroundColor: kMainColor,
                      backgroundImage: profileImg == null || profileImg == "" ?
                      AssetImage("assets/icon/seem_off_3x.png",) :
                      CachedNetworkImageProvider(profileImg,),
                    ),
                  ),
                ),
                SizedBox(width: 6,),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      userName,
                      style: TextStyle(
                        color: Theme.of(context).backgroundColor,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 1,),
                    Text(
                      timestamp,
                      style: TextStyle(
                        color: Theme.of(context).backgroundColor,
                        fontSize: 10,
                      ),
                    )
                  ],
                )
              ],
            ),
            Expanded(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        (text == null || text == "") ? "" : text,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Theme.of(context).backgroundColor,
                            fontSize: 13,
                            shadows: [
                              BoxShadow(
                                  color: Colors.black,
                                  blurRadius: 2,
                                  offset: Offset(0, 1)
                              )
                            ]
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}


class FeedTabCategoryTab extends StatelessWidget {
  final String name;
  final bool selected;

  const FeedTabCategoryTab({
    Key key,
    this.name = "",
    this.selected = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      height: 27,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        border: Border.all(
            color: selected ? Theme.of(context).primaryColor : kUnSelectedColor
        ),
        borderRadius: BorderRadius.circular(14),
      ),
      child: Center(
        child: Text(
          name,
          style: TextStyle(
              color: selected ? Theme.of(context).primaryColor : kSelectableTextColor,
              fontSize: 12
          ),
        ),
      ),
    );
  }
}

List<Color> kHomeFeedColors = [
  Color.fromRGBO(240, 128, 128, 1),
  Color.fromRGBO(248, 173, 157, 1),
  Color.fromRGBO(254,200,154,1),
  Color.fromRGBO(192, 199, 152, 1)
];
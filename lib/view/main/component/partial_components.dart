import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';

import '../../../colors.dart';
import '../../../colors.dart';

class SubscribingCelebIcons extends StatelessWidget {
  final String badgeUrl;
  final String celebName;
  final String imgUrl;

  const SubscribingCelebIcons({
    Key key, this.badgeUrl, this.celebName, this.imgUrl
  }) : super(key: key);

  static SubscribingCelebIcons buildWith(UserAvatarData data){
    return SubscribingCelebIcons(
      celebName: data.userName,
      imgUrl: data.imgUrl,
      badgeUrl: data.badgeUrl,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 78,
      height: 103.6,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // Profile Image
          UserAvatar(
            imgUrl: imgUrl,
            badgeUrl: badgeUrl,
          ),
          SizedBox(height: 4.6,),
          Text(
            celebName,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: 13,
                fontWeight: FontWeight.bold
            ),
          )
        ],
      ),
    );
  }
}

class UserAvatar extends StatelessWidget {
  final String imgUrl;
  final String badgeUrl;

  const UserAvatar({
    Key key,
    this.imgUrl,
    this.badgeUrl
  }) : super(key: key);

  UserAvatar buildWith(UserAvatarData data){
    return UserAvatar(
      imgUrl: data.imgUrl,
      badgeUrl: data.badgeUrl,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 78,
      height: 78,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
              blurRadius: 4,
              color: Color.fromRGBO(0, 0, 0, 0.34)
          )
        ],
      ),
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          CircleAvatar(
            radius: 39,
            backgroundColor: kFunctionButtonColor,
            child: CircleAvatar(
              radius: 37,
              backgroundColor: kMainColor,
              backgroundImage: imgUrl == null || imgUrl == "" ?
                AssetImage("assets/icon/seem_off_3x.png",) :
                NetworkImage(imgUrl),
            ),
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              width: 30,
              height: 30,
              child: badgeUrl == null ? Container() :
              Image.asset("assets/samples/sample_badge.png", fit: BoxFit.contain,),
//              Image.network(badgeUrl, fit: BoxFit.contain,),
            )
          )
        ],
      ),
    );
  }
}

class UserAvatarData{
  final int userId;
  final String userName;
  final String imgUrl;
  final String badgeUrl;

  const UserAvatarData({this.userId, this.userName, this.imgUrl, this.badgeUrl});

  @override
  String toString() {
    return 'UserAvatarData{userId: $userId, userName: $userName, imgUrl: $imgUrl}';
  }


}

class UserListItem extends StatelessWidget {
  final String userName;
  final String imgUrl;
  final String badgeUrl;
  final void Function() onIconTap;

  const UserListItem({
    Key key,
    this.userName,
    this.imgUrl,
    this.badgeUrl,
    this.onIconTap
  }) : super(key: key);

  static UserListItem buildWith(UserAvatarData data){
    return UserListItem(
      userName: data.userName,
      imgUrl: data.imgUrl,
      badgeUrl: data.badgeUrl,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 95,
      padding: const EdgeInsets.only(left: 30, top: 7.5, right: 30, bottom: 7.5),
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          UserAvatar(
            imgUrl: imgUrl,
            badgeUrl: badgeUrl,
          ),
          SizedBox(width: 15,),
          Text(
            userName,
            style: TextStyle(
                fontSize: 17
            ),
          )
        ],
      ),
    );
  }
}

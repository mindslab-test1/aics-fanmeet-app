import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:downloads_path_provider/downloads_path_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/tts_storage_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_sub_view.dart';

import '../../http/mypage/mypage_view.dart';
import '../../providers/tts_storage_provider.dart';

class TtsStorageBody extends StatelessWidget {
  bool _init = false;

  Future<bool> _getData(BuildContext context, int celebId) async {
    if(!_init){
      _init = true;
      return await httpMyPageCelebRequestedTtsList(context: context);
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    TtsStorageProvider dataProvider = Provider.of<TtsStorageProvider>(context);

    return FutureBuilder(
      future: _getData(context, dataProvider.celebId),
      builder: (context, snapshot) {
        return Container(
          color: kMainBackgroundColor,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                height: 123,
                decoration: BoxDecoration(
                  color: kMainBackgroundColor,
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.16),
                      blurRadius: 14,
                      offset: Offset(0, 3),
                    )
                  ]
                ),
                child: Center(
                  child: SubscribingCelebIcons(
                    celebName: dataProvider.celebName,
                    imgUrl: dataProvider.profileImgUrl,
                  ),
                ),
              ),
              SizedBox(height: 4,),
              Expanded(
                child: _TtsStorageList()
              )
            ],
          ),
        );
      }
    );
  }
}

class _TtsStorageList extends StatefulWidget {
  @override
  __TtsStorageListState createState() => __TtsStorageListState();
}

class __TtsStorageListState extends State<_TtsStorageList> {
  bool _expanded = false;
  int _selected =  -1;

  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  bool _isAudioPlaying = false;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
      if(!_expanded) return false;
      else {
        setState(() {
          _expanded = false;
          _selected = -1;
        });
        _disposeAudio();
        return true;
      }
    }, zIndex: 2);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<TtsStorageProvider>(context, listen: false).onBackButton = () {
        if(_expanded){
          setState(() {
            _expanded = false;
            _selected = -1;
          });
          _disposeAudio();
        } else Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
      };
    });
  }

  @override
  Widget build(BuildContext context) {
    List<RequestedTtsData> _ttsData = Provider.of<TtsStorageProvider>(context).ttsCelebRequestList;

    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        child: _expanded ? Container(
          padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 30),
          decoration: BoxDecoration(
            color: kMainBackgroundColor,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Container(
                child: Row(
                  children: <Widget>[
                    Text(
                      "본문",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 30),
                child: Text(
                  _ttsData[_selected].text,
                ),
              ),
              Container(
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        if(!this._isAudioPlaying){
                          String ttsText = _ttsData[_selected].text;
                          if(ttsText.length < 1)
                            Fluttertoast.showToast(msg: "빈 문장은 재생할 수 없습니다.", gravity: ToastGravity.TOP);
                          else{
                            Fluttertoast.showToast(msg: "다음이 재생됩니다: $ttsText", gravity: ToastGravity.TOP);
                            this._isAudioPlaying = true;
                            this._play(_ttsData[_selected].url);
                          }
                        } else{
                          Fluttertoast.showToast(msg: "TTS 미리듣기 중입니다", gravity: ToastGravity.TOP);
                        }
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                        width: 40,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.play,
                              size: 26,
                              color: kSelectableTextColor,
                            ),
                            SizedBox(
                              height: 5.5,
                            ),
                            Text(
                              "재생",
                              style: TextStyle(
                                color: kSelectableTextColor,
                                fontSize: 10
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        var status = await Permission.storage.status;
                        if (!status.isGranted) {
                          await Permission.storage.request();
                        }
                        var dir = await DownloadsPathProvider.downloadsDirectory;
                        final taskId = await FlutterDownloader.enqueue(
                          url: _ttsData[_selected].url,
                          savedDir: dir.absolute.path,
                          showNotification: true, // show download progress in status bar (for Android)
                          openFileFromNotification: true, // click on notification to open downloaded file (for Android)
                        );
//                        return Fluttertoast.showToast(msg: "TODO: TTS download", toastLength: Toast.LENGTH_SHORT);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                        width: 40,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              FontAwesomeIcons.download,
                              size: 26,
                              color: kSelectableTextColor,
                            ),
                            SizedBox(
                              height: 5.5,
                            ),
                            Text(
                              "다운로드",
                              style: TextStyle(
                                  color: kSelectableTextColor,
                                  fontSize: 10
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
//                    GestureDetector(
//                      onTap: () => Fluttertoast.showToast(msg: "TODO: TTS remove", toastLength: Toast.LENGTH_SHORT),
//                      behavior: HitTestBehavior.translucent,
//                      child: Container(
//                        width: 40,
//                        child: Column(
//                          mainAxisSize: MainAxisSize.min,
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: <Widget>[
//                            Icon(
//                              FontAwesomeIcons.trashAlt,
//                              size: 26,
//                              color: kSelectableTextColor,
//                            ),
//                            SizedBox(
//                              height: 5.5,
//                            ),
//                            Text(
//                              "삭제",
//                              style: TextStyle(
//                                  color: kSelectableTextColor,
//                                  fontSize: 10
//                              ),
//                            )
//                          ],
//                        ),
//                      ),
//                    ),
                  ],
                ),
              ),
              SizedBox(height: 15,)
            ],
          ),
        ) : Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: Provider.of<TtsStorageProvider>(context).ttsCelebRequestList.asMap().entries.map((e) => GestureDetector(
            onTap: () {
              setState(() {
                _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
                this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
                  this._isAudioPlaying = false;
                });
                this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {print(event);});
                _selected = e.key;
                _expanded = true;
              });
            },
            behavior: HitTestBehavior.translucent,
            child: _TtsStoredItem(
              celebId: Provider.of<TtsStorageProvider>(context).celebId,
              celebName: Provider.of<TtsStorageProvider>(context).celebName,
              timestamp: formattedTime(e.value.timestamp),
              text: e.value.text,
            ),
          )).toList(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    this._audioPlayer?.dispose();
    this._playerCompletionSubscription?.cancel();
    this._playerErrorSubscription?.cancel();
    super.dispose();
  }

  void _disposeAudio(){
    this._audioPlayer = null;
    this._playerCompletionSubscription = null;
    this._playerErrorSubscription = null;
    this._isAudioPlaying = false;
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }
}

class _TtsStoredItem extends StatelessWidget {
  final int celebId;
  final String celebName;
  final String timestamp;
  final String text;

  const _TtsStoredItem({
    Key key,
    this.celebId,
    this.celebName,
    this.timestamp,
    this.text
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 13),
      height: 72,
      decoration: BoxDecoration(
        color: kMainBackgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            blurRadius: 4,
            offset: Offset(0, 2),
          )
        ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: (MediaQuery.of(context).size.width - 60) / 2,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      celebName,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                    SizedBox(width: 10,),
                    Text(
                      timestamp,
                      style: TextStyle(
                        color: kNavigationPressableColor,
                        fontSize: 12,
                      ),
                    )
                  ],
                ),
                SizedBox(height: 6,),
                Text(
                  text,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 13,
                  ),
                ),
              ],
            ),
          ),
          Icon(
            Icons.arrow_forward_ios,
            size: 25,
            color: kBorderColor,
          ),
        ],
      ),
    );
  }
}

class _TmpTtsItemData {
  final int celebId;
  final String celebName;
  final String timestamp;
  final String text;

  const _TmpTtsItemData({this.celebId, this.celebName, this.timestamp, this.text});
}

List<_TmpTtsItemData> _tmpTtsItemData = const [
  _TmpTtsItemData(
      celebName: "윤도현",
      timestamp: "2020-07-10",
      text: "안녕하세요."
  ),
  _TmpTtsItemData(
      celebName: "윤도현",
      timestamp: "2020-07-10",
      text: "테스트 입니다."
  ),
  _TmpTtsItemData(
      celebName: "윤도현",
      timestamp: "2020-07-10",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
  ),
];
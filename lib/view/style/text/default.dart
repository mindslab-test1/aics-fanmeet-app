import 'package:flutter/cupertino.dart';

import '../../../colors.dart';

//community
const kProfileInfoTextStyle = TextStyle(
  fontFamily: "Roboto",
  fontSize: 13,
  fontWeight: FontWeight.w500
);

//app bar
const kAppBarTextStyle = TextStyle(
    fontFamily: "Roboto",
    fontSize: 20,
    fontWeight: FontWeight.w700,
    color: kAppBarButtonColor
);
const kEditCommentInfoTextStyle = TextStyle(
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: FontWeight.bold
);
const kEditCommentTextStyle = TextStyle(
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: FontWeight.w500,
    height: 1.54
);
const kEditButtonTextStyle = TextStyle(
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: FontWeight.bold,
    height: 1.33,
    color: kMainBackgroundColor
);
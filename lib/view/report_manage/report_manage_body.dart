
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/report_manage_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';

class ReportManageBody extends StatelessWidget {
  final bool isReport;
  List<_TmpReportData> _reportData = [
    _TmpReportData(
      username: "권욱제",
      reportCount: 99,
    ),
    _TmpReportData(
      username: "권욱제",
      reportCount: 99,
    ),
  ];

  ReportManageBody({
    Key key,
    this.isReport = true
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _tmpFuture(),
      builder: (context, snapshot) {
        if(snapshot.hasData)
          return Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: _reportData.map((e) => _ReportUserItem(
                  username: e.username,
                  reportCount: e.reportCount,
                  isBlockedItem: !isReport,
                )).toList(),
              ),
            ),
          );
        else return Container(
          color: Colors.white,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );
      }
    );
  }
}

class _TmpReportData {
  final String username;
  final int reportCount;

  const _TmpReportData({
    this.username,
    this.reportCount
  });
}

class _ReportUserItem extends StatelessWidget {
  final int userId;
  final String profileImg;
  final String username;
  final int reportCount;
  final bool isBlockedItem;

  const _ReportUserItem({
    Key key,
    this.userId,
    this.profileImg,
    this.username,
    this.reportCount,
    this.isBlockedItem
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Provider.of<ReportManageProvider>(context, listen: false).setReportUser(
          userId: userId,
          username: username,
          profileImgUrl: profileImg,
          reportCount: reportCount,
        );
        Navigator.pushNamed(context, kRouteReportDetail);
      },
      behavior: HitTestBehavior.translucent,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  UserAvatar(
                    imgUrl: profileImg,
                  ),
                  SizedBox(width: 15,),
                  Container(
                    width: 100,
                    child: Text(
                      username,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.bold
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  isBlockedItem ? Container() : Text(
                    "신고누적 : $reportCount회",
                    style: TextStyle(
                      color: kNegativeTextColor,
                      fontSize: 12
                    ),
                  ),
                  SizedBox(width: 25,),
                  Icon(
                    Icons.arrow_forward_ios,
                    size: 21,
                    color: kBorderColor,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Future<bool> _tmpFuture() async {
  await Future.delayed(Duration(milliseconds: 500));

  return true;
}
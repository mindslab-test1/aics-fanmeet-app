

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/membership_manage_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';

import '../../colors.dart';

class MembershipDetailRoute extends StatefulWidget {
  @override
  _MembershipDetailRouteState createState() => _MembershipDetailRouteState();
}

class _MembershipDetailRouteState extends State<MembershipDetailRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _MemberDetailAppbar(),
      ),
      body: _MembershipDetailBody(),
    );
  }
}

class _MemberDetailAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 27,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "${Provider.of<MembershipManageProvider>(context).membershipName}",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () =>
                        Navigator.pushNamed(context, kRouteMembershipEnd),
                    behavior: HitTestBehavior.translucent,
                    child: Text(
                      "해지",
                      style: TextStyle(
                        fontSize: 15,
                        color: kAppBarButtonColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(width: 30,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _MembershipDetailBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  _MembershipInfo(membershipImageUrl: Provider.of<MembershipManageProvider>(context).badgeUrl,),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


class _MembershipInfo extends StatelessWidget {

  final String membershipImageUrl;

  const _MembershipInfo({Key key, this.membershipImageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
//      height: 188,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 15,),
          Container(
            height: 112,
            child: CachedNetworkImage(
              imageUrl: membershipImageUrl,
              fit: BoxFit.fitHeight,
              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          SizedBox(height: 14.5,),
          Divider(
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
            color: kBorderColor,
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "가입 정보",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 15,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 16,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: 60,
                          child: Text(
                            "이름",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                            ),
                          )
                      ),
                      SizedBox(width: 24,),
                      Text(
                        "${Provider.of<MembershipManageProvider>(context).membershipName}",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "가입 일자",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).createDate}",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(
//            height: 16,
//          ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          width: 60,
                          child: Text(
                            "갱신일",
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                            ),
                          )
                      ),
                      SizedBox(width: 24,),
                      Text(
                        "${Provider.of<MembershipManageProvider>(context).updateDate}",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 13,
                        ),
                      )
                    ],
                  ),
                ),
//          SizedBox(
//            height: 16,
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "가입 비용",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).cost}",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(
//            height: 16,
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "구독 기간",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).continuous}개월째",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';

class FeedDetailPhotoView extends StatefulWidget {

  final String imgUrl;

  FeedDetailPhotoView({
    Key key,
    this.imgUrl,
}) : super(key : key);

  @override
  State<StatefulWidget> createState() {
    return _FeedDetailPhotoViewState();
  }
}

class _FeedDetailPhotoViewState extends State<FeedDetailPhotoView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(
          height: MediaQuery.of(context).size.height,
        ),
        child: Stack(
          alignment: Alignment.topLeft,
          children: <Widget>[
            PhotoViewGallery.builder(
              scrollDirection: Axis.horizontal,
              scrollPhysics: NeverScrollableScrollPhysics(),
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: CachedNetworkImageProvider(
                      widget.imgUrl
                  ),
                  initialScale: PhotoViewComputedScale.contained * 0.8,
                );
              },
              itemCount: widget.imgUrl.length,
            ),
            Container(
                width: 100,
                height: 100,
                child: GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }
}
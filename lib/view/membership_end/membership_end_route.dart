

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/membership_manage_provider.dart';
import 'package:rf_tap_fanseem/view/membership_end/membership_end_info_dialog.dart';

import '../../colors.dart';

class MembershipEndRoute extends StatefulWidget {
  @override
  _MembershipEndRouteState createState() => _MembershipEndRouteState();
}

class _MembershipEndRouteState extends State<MembershipEndRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _MemberEndAppbar(),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
                child: SingleChildScrollView(
                    child: _MembershipInfo()
                )
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 49),
              height: 70,
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        blurRadius: 6
                    )
                  ]
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: GestureDetector(
                  onTap: () {
                    showGeneralDialog(
                      context: context,
                      pageBuilder: (context, animation, secondAnimation) => MemberEndDialog(),
                      barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                      barrierDismissible: false,
                      transitionDuration: const Duration(milliseconds: 150),
                    );
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        border: Border.all(
                          color: Color.fromRGBO(225, 225, 225, 1),
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              offset: Offset(0, 3),
                              blurRadius: 6
                          )
                        ]
                    ),
                    child: Center(
                      child: Text(
                        "해지 신청",
                        style: TextStyle(
                          fontSize: 18,
//                          fontWeight: FontWeight.bold,
                          color: kNavigationPressableColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _MemberEndAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            FontAwesomeIcons.times,
                            size: 27,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "멤버십 해지",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }
}

class _MembershipInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Text(
                "멤버십 정보",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: 60,
                    child: Text(
                      "이름",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                      ),
                    )
                ),
                SizedBox(width: 24,),
                Text(
                  "${Provider.of<MembershipManageProvider>(context).membershipName}",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 16,
          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "가입 일자",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).createDate}",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(
//            height: 16,
//          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                    width: 60,
                    child: Text(
                      "갱신일",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 13,
                      ),
                    )
                ),
                SizedBox(width: 24,),
                Text(
                  "${Provider.of<MembershipManageProvider>(context).updateDate}",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 13,
                  ),
                )
              ],
            ),
          ),
//          SizedBox(
//            height: 16,
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "가입 비용",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).cost}",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
//          SizedBox(
//            height: 16,
//          ),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 15),
//            child: Row(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.start,
//              children: <Widget>[
//                Container(
//                    width: 60,
//                    child: Text(
//                      "구독 기간",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontWeight: FontWeight.bold,
//                        fontSize: 13,
//                      ),
//                    )
//                ),
//                SizedBox(width: 24,),
//                Text(
//                  "${Provider.of<MembershipManageProvider>(context).continuous}개월째",
//                  style: TextStyle(
//                    color: Colors.black,
//                    fontSize: 13,
//                  ),
//                )
//              ],
//            ),
//          ),
          SizedBox(
            height: 40,
          ),
          Container(
            child: RichText(
              text: TextSpan(
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 12,
                ),
                children: [
                  TextSpan(
                    text: "${Provider.of<MembershipManageProvider>(context).updateDate}",
                    style: TextStyle(
                      color: kNegativeTextColor
                    )
                  ),
                  TextSpan(
                    text: "멤버십 서비스가 해지됩니다. "
                        "해당 기간까지 멤버십 혜택은 유지됩니다. "
                        "해지 신청 후 취소는 불가능하며 기존 멤버십 혜택이 유지되는 동안은 새 멤버십 가입이 불가능합니다."
                        "\n\n정말 해지 하시겠습니까?"
                  )
                ]
              ),
              textAlign: TextAlign.left,
            ),
          )
        ],
      ),
    );
  }
}

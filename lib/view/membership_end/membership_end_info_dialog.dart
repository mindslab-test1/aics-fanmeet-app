

import 'package:flutter/material.dart';
import 'dart:io';
import '../../colors.dart';

class MemberEndDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        height: MediaQuery.of(context).size.height * 80 / 100,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
                child: SingleChildScrollView(
                  physics: const BouncingScrollPhysics(),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: Platform.isAndroid?<Widget>[
                      Text(
                        "[Google Play 스토어 인앱 결제의 해지]"
                        "플레이스토어 > 왼쪽 메뉴바 선택 > [정기 결제] 선택 > 팬밋에서 결제한 해당 상품 [구독취소] 선택하시면 됩니다."
                        "인앱 결제자의 경우, 구글에 직접 해지신청을 하셔야 합니다."
                        "이 경우 신청일부터 구독 만료일까지는 멤버십 혜택을 이용할 수 있으며, 다음 결제일에 자동결제가 되지 않습니다.",
                        style: TextStyle(
                            fontSize: 15,
                            color: kSelectableTextColor
                        ),
                      ),
                      SizedBox(height: 5,),
                      Image.asset("assets/samples/sample_member_term_1.jpg", fit: BoxFit.fitWidth,),
                      SizedBox(height: 5,),
                      Image.asset("assets/samples/sample_member_term_2.png", fit: BoxFit.fitWidth,),
                      SizedBox(height: 5,),
                      Image.asset("assets/samples/sample_member_term_3.png", fit: BoxFit.fitWidth,),
                      SizedBox(height: 5,),
                    ]:<Widget>[
                  Text(
                    "정기구독의 취소(멤버십 해지)는 [애플 앱스토어] 또는 [설정 - Apple ID] 에서 신청 가능합니다.\n"
                        "* 정기구독 취소 경로\n"
                        "[계정 >> 구독 >> 구독상품 >> 구독 취소]\n"
                        "- 정기구독을 취소하면 만료일 이후에 멤버십 서비스가 자동으로 해지되며, 월 자동결제가 이루어지지 않습니다.\n"
                        "- 지금 해지신청을 해도 갱신일까지는 멤버십 혜택이 유지됩니다.\n"
                        "- 기존 멤버십 혜택이 유지되는 동안에는 해당 셀럽의 다른 멤버십 가입이 불가능합니다.\n",
                    style: TextStyle(
                        fontSize: 15,
                        color: kSelectableTextColor
                    ),
                  ),
                    ]
                  ),
                ),
//                child: Center(
//                    child: RichText(
//                      textAlign: TextAlign.left,
//                      text: TextSpan(
//                          style: TextStyle(
//                              fontSize: 15,
//                              color: kSelectableTextColor
//                          ),
//                          children: [
//                            TextSpan(
//                              text: "해당 피드는",
//                            ),
//                            TextSpan(
//                              text: " 멤버십 ",
//                              style: TextStyle(
//                                  color: Theme.of(context).primaryColor
//                              ),
//                            ),
//                            TextSpan(
//                              text: "전용 콘텐츠 입니다. 멤버십 가입 이 후 즐기실 수 있습니다!",
//                            )
//                          ]
//                      ),
//                    )
//                ),
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: GestureDetector(
//                        onTap: () => Navigator.pop(context),
//                        behavior: HitTestBehavior.translucent,
//                        child: Center(
//                          child: Text(
//                            "취소",
//                            style: TextStyle(
//                              fontSize: 17,
//                              color: Colors.black,
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                    VerticalDivider(
//                      width: 0.5,
//                    ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "확인",
                          style: TextStyle(
                              fontSize: 17,
                              color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

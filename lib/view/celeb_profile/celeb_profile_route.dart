
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/celeb_profile_provider.dart';
import 'package:rf_tap_fanseem/view/celeb_profile/celeb_profile_body.dart';
import 'package:rf_tap_fanseem/view/style/text/default.dart';

import '../../colors.dart';

class CelebProfileRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        titleSpacing: 0,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _CelebProfileAppbar(),
      ),
      body: CelebProfileBody(),
    );
  }
}

class _CelebProfileAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                GestureDetector(
                  onTap: () => Navigator.pop(context,true),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              "${Provider.of<CelebProfileProvider>(context, listen: false).celebName}의 프로필",
              textAlign: TextAlign.center,
              style: kAppBarTextStyle
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

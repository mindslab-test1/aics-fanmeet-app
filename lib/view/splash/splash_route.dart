import 'dart:convert';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';



class SplashScreenRoute extends StatefulWidget {
  @override
  _SplashScreenRouteState createState() => _SplashScreenRouteState();
}

class _SplashScreenRouteState extends State<SplashScreenRoute> {
  String projectCode;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      final FlutterSecureStorage storage = FlutterSecureStorage();
      SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
      var loginError = sharedPreferences.get("ai_maum_fanmeet_login_fix_1.0.17");

      if(loginError == null){
        await FlutterNaverLogin.logOut();
        await storage.delete(key: "ai_maum_fanmeet_user_data");
        await sharedPreferences.setBool("ai_maum_fanmeet_login_fix_1.0.17", true);
        Fluttertoast.showToast(msg: "버전이 바뀌어 자동으로 로그아웃 됩니다.", gravity: ToastGravity.TOP);
      }

      developer.log("try to autologin");

      storage.read(key: "ai_maum_fanmeet_user_data").then((value) {
        if(value != null){
          Map<String, dynamic> userData = jsonDecode(value);
          Provider.of<LoginVerifyProvider>(context, listen: false).setLoginInfo(
              true,
              userData["social"],
              userData["token"],
              userData["isCeleb"] != null ? userData["isCeleb"] : false,
              userData["userId"]
          );
        }
      });
    });
    return Stack(
      children: <Widget>[
        SplashScreen(
          seconds: 1,
          backgroundColor: Theme.of(context).primaryColor,
          navigateAfterSeconds: kRouteHome,
          loaderColor: Colors.transparent,
        ),
        Container(
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width / 2,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    "assets/logo/splash_X3.png"
                  )
                )
              ),
            ),
          ),
        )
      ],
    );
  }
}
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_edit.dart';
import 'package:rf_tap_fanseem/http/feed/feed_edit.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/feed_edit/feed_edit_body.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_view_provider.dart';

import '../../colors.dart';

class FeedEditRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _FeedEditAppBar(),
      ),
      body: FeedEditBody(),
    );
  }
}

class _FeedEditAppBar extends StatefulWidget {
  @override
  __FeedEditAppBarState createState() => __FeedEditAppBarState();
}

class __FeedEditAppBarState extends State<_FeedEditAppBar> {
  ProgressDialog _pr;


  @override
  Widget build(BuildContext context) {
    FeedEditDataProvider _feedEditDataProvider = Provider.of<FeedEditDataProvider>(context, listen: false);
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                Provider.of<FeedEditViewProvider>(context).title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: () async {
                        _feedEditDataProvider.isUploadingVideo=true;
                        await _pr.show();
                        await Future.delayed(Duration(milliseconds: 200));

                        if (_feedEditDataProvider.text == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(
                              msg: "빈 텍스트는 업로드할 수 없습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        bool isSuccess = false;
                        if (Provider.of<FeedDataProvider>(context,listen: false).isUserFeed){
                          isSuccess = await httpFeedEdit(
                            context: context,
                            feedId: _feedEditDataProvider.feedId,
                            celebId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId,
                            categoryId: _feedEditDataProvider.categoryId,
                            text: _feedEditDataProvider.text,
                            removedPictures: _feedEditDataProvider.removedPictures,
                            addedPictures: _feedEditDataProvider.imageUrls,
                            pictures: _feedEditDataProvider.pictures,
                            youtubeUrl: _feedEditDataProvider.youtubeUrl,
                            videoUrl: _feedEditDataProvider.videoUrl,
                          );
                        }
                        else{
                          isSuccess = await httpCelebFeedEdit(
                              context: context,
                              celebFeed: _feedEditDataProvider.feedId,
                              categoryId: _feedEditDataProvider.categoryId,
                              text: _feedEditDataProvider.text,
                              addedPictures: _feedEditDataProvider.imageUrls,
                              pictures: _feedEditDataProvider.pictures,
                              videoUrl: _feedEditDataProvider.videoUrl,
                              youtubeUrl: _feedEditDataProvider.youtubeUrl,
                              removedPictures: _feedEditDataProvider.removedPictures,
                              accessLevel: _feedEditDataProvider.accessLevel
                          );
                        }
                        if (isSuccess) {
                          await _pr.hide();
                          Navigator.pop(context, true);
                        }
                        else {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "피드 업로드에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        }
                        _feedEditDataProvider.isUploadingVideo=false;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: kAppBarButtonColor,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


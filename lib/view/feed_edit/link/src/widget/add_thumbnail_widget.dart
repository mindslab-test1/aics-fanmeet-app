// import 'package:cached_network_image/cached_network_image.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/bloc.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/thumbnail_event.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/thumbnail_state.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/model/media_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/colors.dart';

class AddMediaDialogContent extends StatefulWidget {
  final String titleText;
  final String textFieldHintText;

  const AddMediaDialogContent(
      {Key key,
        this.titleText,
        this.textFieldHintText,
      })
      : super(key: key);
  @override
  _AddMediaDialogContentState createState() => _AddMediaDialogContentState();
}

class _AddMediaDialogContentState extends State<AddMediaDialogContent> {
  String textFieldText = "";

  TextField _textField;
  TextEditingController _txtController = TextEditingController();

  @override
  void initState() {
    super.initState();

    // Give TextField focus after a little delay
    Future.delayed(Duration(milliseconds: 500), () {
      FocusScope.of(context).requestFocus(_textField.focusNode);
    });
  }

  initTextField() {
    _textField = TextField(
      focusNode: FocusNode(),
      maxLines: 1,
      onChanged: (text) {
        textFieldText = text;
        BlocProvider.of<ThumbnailBloc>(context).add(UrlChange());
      },
      controller: _txtController,
      textAlign: TextAlign.start,

      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 10),
        hintText: widget.textFieldHintText,
        hintStyle: TextStyle(fontSize: 14.0, color: Color.fromRGBO(112, 112, 112, 1)),
        border: InputBorder.none,
      ),
      cursorColor: Colors.black,

    );
  }

  Widget _footer(ThumbnailState state) {
    MediaInfo media;
    if (state is LoadedMedia) {
      media = state.mediaInfo;
    }
    if (state is UrlChanged) {
      media = null;
    }
    if (state is FailureDetail) {
      Fluttertoast.showToast(msg: "영상을 불러오는데 실패하였습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
    }
    return Row(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: FlatButton(
            child: Text("취소", style: TextStyle(color: Colors.black),),
            onPressed: () {
              BlocProvider.of<ThumbnailBloc>(context).add(CancelEvent());
              Navigator.pop(context);
            },
          ),
        ),
        Expanded(
          flex: 1,
          child: FlatButton(
            child: Text(
                media != null && media.thumbnailUrl != null ? "완료" : "다음",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color.fromRGBO(216, 72, 97, 1)
                )
            ),
            onPressed: () {
              if(_txtController.text == null){
                Fluttertoast.showToast(msg: "url을 입력하세요", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
              } else if(_txtController.text.length == 0){
                Fluttertoast.showToast(msg: "url을 입력하세요", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
              } else if (media == null || media.thumbnailUrl == null) {
                SystemChannels.textInput.invokeMethod('TextInput.hide');
                BlocProvider.of<ThumbnailBloc>(context).add(
                    AddUrl(
                        url: _txtController.text
                    )
                );
              } else {
                Navigator.pop(context, media);
              }
            },
          ),
        ),
      ],
    );
  }

  Widget _previewSection(ThumbnailState state) {
    MediaInfo media;
    if (state is LoadedMedia) {
      media = state.mediaInfo;
    }
    return AnimatedSwitcher(
      duration: Duration(milliseconds: 400),
      switchInCurve: Curves.easeIn,
      switchOutCurve: Curves.linear,
      child: state is LoadingMedia ||
          state is DialogOpened ||
          state is LoadedMedia &&
              (media == null || media.thumbnailUrl == null)
          ? _loader(state)
          : state is LoadedMedia && media != null
          ? _thumbnailWidget(media)
          : Container(
        key: ValueKey(5),
      ),
    );
  }

  Widget _thumbnailWidget(MediaInfo media) {
    return Container(
      key: ValueKey(4),
      padding: EdgeInsets.only(left: 30, right: 30, bottom: 20),
      width: double.infinity,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.grey[850],
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Opacity(
                opacity: 0.5,
                child: AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Image.network(
                    media.thumbnailUrl != null ? media.thumbnailUrl : "",
                    fit: BoxFit.cover,
                    loadingBuilder: (BuildContext context, Widget child,
                        ImageChunkEvent loadingProgress) {
                      if (loadingProgress == null) return child;
                      return Center(
                        child: CircularProgressIndicator(
                          value: loadingProgress.expectedTotalBytes != null
                              ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes
                              : null,
                        ),
                      );
                    },
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(16),
                child: media.title != null
                    ? Text(
                  media.title,
                  softWrap: false,
                  overflow: TextOverflow.fade,
                  style: TextStyle(color: Colors.white, fontSize: 16),
                )
                    : Container(
                  height: 16,
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.grey[700],
                    borderRadius: BorderRadius.circular(4),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _loader(ThumbnailState state) {
    return state is DialogOpened
        ? SizedBox()
        : Container(
        key: ValueKey(3),
        padding: EdgeInsets.symmetric(vertical: 32, horizontal: 8),
        decoration: BoxDecoration(
          color: Colors.grey[850],
          borderRadius: BorderRadius.circular(10),
        ),
        child: state is LoadingMedia
            ? Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white30),
          ),
        ): null
    );
  }

  @override
  Widget build(BuildContext context) {
    initTextField();

    return BlocBuilder<ThumbnailBloc, ThumbnailState>(
//       bloc: ThumbnailBloc,
      builder: (context, state) {
        return Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(26),
          ),
          child: Container(
            width: MediaQuery.of(context).size.width - 28,
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.circular(26),
                boxShadow: [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.64),
                      blurRadius: 14
                  )
                ]
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SizedBox(height: 32),
                // title
                Padding(
                  padding: const EdgeInsets.only(left: 41),
                  child: Text(
                      widget.titleText,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20, color: Colors.black
                      )
                  ),
                ),
                SizedBox(height: 16),
                // text field
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 31),
                  child: Container(
                      width: MediaQuery.of(context).size.width - 88,
                      decoration: BoxDecoration(
                          border: Border.all(color: kBorderColor),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.16),
                                blurRadius: 6,
                                offset: Offset(0, 3)
                            )
                          ]
                      ),
                      child: _textField
                  ),
                ),
                SizedBox(height: 16),
                // thumbnail preview
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: _previewSection(state),
                ),
                // footer
                _footer(state)
              ],
            ),
          ),
        );
      },
    );
  }
}

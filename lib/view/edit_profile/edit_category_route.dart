import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_category_provider.dart';

class EditCategoryRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CategoryProvider provider = Provider.of<CategoryProvider>(context);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        automaticallyImplyLeading: false,
        title: Text(
          "관심분야 설정",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 70,
              child: Center(
                child: Text(
                  "관심분야를 선택하세요!(최대 4개)\n선택한 관심사에 따라 알맞는 콘텐츠를 추천해드립니다.",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                ),
              ),
            ),
            Expanded(child: Container(
              child: GridView.count(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.1,
                    right: MediaQuery.of(context).size.width * 0.1,
                    top: 15.3,
                    bottom: 5.3
                ),
                crossAxisCount: 3,
                crossAxisSpacing: MediaQuery.of(context).size.width * 0.07,
                childAspectRatio: 0.75,
                children: CategoryEnum.values.map((e) => _CategoryWidget(
                  category: e,
                  iconUri: _getCategoryIcon(e),
                  text: _getCategoryText(e),
                )).toList(),
              ),
            )),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 70,
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    blurRadius: 6
                  )
                ]
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: MediaQuery.of(context).size.width * 0.55 / 2),
                child: GestureDetector(
                  onTap: () {
                    Fluttertoast.showToast(msg: "TODO", toastLength: Toast.LENGTH_SHORT);
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.55,
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        border: Border.all(
                          color: provider.selected.isNotEmpty ? kMainColor : kUnSelectedColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              offset: Offset(0, 3),
                              blurRadius: 6
                          )
                        ]
                    ),
                    child: Center(
                      child: Text(
                        "완료",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                            color: provider.selected.isNotEmpty ? kMainColor : kUnSelectedColor,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _CategoryWidget extends StatelessWidget {
  final CategoryEnum category;
  final String iconUri;
  final String text;

  _CategoryWidget({Key key, this.category, this.iconUri, this.text})
      : assert(category != null), super(key: key);
  @override
  Widget build(BuildContext context) {
    CategoryProvider categoryProvider = Provider.of<CategoryProvider>(context);

    return Container(
      height: MediaQuery.of(context).size.width * 0.25,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          GestureDetector(
            onTap: () {
              if(categoryProvider.selected.contains(category)) categoryProvider.pop(category);
              else if(categoryProvider.selected.length == 4){
                Fluttertoast.showToast(msg: "총 4개 까지의 관심분야를 설정할 수 있습니다.", gravity: ToastGravity.TOP);
              }
              else{
                categoryProvider.selected.contains(category) ?
                  categoryProvider.pop(category) : categoryProvider.push(category);
              }
            },
            child: Container(
              height: MediaQuery.of(context).size.width * 0.2,
              width: MediaQuery.of(context).size.width * 0.2,
              decoration: BoxDecoration(
                  color: categoryProvider.selected.contains(category) ? kFunctionButtonColor : kUnSelectedColor,
                  shape: BoxShape.circle
              ),
              child: Center(
                child: Image.asset(
                  iconUri == null ?
                      "assets/icon/seem_off_3x.png" : iconUri,
                  color: Theme.of(context).backgroundColor,
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: Text(
                text == null ? "테스트": text,
                style: TextStyle(
                  fontSize: 11,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
    );
  }
}

String _getCategoryIcon(CategoryEnum category){
  switch(category){
    case CategoryEnum.acting:
      return "assets/cat_icon/cat_icon_act.png";
    case CategoryEnum.singing:
      return "assets/cat_icon/cat_icon_sing.png";
    case CategoryEnum.talking:
      return "assets/cat_icon/cat_icon_talk.png";
    case CategoryEnum.cooking:
      return "assets/cat_icon/cat_icon_cook.png";
    case CategoryEnum.exercise:
      return "assets/cat_icon/cat_icon_exercise.png";
    case CategoryEnum.streaming:
      return "assets/cat_icon/cat_icon_stream.png";
    case CategoryEnum.games:
      return "assets/cat_icon/cat_icon_game.png";
    case CategoryEnum.songs:
      return "assets/cat_icon/cat_icon_music.png";
    case CategoryEnum.eating:
      return "assets/cat_icon/cat_icon_eating.png";
    case CategoryEnum.asmr:
      return "assets/cat_icon/cat_icon_asmr.png";
    case CategoryEnum.arts:
      return "assets/cat_icon/cat_icon_arts.png";
    case CategoryEnum.travel:
      return "assets/cat_icon/cat_icon_travel.png";
    case CategoryEnum.beauty:
      return "assets/cat_icon/cat_icon_beauty.png";
    case CategoryEnum.dailyLife:
      return "assets/cat_icon/cat_icon_daily_life.png";
    case CategoryEnum.engineer:
      return "assets/cat_icon/cat_icon_engineer.png";
    case CategoryEnum.movie:
      return "assets/cat_icon/cat_icon_movie.png";
    case CategoryEnum.review:
      return "assets/cat_icon/cat_icon_review.png";
    case CategoryEnum.dance:
      return "assets/cat_icon/cat_icon_dance.png";
  }
}

String _getCategoryText(CategoryEnum category){
  switch(category){
    case CategoryEnum.acting:
      return "연기";
    case CategoryEnum.singing:
      return "노래";
    case CategoryEnum.talking:
      return "토크";
    case CategoryEnum.cooking:
      return "요리";
    case CategoryEnum.exercise:
      return "운동";
    case CategoryEnum.streaming:
      return "방송";
    case CategoryEnum.games:
      return "게임";
    case CategoryEnum.songs:
      return "음악";
    case CategoryEnum.eating:
      return "먹방";
    case CategoryEnum.asmr:
      return "ASMR";
    case CategoryEnum.arts:
      return "미술·공예";
    case CategoryEnum.travel:
      return "여행";
    case CategoryEnum.beauty:
      return "뷰티";
    case CategoryEnum.dailyLife:
      return "일상";
    case CategoryEnum.engineer:
      return "엔지니어";
    case CategoryEnum.movie:
      return "영화";
    case CategoryEnum.review:
      return "리뷰";
    case CategoryEnum.dance:
      return "댄스";
  }
}
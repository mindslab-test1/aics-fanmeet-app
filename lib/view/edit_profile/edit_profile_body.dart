
import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_profile_edit.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_profile_view.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/edit_profile/edit_nickname_route.dart';

import '../../colors.dart';
import '../../providers/mypage_view_provider.dart';
import '../../util/photo_util.dart';
import '../signup/component/signup_category_view.dart';

class EditProfileBody extends StatelessWidget {
  Future<bool> _getData(BuildContext context) async {
    return await httpMyPageProfileView(context: context);
  }

  @override
  Widget build(BuildContext context) {
    ProgressDialog _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Container(
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 45,
                  width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "프로필 변경 중 입니다..",
                  style: TextStyle(
                      fontSize: 12,
                      color: kNegativeTextColor
                  ),
                )
              ],
            ),
          ),
        )
    );
    _pr.style(elevation: 0, backgroundColor: Colors.transparent);

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BackButtonInterceptor.add((stopDefaultButtonEvent, _) {
        Navigator.popUntil(context,ModalRoute.withName('/home'));
        return true;
      }, zIndex: 5, ifNotYetIntercepted: true);
    });

    return FutureBuilder(
        future: _getData(context),
        builder: (context, snapshot) {
          if(!snapshot.hasData)
            return Container(
              child: Container(color: Colors.white,child: Center(child: CircularProgressIndicator())),
            );

          return Container(
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        _ProfileImageStack(
                        ),
                        SizedBox(height: 20,),
                        _NickNameEditBar(

                        ),
//                        Divider(height: 0.5, thickness: 0.5, indent: 30.5, endIndent: 30.5,),
//                        SizedBox(height: 15,),
//                        _DescriptionEditBar(),
                        Divider(height: 0.5, thickness: 0.5, indent: 30.5, endIndent: 30.5,),
                        SizedBox(height: 15,),
//                        _InterestedCategoryBar(categories: [CategoryEnum.acting, CategoryEnum.asmr],),
//                        Divider(height: 0.5, thickness: 0.5, indent: 30.5, endIndent: 30.5,),
//                        SizedBox(height: 15,),
//                        _PrivacyOptionBar()
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 70,
                  decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.16),
                            blurRadius: 6,
                            offset: Offset(0, 0)
                        )
                      ]
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: MediaQuery.of(context).size.width * 0.55 / 2),
                    child: GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () async {
                        _pr.show();
                        await Future.delayed(Duration(milliseconds: 300));
                        String profileUri = Provider.of<MyPageEditProvider>(context, listen: false).profileFileUri;
                        String bannerUri = Provider.of<MyPageEditProvider>(context, listen: false).bannerFileUri;
                        bool isSuccess = await httpEditProfileImages(
                            context: context,
                            profileImagePath: profileUri,
                            coverImagePath: bannerUri
                        );
                        if(isSuccess){
                          _pr.hide();
                          Navigator.pop(context);
                        }
                        else{
                          _pr.hide();
                          Fluttertoast.showToast(msg: "프로필 변경에 실패하였습니다.", gravity: ToastGravity.TOP);
                        }
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.55,
                        decoration: BoxDecoration(
                            color: Theme.of(context).backgroundColor,
                            border: Border.all(
                              color: Theme.of(context).primaryColor,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.16),
                                  offset: Offset(0, 3),
                                  blurRadius: 6
                              )
                            ]
                        ),
                        child: Center(
                          child: Text(
                            "완료",
                            style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        }
    );
  }
}

class _ProfileImageStack extends StatelessWidget {
  const _ProfileImageStack({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MyPageViewProvider viewProvider = Provider.of<MyPageViewProvider>(context);
    MyPageEditProvider dataProvider = Provider.of<MyPageEditProvider>(context);

    return Container(
      height: 166,
      child: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            top: 0.0,
            width: MediaQuery.of(context).size.width,
            child: Container(
              height: 120,
              color: kMainColor,
              width: double.infinity,
              child: ColorFiltered(
                colorFilter: ColorFilter.mode(
                    Color.fromRGBO(0, 0, 0, 0.55),
                    BlendMode.darken
                ),
                child: dataProvider.bannerFileUri != null ?
                Image.file(File(dataProvider.bannerFileUri), fit: BoxFit.fitWidth)
                    : (viewProvider.bannerImgUrl != null ?
                Image.network(viewProvider.bannerImgUrl, fit: BoxFit.fitWidth) :
                Image.asset(
                  "assets/logo/splash_X3.png",
                  fit: BoxFit.fitWidth,
                )),
              ),
            ),
          ),
          Positioned(
            top: 12,
            right: 13,
            child: GestureDetector(
              onTap: () async {
//                var status = await Permission.storage.status;
//                if (!status.isGranted) {
//                  status = await Permission.storage.request();
//                }
//                if(!status.isGranted) return;

                File file = await openImagePicker();
                if (file != null) {
                  Provider.of<MyPageEditProvider>(context, listen: false).bannerFileUri = file.path;
                }
              },
              behavior: HitTestBehavior.translucent,
              child: Container(
                child: Image.asset(
                  "assets/icon/add_a_photo.png",
                  width: 45.1,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            left: MediaQuery.of(context).size.width / 2 - 50,
            child: Align(
              child: Container(
                width: 100,
                height: 100,
                child: CircleAvatar(
                    radius: 50,
                    backgroundColor: Theme.of(context).backgroundColor,
                    child: CircleAvatar(
                      radius: 49,
                      backgroundColor: kMainColor,
                      backgroundImage: dataProvider.profileFileUri != null ?
                      FileImage(File(dataProvider.profileFileUri),) :
                      (viewProvider.profileImgUrl != null ?
                      NetworkImage(viewProvider.profileImgUrl) :
                      AssetImage("assets/icon/seem_off_3x.png")),
                    )
                ),
              ),
            ),
          ),
          Positioned(
            bottom: 13,
            right: 124,
            child: GestureDetector(
              onTap: () async {
//                var status = await Permission.storage.status;
//                if (!status.isGranted) {
//                  status = await Permission.storage.request();
//                }
//                if(!status.isGranted) return;

                File file = await openImagePicker();
                if (file != null) {
                  Provider.of<MyPageEditProvider>(context, listen: false).profileFileUri = file.path;
                }
              },
              behavior: HitTestBehavior.translucent,
              child: Container(
                child: Image.asset(
                  "assets/icon/add_a_photo.png",
                  width: 45.1,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _NickNameEditBar extends StatefulWidget {
  final String userName;

  const _NickNameEditBar({Key key, this.userName = "test"}) : super(key: key);

  @override
  __NickNameEditBarState createState() => __NickNameEditBarState();
}

class __NickNameEditBarState extends State<_NickNameEditBar> {

  @override
  void initState() {
    super.initState();
//    this._isEditing = false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "닉네임",
            style: _headerStyle,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 10, bottom: 10),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 10,
                    child: Text(
                      Provider.of<MyPageEditProvider>(context).name == null ?
                      Provider.of<MyPageViewProvider>(context).name : Provider.of<MyPageEditProvider>(context).name,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 13,
                      ),
                    )
                ),
                GestureDetector(
                  onTap: () => Navigator.push(context, MaterialPageRoute(
                      builder: (context) => EditNicknameRoute()
                  )),
                  child: Icon(
                    FontAwesomeIcons.pen,
                    size: 15,
                  ),
                )
              ],
            ),
          ),
//          Divider(
//            indent: 30,
//            endIndent: 30,
//            color: kUnSelectedColor,
//            thickness: 1,
//            height: 0,
//          )
        ],
      ),
    );
  }
}

class _DescriptionEditBar extends StatefulWidget {
  final String description;

  const _DescriptionEditBar({Key key, this.description}) : super(key: key);

  @override
  __DescriptionEditBarState createState() => __DescriptionEditBarState();
}

class __DescriptionEditBarState extends State<_DescriptionEditBar> {
  TextEditingController _textController = TextEditingController();

  @override
  void initState() {
    super.initState();
    widget.description != null ? _textController.text = widget.description : "";
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "인사말",
            style: _headerStyle,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 10,
                  child: TextField(
//                    enabled: this._isEditing,
                    controller: _textController,
                    textAlign: TextAlign.start,
                    style: _descriptionStyle,
                    decoration: InputDecoration.collapsed(
                      border: InputBorder.none,
                      hintText: "안녕하세요~",
                    ),
                  ),
                ),
//                Expanded(
//                  flex: 1,
//                  child: GestureDetector(
//                    onTap: () => setState(() => this._isEditing = !this._isEditing),
//                    behavior: HitTestBehavior.translucent,
//                    child: Icon(
//                      this._isEditing ? Icons.done : FontAwesomeIcons.pen,
//                      size: 15,
//                    ),
//                  ),
//                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _InterestedCategoryBar extends StatelessWidget {
  final List<CategoryEnum> categories;

  const _InterestedCategoryBar({
    Key key,
    this.categories = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "관심 분야",
                style: _headerStyle,
              ),
              GestureDetector(
                onTap: () => Navigator.pushNamed(context, kRouteCategoryEdit),
                behavior: HitTestBehavior.translucent,
                child: Icon(
                  FontAwesomeIcons.pen,
                  size: 15,
                ),
              )
            ],
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Center(
              child: categories.isNotEmpty ? Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: categories.map((e) => _InterestedCategory(
                  category: e,
                )).toList(),
              ) : Container(
                child: Center(
                  child: Text(
                    "아직 관심분야가 없습니다.",
                    style: _descriptionStyle,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _InterestedCategory extends StatelessWidget {
  final CategoryEnum category;

  const _InterestedCategory({Key key, this.category}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 5),
      child: Container(
        width: 56.8,
        height: 56.8,
        decoration: BoxDecoration(
            color: kFunctionButtonColor,
            shape: BoxShape.circle
        ),
        // TODO get Icon
        child: Image.asset(getCategoryIcon(category), fit: BoxFit.fitWidth,),
      ),
    );
  }
}

class _PrivacyOptionBar extends StatelessWidget {
  List<String> _options = [
    "내가 구독한 셀럽",
    "니의 활동",
    "나의 스크랩",
//    "팔로워 / 팔로우",
    "팔로우"
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: _options.map((e) => _PrivacyOption(
          option: e,
        )).toList(),
      ),
    );
  }
}

class _PrivacyOption extends StatefulWidget {
  final String option;
  final bool on;

  const _PrivacyOption({
    Key key,
    this.option,
    this.on = false,
  }) : super(key: key);

  @override
  __PrivacyOptionState createState() => __PrivacyOptionState();
}

class __PrivacyOptionState extends State<_PrivacyOption> {
  bool _on;


  @override
  void initState() {
    super.initState();
    _on = widget.on;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 7.4),
      child: Container(
        height: 27,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.option,
              style: _descriptionStyle,
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Divider(
                    indent: 10,
                    endIndent: 10,
                  )
                ],
              ),
            ),
            Container(
              width: 55,
              child: Switch(
                value: this._on,
                onChanged: (value) async {
                  setState(() => this._on = !this._on);
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}


TextStyle _headerStyle = TextStyle(
    color: Colors.black,
    fontSize: 15,
    fontWeight: FontWeight.bold
);

TextStyle _descriptionStyle = TextStyle(
  color: Colors.black,
  fontSize: 13,
);
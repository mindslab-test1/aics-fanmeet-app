
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_profile_edit.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';

import '../../colors.dart';
import '../../http/rest_call_functions.dart';

class EditNicknameRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _EditNicknameAppbar(),
      ),
      body: EditNickNameView(),
      resizeToAvoidBottomInset: false,
    );
  }
}
class EditNickNameView extends StatefulWidget {
  @override
  _EditNickNameViewState createState() => _EditNickNameViewState();
}

class _EditNickNameViewState extends State<EditNickNameView> {
  final TextEditingController _textController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    MyPageEditProvider provider = Provider.of<MyPageEditProvider>(context, listen: false);
    return Container(
      height: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                "닉네임",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 30,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    onChanged: (value) {
                      provider.validatePressed = false;
                    },
                    controller: _textController,
                    maxLength: 15,
                    maxLengthEnforced: true,
                    decoration: InputDecoration.collapsed(
                      hintText: "닉네임을 입력해 주세요.",
                      hintStyle: TextStyle(
                        color: kUnSelectedColor,
                        fontSize: 14,
                      ),
                    ),
                  ),
                ),
                SizedBox(width: 15,),
                GestureDetector(
                  onTap: () async {
                    if(_textController.text == null) Fluttertoast.showToast(msg: "닉네임을 입력해주세요!", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    else if(_textController.text.length == 0)  Fluttertoast.showToast(msg: "닉네임을 입력해주세요!", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    else if(_textController.text.length > 15)  Fluttertoast.showToast(msg: "닉네임은 최대 15자까지 설정할 수 있습니다.", gravity: ToastGravity.TOP);
                    else{
                      RegExp regExp = RegExp("^[가-힣a-zA-Z0-9._\-]");
                      if(regExp.allMatches(_textController.text).isEmpty){
                        Fluttertoast.showToast(msg :"닉네임에 사용할 수 없는 문자가 포함되어 있습니다.", gravity: ToastGravity.TOP);
                      }
                      else if(!(await _checkName(_textController.text)))
                        Fluttertoast.showToast(msg: "중복된 닉네임 입니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      else {
                        Fluttertoast.showToast(msg: "사용 가능한 닉네임입니다!", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        provider.validatePressed = true;
                        Provider.of<MyPageEditProvider>(context, listen: false).name = _textController.text;
                      }
                    }
                  },
                  child: Container(
                    width: 75,
                    height: 31,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        color: Theme.of(context).backgroundColor,
                        border: Border.all(color: kMainColor),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6,
                              offset: Offset(0, 3)
                          )
                        ]
                    ),
                    child: Center(
                      child: Text(
                        "중복 확인",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: kMainColor,
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 100,
          ),
          Text(
            "닉네임은 최대 15자까지 가능하며 -,_,. 를 제외한 특수문자( 공백,자음,모음 포함) 사용은 불가합니다."
                "\n\n“사용자”는 아래 각 호에 해당하는 규칙에 따라 닉네임 사용에 제재 당할 수 있으며, “사용자”가 아래 각 호에 해당하는 닉네임을 사용하고자 하거나 사용하고 있는 경우 “회사”는 닉네임 부여를 중단하거나 닉네임 변경 요구를 할 수 있습니다."
                "\n\n① “사용자” 닉네임이 “회원”의 주소, 휴대폰 번호 등으로 등록되어 사생활 침해의 우려가 있는 경우"
                "\n② 타인에게 혐오감을 주거나 청소년 및 아동에 유해하다고 판단하는 경우"
                "\n③ “사용자” 닉네임이 다른 “사용자” 및 “셀럽”의 명칭과 중복되거나 혼란을 줄 수 있다고 판단 되는 경우"
                "\n④ 회사가 판단하는 바 합리적인 사유가 있는 경우",
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 12,
            ),
          )
        ],
      ),
    );
  }
}

class _EditNicknameAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      Provider.of<MyPageEditProvider>(context, listen: false).name = null;
                      Navigator.pop(context);
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            FontAwesomeIcons.times,
                            size: 30,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "닉네임 수정",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      httpEditName(context: context, name: Provider.of<MyPageEditProvider>(context, listen: false).name).then((value) {
                        if(value&&Provider.of<MyPageEditProvider>(context, listen:false).validatePressed) Navigator.pop(context);
                        else Fluttertoast.showToast(msg: "닉네임 변경에 실패했습니다...", gravity: ToastGravity.TOP);
                      });

                    },
                    behavior: HitTestBehavior.translucent,
                    child: Text(
                      "완료",
                      style: TextStyle(
                        fontSize: 15,
                        color: kAppBarButtonColor,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(width: 30,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<bool> _checkName(String name) async {
  Map<String, String> body = {
    "name": name
  };
  dynamic response = await postAsGuest("/misc/name/duplication", body);

  return response["status"] == 200;
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';

class CelebPushSettingsBody extends StatefulWidget {
  @override
  _CelebPushSettingsBodyState createState() => _CelebPushSettingsBodyState();
}

class _CelebPushSettingsBodyState extends State<CelebPushSettingsBody> {
  List<CelebData> _subscribingCelebs;
  List<bool> _celebPushOn;
  bool _allOn;

  @override
  void initState() {
    super.initState();
    _subscribingCelebs = [];
//    // TODO get subscribing celebs
//    _subscribingCelebs = [
//      CelebData(
//          name: "윤도현",
//          greeting: "안녕하세요"
//      ),
//      CelebData(
//          name: "윤도현",
//          greeting: "안녕하세요"
//      ),
//      CelebData(
//          name: "윤도현",
//          greeting: "안녕하세요"
//      ),
//    ];
    // TODO get previous settings
    _celebPushOn = [];
    _subscribingCelebs.forEach((_) => _celebPushOn.add(true));
    _allOn = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color.fromRGBO(249, 249, 249, 1)
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              // entire switch
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Text(
                      "전체",
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 12
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Container(
                      width: 55,
                      child: Switch(
                        value: _allOn,
                        onChanged: (value) => setState(() => _allOn = value),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: _subscribingCelebs.map((e) => _CelebAlertSwitch(
                      profileImgUrl: e.profileImgUrl,
                      celebName: e.name,
                      greeting: e.greeting,
                      on: _celebPushOn[_subscribingCelebs.indexOf(e)],
                      onChanged: (value) => setState(() => _celebPushOn[_subscribingCelebs.indexOf(e)] = value),
                    )).toList(),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}

class _CelebAlertSwitch extends StatelessWidget {
  final String profileImgUrl;
  final String celebName;
  final String greeting;
  final bool on;
  final Function(bool) onChanged;

  const _CelebAlertSwitch({
    Key key,
    this.profileImgUrl,
    this.celebName,
    this.greeting,
    this.on,
    this.onChanged
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            blurRadius: 6,
            offset: Offset(0, 3)
          )
        ]
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: CelebListInfoItem(
              celebName: celebName,
              greeting: greeting,
              profileImgUrl: profileImgUrl,
            ),
          ),
          Container(
            width: 65,
            child: Switch(
              value: on,
              onChanged: onChanged,
            ),
          )
        ],
      ),
    );
  }
}

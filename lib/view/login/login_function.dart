
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_naver_login/flutter_naver_login.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/data/login_data.dart';
import 'package:rf_tap_fanseem/http/login/apple_login.dart';
import 'package:rf_tap_fanseem/http/login/dto/http_login_response_dto.dart';
import 'package:rf_tap_fanseem/http/login/naver_login.dart';
import 'package:rf_tap_fanseem/http/login/naver_logout.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

import '../../routes.dart';
enum SocialLoginStatus {
  success,
  requiresSignUp,
  failCancelled,
  failInvalidPermissions,
  failNaverFailure,
  failAppleFailure,
  failApplicationFailure,
  failServerFailure
}
void loginWithSocial(BuildContext context, LoginSocial social) async {
  SocialLoginStatus status;
  switch (social){
    case LoginSocial.naver:
      status = await NaverLoginController().naverSignIn(context);
      break;
    case LoginSocial.apple:
      status = await AppleLoginController().appleSignIn(context);
      break;
    default:
      print("todo");
      break;
  }

  switch(status){
    case SocialLoginStatus.success:
      Navigator.pop(context);
      break;
    case SocialLoginStatus.requiresSignUp:
      Navigator.popAndPushNamed(context, kRouteSignUp);
      break;
    case SocialLoginStatus.failCancelled:
      Fluttertoast.showToast(msg: "로그인을 취소하였습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      break;
    case SocialLoginStatus.failInvalidPermissions:
      Fluttertoast.showToast(msg: "필요 항목을 전부 체크해주세요!", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      break;
    case SocialLoginStatus.failNaverFailure:
      Fluttertoast.showToast(msg: "네이버 로그인중 문제가 발생하여 로그인에 실패하였습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      break;
    case SocialLoginStatus.failAppleFailure:
      Fluttertoast.showToast(msg: "애플 로그인중 문제가 발생하여 로그인에 실패하였습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      break;
    case SocialLoginStatus.failApplicationFailure:
    case SocialLoginStatus.failServerFailure:
      Fluttertoast.showToast(msg: "알 수 없는 문제가 발생하였습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      break;
  }
//  Navigator.pushNamed(context, kRouteSignUp);
}

class NaverLoginController{
Future<SocialLoginStatus> naverSignIn(BuildContext context) async {
    try{
      NaverLoginResult result = await FlutterNaverLogin.logIn();
      switch(result.status){
        case NaverLoginStatus.loggedIn:
          break;
        case NaverLoginStatus.cancelledByUser:
          return SocialLoginStatus.failCancelled;
        case NaverLoginStatus.error:
          return SocialLoginStatus.failNaverFailure;
      }

      NaverAccessToken token = await FlutterNaverLogin.currentAccessToken;
      if(result.account.id == null) return SocialLoginStatus.failInvalidPermissions;
//      if(result.account.name == null) return SocialLoginStatus.failInvalidPermissions;
//      if(result.account.gender == null) return SocialLoginStatus.failInvalidPermissions;
//      if(result.account.age == null) return SocialLoginStatus.failInvalidPermissions;
//      if(result.account.birthday == null) return SocialLoginStatus.failInvalidPermissions;
//      if(result.account.email == null) return SocialLoginStatus.failInvalidPermissions;

      String deviceTokenId =  await FirebaseMessaging().getToken();
      HttpLoginResponseDto response = await httpNaverLogin(deviceTokenId: deviceTokenId, accessToken: token.accessToken);
      final FlutterSecureStorage storage = FlutterSecureStorage();
      Map userData = {
        "userId": response.userId,
        "isCeleb": response.isCeleb,
        "social": "naver",
        "token": token.accessToken,
        "deviceTokenId": deviceTokenId
      };
      await storage.write(key: "ai_maum_fanmeet_user_data", value: jsonEncode(userData));

      switch(response.status){
        case 200:
          break;
        case 401:
          return SocialLoginStatus.requiresSignUp;
        case 404:
        case 500:
          return SocialLoginStatus.failServerFailure;
      }

      LoginVerifyProvider loginProvider = Provider.of<LoginVerifyProvider>(context, listen: false);
      loginProvider.setLoginInfo(true, "naver", token.accessToken, response.isCeleb, response.userId);

      return SocialLoginStatus.success;
    } catch (e){
      print(e);
      FlutterNaverLogin.logOut();
      final FlutterSecureStorage storage = FlutterSecureStorage();
      await storage.delete(key: "ai_maum_fanmeet_user_data");
      Provider.of<LoginVerifyProvider>(context, listen: false).setLoginInfo(false, "", "", false, null);
      return SocialLoginStatus.failApplicationFailure;
    }
  }
}

class AppleLoginController {
  Future<SocialLoginStatus> appleSignIn(BuildContext context) async {
    try {
      final credential = await SignInWithApple.getAppleIDCredential(scopes: [
        AppleIDAuthorizationScopes.email,
        AppleIDAuthorizationScopes.fullName
      ]);
      String deviceTokenId =  await FirebaseMessaging().getToken();
      final FlutterSecureStorage storage = FlutterSecureStorage();
      HttpLoginResponseDto response = await httpAppleLogin(
          deviceTokenId: deviceTokenId, authorizationCode: credential.authorizationCode, accessToken: credential.identityToken);
      Map userData = {};

      if(response.status==401) {
        userData = {
          "social": "apple",
          "authorizationCode": credential.authorizationCode,
          "deviceTokenId": deviceTokenId,
          "token": credential.identityToken,
        };
      }else{
        userData = {
          "userId": response.userId,
          "isCeleb": response.isCeleb,
          "social": "apple",
          "authorizationCode": credential.authorizationCode,
          "deviceTokenId": deviceTokenId,
          "token": response.iosRefreshToken,
        };
      }

      await storage.write(
          key: "ai_maum_fanmeet_user_data", value: jsonEncode(userData));
      switch (response.status) {
        case 200:
          break;
        case 401:
          return SocialLoginStatus.requiresSignUp;
        case 404:
        case 500:
          return SocialLoginStatus.failServerFailure;
      }

      LoginVerifyProvider loginProvider = Provider.of<LoginVerifyProvider>(
          context, listen: false);
      loginProvider.setLoginInfo(
          true, "apple", response.iosRefreshToken, response.isCeleb,
          response.userId);

      return SocialLoginStatus.success;
    }
    catch(e){
      print(e);
      final FlutterSecureStorage storage = FlutterSecureStorage();
      await storage.delete(key: "ai_maum_fanmeet_user_data");
      Provider.of<LoginVerifyProvider>(context, listen: false).setLoginInfo(false, "", "", false, null);
      return SocialLoginStatus.failApplicationFailure;
    }
  }
}
Future<bool> logout(BuildContext context) async {
    await httpNaverLogout(context: context);
    FlutterNaverLogin.logOut();

  Provider.of<LoginVerifyProvider>(context, listen: false).setLoginInfo(
      false, "", "", false, -1);
  final FlutterSecureStorage storage = FlutterSecureStorage();
  await storage.delete(key: "ai_maum_fanmeet_user_data");
  return true;
}
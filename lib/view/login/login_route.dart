

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/data/login_data.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/login/login_function.dart';
import 'package:rf_tap_fanseem/view/login/privacy_policy.dart';


class LoginRoute extends StatefulWidget {
  @override
  _LoginRouteState createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  bool _autoLogin;

  @override
  void initState() {
    super.initState();
    _autoLogin = true;
  }

  @override
  Widget build(BuildContext context) {
    double buttonWidth = MediaQuery.of(context).size.width - 140;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 62,
            ),
            SizedBox(
              height: 41,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Image.asset("assets/icon/seem_off_3x.png", color: Theme.of(context).backgroundColor, fit: BoxFit.contain,),
                  SizedBox(width: 32,)
                ],
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "환영합니다 :-)",
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).backgroundColor
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "지금 바로 셀럽과의 만남을 시작해 보세요.",
                    style: TextStyle(
                        fontSize: 15,
                        color: Theme.of(context).backgroundColor
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 7,
            ),
            Padding(
              padding: const EdgeInsets.all(30),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.36),
                          offset: Offset(0, 3),
                          blurRadius: 12
                      )
                    ]
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 23,
                    ),
                    _SocialLoginButton(
                      width: buttonWidth,
                      social: LoginSocial.naver,
                    ),
                    SizedBox(
                      height: 11,
                    ),
//                    _SocialLoginButton(
//                      width: buttonWidth,
//                      social: LoginSocial.google,
//                    ),
//                    SizedBox(
//                      height: 11,
//                    ),
//                    _SocialLoginButton(
//                      width: buttonWidth,
//                      social: LoginSocial.kakao,
//                    ),
                  /*
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          value: _autoLogin,
                          onChanged: (bool value) => setState(() => _autoLogin = value),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "자동 로그인",
                          style: TextStyle(
                              fontSize: 12,
                              color: kSelectableTextColor
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        )
                      ],
                    ),
                    */
                    SizedBox(height: 10,)
                  ],
                ),
              ),
            ),
            Center(
              child: GestureDetector(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PrivacyPolicy(), maintainState: true)),
                child: Text(
                  "이용 약관 및 개인정보 처리방침",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).backgroundColor
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _SocialLoginButton extends StatelessWidget {
  final double width;
  final LoginSocial social;

  const _SocialLoginButton({Key key, this.width, this.social})
      : assert(width != null),
        assert(social != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => loginWithSocial(context, social),
      child: Container(
        width: width,
        height: 60,
        decoration: _buttonDecoration(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 19,
            ),
            // TODO icon image per social
            Container(
              width: 17,
              height: 20,
              child: Image.asset("assets/icon/naver_icon.png", fit: BoxFit.contain,)
//              child: Icon(Icons.add),
            ),
            Expanded(
              child: Center(
                child: Text(
                  _buttonText(),
                  style: TextStyle(
                    color: social != LoginSocial.naver ? Colors.black : Theme.of(context).backgroundColor,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 36,
              height: 36,
            )
          ],
        ),
      ),
    );
  }

  String _buttonText(){
    switch(social){
      case LoginSocial.naver:
        return "네이버로 로그인";
      case LoginSocial.kakao:
        return "카카오톡으로 로그인";
      case LoginSocial.google:
        return "구글 아이디로 로그인";
      default:
        throw UnsupportedError("Unknown social login type!");
    }
  }

  BoxDecoration _buttonDecoration(){
    switch(social){
      case LoginSocial.naver:
        return BoxDecoration(
            color: Color.fromRGBO(71, 181, 72, 1),
            borderRadius: BorderRadius.all(Radius.circular(12))
        );
      case LoginSocial.kakao:
        return BoxDecoration(
            color: Color.fromRGBO(254, 205, 47, 1),
            borderRadius: BorderRadius.all(Radius.circular(12))
        );
      case LoginSocial.google:
        return BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 1),
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(
                offset: Offset(1, 1),
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 2
              )
            ]
        );
      default:
        throw UnsupportedError("Unknown social login type!");
    }
  }
}


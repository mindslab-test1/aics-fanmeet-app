import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/content/content_edit.dart';
import 'package:rf_tap_fanseem/providers/content_edit_data_provider.dart';
import 'package:rf_tap_fanseem/view/content_edit/content_edit_body.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_view_provider.dart';

import '../../colors.dart';

class ContentEditRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _ContentEditAppBar(),
      ),
      body: ContentEditBody(),
    );
  }
}

class _ContentEditAppBar extends StatelessWidget {
  ProgressDialog _pr;

  @override
  Widget build(BuildContext context) {
    ContentEditDataProvider _contentEditProvider = Provider.of<ContentEditDataProvider>(context, listen: false);
//    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
//      return _pr == null ? true : false;
//    });
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                Provider.of<FeedEditViewProvider>(context).title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: () async {
                        _contentEditProvider.isUploadingVideo=true;
                        await _pr.show();
                        Future.delayed(Duration(milliseconds: 200));
                        if (_contentEditProvider.text == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "빈 텍스트는 업로드할 수 없습니다.", gravity: ToastGravity.TOP);
                          return;
                        }
                        bool isSuccess = await httpContentEdit(
                            context: context,
                            categoryId: _contentEditProvider.categoryId,
                            pictures: _contentEditProvider.pictures,
                            addedPictures: _contentEditProvider.imageUrls,
                            videoUrl: _contentEditProvider.videoUrl,
                            youtubeUrl: _contentEditProvider.youtubeUrl,
                            removedPictures: _contentEditProvider.removedPictures,
                            ttsId: _contentEditProvider.ttsId,
                            text: _contentEditProvider.text,
                            title: _contentEditProvider.title,
                            accessLevel: _contentEditProvider.tier,
                            contentId: _contentEditProvider.contentId
                        );
                        if (isSuccess) {
                          await _pr.hide();
                          Navigator.pop(context, true);
                        }
                        else {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "피드 업로드에 실패했습니다.", gravity: ToastGravity.TOP);
                        }
                        _contentEditProvider.isUploadingVideo=false;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: kAppBarButtonColor,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


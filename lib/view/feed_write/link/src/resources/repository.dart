import 'package:rf_tap_fanseem/view/feed_write/link/src/model/media_info.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/resources/api_provider.dart';

class Repository {
  final moviesApiProvider = ThumbnailApiProvider();
  Future<MediaInfo> fetchAllNews({String link = ''}) => moviesApiProvider.fetchMediaInfo(link);
}
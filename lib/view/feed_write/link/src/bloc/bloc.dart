import 'dart:async';

import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/thumbnail_event.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/thumbnail_state.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/resources/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ThumbnailBloc extends Bloc<ThumbnailEvent, ThumbnailState> {
  final Repository repo = Repository();

  
  @override
  ThumbnailState get initialState => DialogOpened();

  @override
  Stream<ThumbnailState> mapEventToState(ThumbnailEvent event) async* {
    if (event is AddUrl) {
      try {
        yield LoadingMedia();
        var media = await repo.fetchAllNews(link: event.url);
        if(media.providerUrl != "https://www.youtube.com/"){
          yield FailureDetail();
        }
        else yield LoadedMedia(mediaInfo: media);
      } catch (_) {
        yield FailureDetail();
      }
    }
    if (event is UrlChange) {
      yield UrlChanged();
    }
    if (event is CancelEvent) {
      yield Cancelled();
    }
  }
}

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_write.dart';
import 'package:rf_tap_fanseem/http/feed/feed_write.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/feed_write/feed_write_body.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';

import '../../colors.dart';

class FeedWriteRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _FeedWriteAppBar(),
      ),
      body: FeedWriteBody(),
    );
  }
}

class _FeedWriteAppBar extends StatefulWidget {
  @override
  __FeedWriteAppBarState createState() => __FeedWriteAppBarState();
}

class __FeedWriteAppBarState extends State<_FeedWriteAppBar> {
  ProgressDialog _pr;

  @override
  Widget build(BuildContext context) {
    FeedWriteDataProvider _feedWriteDataProvider = Provider.of<FeedWriteDataProvider>(context, listen: false);
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                Provider.of<FeedWriteViewProvider>(context).title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: () async {
                        _feedWriteDataProvider.isUploadingVideo=true;
                        await _pr.show();
                        await Future.delayed(Duration(milliseconds: 200));
                        if (_feedWriteDataProvider.text == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(
                              msg: "빈 텍스트는 업로드할 수 없습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        bool isUserFeedWrite = (
                            Provider.of<LoginVerifyProvider>(context, listen: false).userId !=
                                Provider.of<SelectedCelebProvider>(context, listen: false).celebId
                        );

                        bool isSuccess = await (isUserFeedWrite ? httpFeedWrite(
                            context: context,
                            celebId: Provider
                                .of<SelectedCelebProvider>(context, listen: false)
                                .celebId,
                            categoryId: _feedWriteDataProvider.categoryId,
                            text: _feedWriteDataProvider.text,
                            imageUrls: _feedWriteDataProvider.imageUrls,
                            youtubeUrl: _feedWriteDataProvider.youtubeUrl,
                            videoUrl: _feedWriteDataProvider.videoUrl
                        ) : httpCelebFeedWrite(
                            context: context,
                            categoryId: _feedWriteDataProvider.categoryId,
                            text: _feedWriteDataProvider.text,
                            imageUrls: _feedWriteDataProvider.imageUrls,
                            videoUrl: _feedWriteDataProvider.videoUrl,
                            youtubeUrl: _feedWriteDataProvider.youtubeUrl,
                            accessLevel: _feedWriteDataProvider.accessLevel
                        ));
                        if (isSuccess) {
                          await _pr.hide();
                          Navigator.pop(context, true);
                        }
                        else {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "피드 업로드에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        }
                        _feedWriteDataProvider.isUploadingVideo = false;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: kAppBarButtonColor,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}


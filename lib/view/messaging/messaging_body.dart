
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_message/celeb_write_message.dart';
import 'package:rf_tap_fanseem/http/message/message_celeb_list.dart';
import 'package:rf_tap_fanseem/view/main/dialog/tts_message_dialog.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';

import '../../colors.dart';
import 'component/messaging_data.dart';

class MessagingBody extends StatefulWidget {
  final List<UserMessageData> initialData;

  const MessagingBody({
    Key key,
    this.initialData = const []
  }) : super(key: key);

  @override
  _MessagingBodyState createState() => _MessagingBodyState();
}

class _MessagingBodyState extends State<MessagingBody> {
  List<UserMessageData> _displayedData = [];
  ScrollController _scrollController = ScrollController();
  bool _endOfMessages;
  bool _loading;

  @override
  void initState() {
    super.initState();
    _displayedData.addAll(widget.initialData);
    _endOfMessages = false;
    _loading = false;
    _scrollController.addListener(() {
      if(!_endOfMessages && !_loading && _scrollController.position.pixels >= _scrollController.position.maxScrollExtent) {
        // TODO check for previous messages
//        _getMoreData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
//        future: _getData(),
          builder: (context, snapshot) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Container(
                    child: _displayedData.isNotEmpty ? SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      controller: _scrollController,
                      reverse: true,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          _loading ? Container(height: 200, child: Center(child: CircularProgressIndicator(),),) : Container(),
                          Column(
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: _displayedData.map((e) => Padding(
                              padding: EdgeInsets.only(
                                  top: 10, bottom: 10,
                                  left: e.isSender ? 48 : 15, right: e.isSender ? 15 : 48
                              ),
                              child: MessageBlock.buildWith(e.data),
                            )).toList(),
                          ),
                        ],
                      ),
                    ) : Container(
                      height: 200,
                      child: Center(
                        child: Text(
                            "아직 온 메시지가 없습니다..."
                        ),
                      ),
                    ),
                  ),
                ),
                // control
                Container(
                  height: 58,
                  decoration: BoxDecoration(
                      color: Theme.of(context).backgroundColor,
                      boxShadow: [
                        BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.16),
                          blurRadius: 6,
                        )
                      ]
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: Provider.of<MessagingDisplayProvider>(context).type != MessagingType.getCeleb ? <Widget>[
                      SizedBox(width: 20,),
                      Icon(
                        FontAwesomeIcons.plus,
                        color: Color.fromRGBO(159, 159, 159, 1),
                        size: 15,
                      ),
                      SizedBox(width: 22,),
                      Expanded(
                          child: Container(
                            height: 45,
                            decoration: BoxDecoration(
                                color: Theme.of(context).backgroundColor,
                                borderRadius: BorderRadius.circular(18),
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 6,
                                    color: Color.fromRGBO(0, 0, 0, 0.16),
                                  )
                                ]
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Expanded(
                                  child: Container(),
                                ),
                                Container(
                                  width: 64,
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).backgroundColor,
                                      borderRadius: BorderRadius.circular(18),
                                      boxShadow: [
                                        BoxShadow(
                                          blurRadius: 6,
                                          color: Color.fromRGBO(0, 0, 0, 0.16),
                                        )
                                      ]
                                  ),
                                  child: Center(
                                    child: Text(
                                      "전송",
                                      style: TextStyle(
                                          color: Color.fromRGBO(203, 203, 203, 1),
                                          fontSize: 12
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                      ),
                      SizedBox(width: 14,),
                    ] : <Widget>[
                      Container()
                    ],
                  ),
                )
              ],
            );
          }
      ),
    );
  }

  void _getMoreData() async {
    setState(() => _loading = true);

    await Future.delayed(Duration(seconds: 2));

    setState(() {
      _loading = false;
      _displayedData.addAll(_testAddedUserMessages);
      _endOfMessages = true;
    });

  }
}


class CelebMessagingBody extends StatefulWidget {
  @override
  _CelebMessagingBodyState createState() => _CelebMessagingBodyState();
}

class _CelebMessagingBodyState extends State<CelebMessagingBody> {
  ScrollController _scrollController = ScrollController();
  TextEditingController _messageController = TextEditingController();
  ProgressDialog _pr;

  int inputLineCount;
  double _inputHeight;

  @override
  void initState(){
    super.initState();
    inputLineCount = 1;
    _inputHeight = 45;
  }

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    MessagingDisplayProvider provider = Provider.of<MessagingDisplayProvider>(context);

    _scrollController.addListener(() {
      if(!Provider.of<MessagingDisplayProvider>(context, listen: false).endOfData &&
          !Provider.of<MessagingDisplayProvider>(context, listen: false).loading &&
          _scrollController.position.pixels >= _scrollController.position.maxScrollExtent) {
        _getMoreData(context);
      }
    });

    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: FutureBuilder(
                future: _getData(context),
                builder: (context, snapshot) {
                  if(snapshot.hasData)
                    return Container(
                      child: Provider.of<MessagingDisplayProvider>(context).displayMessageData.isNotEmpty ? SingleChildScrollView(
                        physics: BouncingScrollPhysics(),
                        controller: _scrollController,
                        reverse: true,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            provider.loading ? Container(height: 200, child: Center(child: CircularProgressIndicator(),),) : Container(),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: Provider.of<MessagingDisplayProvider>(context).displayMessageData.reversed.map((e) => Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                                child: MessageBlock.buildWith(e),
                              )).toList(),
                            ),
                          ],
                        ),
                      ) : Container(
                        height: 200,
                        child: Center(
                          child: Text(
                              "아직 온 메시지가 없습니다..."
                          ),
                        ),
                      ),
                    );
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
            ),
          ),
          // control
          Provider.of<MessagingDisplayProvider>(context).type != MessagingType.getCeleb ? Container(
//            height: 58,
            height: _inputHeight + 13,
            decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    blurRadius: 6,
                  )
                ]
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 10),
                IconButton(
                    icon: Icon(FontAwesomeIcons.plus),
                    color: Color.fromRGBO(159, 159, 159, 1),
                    iconSize: 15,
                    onPressed: () async {
                      bool result = await showGeneralDialog(
                          context: context,
                          pageBuilder: (context, animation, secondAnimation) => TtsMessageDialog(),
                          barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                          barrierDismissible: false,
                          transitionDuration: const Duration(microseconds: 150)
                      );
                      if(result != null && result){
                        Provider.of<MessagingDisplayProvider>(context, listen: false).displayMessageData = [];
                        Provider.of<MessagingDisplayProvider>(context, listen: false).init = false;
                        setState(() {});
                      }
                    }
                ),
                SizedBox(width: 10),
                Expanded(
                    child: Container(
                      height: _inputHeight,
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(18),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 6,
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                            )
                          ]
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.only(left: 20, right: 13, bottom: 3),
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                    minHeight : 45,
                                    maxHeight : 150
                                ),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  child: TextField(
                                    controller: _messageController,
                                    decoration: InputDecoration(
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                                      ),
                                      focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                                      ),
                                      border: UnderlineInputBorder(
                                        borderSide: BorderSide(color: Colors.transparent, width: 0),
                                      ),
                                    ),
                                    maxLines: null,
                                    onChanged: (String value){
                                      int count = ('\n').allMatches(value).length;
                                      if (inputLineCount != count && count <= 3){
                                        var inputHeight = 50 + (count * 18);
                                        setState(() {
                                          inputLineCount = count;
                                          _inputHeight = inputHeight.toDouble();
                                        });
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              await _pr.show();
                              await Future.delayed(Duration(milliseconds: 200));
                              if (_messageController.text == null ||
                                  _messageController.text.length == 0) {
                                await _pr.hide();
                                Fluttertoast.showToast(msg: "텍스트를 입력해주세요", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                              } else {
                                httpCelebSendMessage(context: context, text: _messageController.text).then((value) {
                                  if (value) {
                                    _pr.hide();
                                    Fluttertoast.showToast(msg: "메세지가 전송 되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                    _messageController.clear();
                                    Provider.of<MessagingDisplayProvider>(context, listen: false).displayMessageData = [];
                                    Provider.of<MessagingDisplayProvider>(context, listen: false).init = false;
                                    setState(() {});
                                  } else {
                                    _pr.hide();
                                    Fluttertoast.showToast(msg: "메세지 전송에 실패하였습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                  }
                                });
                              }
                            },
                            behavior: HitTestBehavior.translucent,
                            child: Container(
                              width: 64,
                              decoration: BoxDecoration(
                                  color: Theme.of(context).backgroundColor,
                                  borderRadius: BorderRadius.circular(18),
                                  border: Border.all(
                                      color: Colors.grey
                                  ),
                                  boxShadow: [
                                    BoxShadow(
                                      blurRadius: 6,
                                      color: Color.fromRGBO(0, 0, 0, 0.16),
                                    )
                                  ]
                              ),
                              child: Center(
                                child: Text(
                                  "전송",
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                ),
                SizedBox(width: 14,),
              ],
            ),
          ) : Container(height: 30,)
        ],
      ),
    );
  }

  Future<bool> _getData(BuildContext context) async {
    switch(Provider.of<MessagingDisplayProvider>(context, listen: false).type){

      case MessagingType.getCeleb:
        return await httpMessageCelebMessageList(context: context);
      case MessagingType.getUser:
        throw UnimplementedError("unimplemented!");
      case MessagingType.celebSend:
        return await httpMessageCelebMyMessageList(context: context);
    }
  }

  Future<bool> _getMoreData(BuildContext context) async {
    if(Provider.of<MessagingDisplayProvider>(context, listen: false).loading) return true;
    Provider.of<MessagingDisplayProvider>(context, listen: false).loading = true;
    return await httpMessageCelebAddPrevMessage(context: context);
  }
}

List<BaseMessageData> _testAddedBaseMessages = [
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "추가된 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "또 추가된 메시지",
      type: MessageType.text
  )
];

List<UserMessageData> _testAddedUserMessages = _testAddedBaseMessages.asMap().entries.map((e) => UserMessageData(
    isSender: e.key % 2 == 0,
    data: e.value
)).toList();
//final String timestamp;
//final String text;
//final MessageType type;
//final String membership;
//final String contentUrl;


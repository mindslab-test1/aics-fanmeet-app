import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_edit_comment.dart';
import 'package:rf_tap_fanseem/http/content/content_edit_comment.dart';
import 'package:rf_tap_fanseem/http/feed/feed_edit_comment.dart';
import 'package:rf_tap_fanseem/providers/comment_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/view/style/text/default.dart';

import '../../colors.dart';
import '../feed_detail/feed_view_components.dart';

class CommentEditBody extends StatefulWidget {
  final String title;

  const CommentEditBody({
    Key key,
    this.title,
  }) : super(key: key);


  @override
  _CommentEditBodyState createState() => _CommentEditBodyState();

}

class _CommentEditBodyState extends State<CommentEditBody> {

  @override
  Widget build(BuildContext context) {
      return Container(
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    FeedCommentEditView(
                      comments: Provider.of<CommentEditDataProvider>(context,listen: false).commentDataList,
                      highlightCommentId: Provider.of<CommentEditDataProvider>(context,listen: false).commentId,
                    ),
                  ],
                ),
              ),
            ),
            _CommentEditBar(commentText: Provider.of<CommentEditDataProvider>(context,listen: false).commentText,)
          ],
        ),
      );
  }
}

class _CommentEditBar extends StatefulWidget {
  final String commentText;

  const _CommentEditBar({Key key, this.commentText}) : super(key: key);

  @override
  __CommentEditBarState createState() => __CommentEditBarState();
}

class __CommentEditBarState extends State<_CommentEditBar> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    _controller.text = widget.commentText;
    super.initState();
  }
  ProgressDialog _pr;
  @override
  Widget build(BuildContext context) {
    FeedDataProvider _feedDataProvider = Provider.of<FeedDataProvider>(context,listen: false);
    bool isSuccess = false;
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "댓글 수정중 입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Column(
      children: [
        Container(
          height: 40,
          color: kProfileButtonColor,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  child: Text(
                    "댓글 수정중",
                    style: kEditCommentInfoTextStyle,
                    textAlign: TextAlign.center,
                  ),
                  padding: EdgeInsets.only(left: 48,right: 6),
                ),
              ),
              GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Container(
                    width: 40,
                    padding: EdgeInsets.only(right: 16),
                    child: Center(
                        child: Icon(FontAwesomeIcons.times, size: 12 )
                    )
                ),
              )
            ],
          ),
        ),
        Container(
            height: 164,
            color: kMainBackgroundColor,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    margin : EdgeInsets.only(left: 16, top: 5, right: 6, bottom: 39),
                    height: 120,
                    child: Container(
                      height: 120,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(
                              color: kProfileButtonColor
                          )
                      ),
                      child: TextField(
                        controller: _controller,
                        style: kEditCommentTextStyle,
                        maxLines: null,
                        keyboardType: TextInputType.multiline,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 16, right: 16, top: 12, bottom: 12),
                        ),
                      ),
                    ),
                  ),
                ),
                GestureDetector(
                  onTap : () async {
                    await _pr.show();
                    if (_controller.text.length > 200) {
                      await _pr.hide();
                      Fluttertoast.showToast(msg: "작성 가능 글자수를 넘었습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    }
                    else{
                      if(_feedDataProvider.isUserFeed){
                        isSuccess  = await httpFeedEditComment(
                            context: context,
                            comment: Provider.of<CommentEditDataProvider>(context, listen: false).commentId,
                            text: _controller.text
                        );
                      }
                      else if(_feedDataProvider.isContent){
                        isSuccess  = await httpContentEditComment(
                            context: context,
                            comment: Provider.of<CommentEditDataProvider>(context, listen: false).commentId,
                            text: _controller.text
                        );
                      }
                      else{
                        isSuccess  = await httpCelebFeedEditComment(
                            context: context,
                            comment: Provider.of<CommentEditDataProvider>(context, listen: false).commentId,
                            text: _controller.text
                        );
                      }
                      if(isSuccess){
                        await _pr.hide();
                        Navigator.pop(context);
                      }
                      else{
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "댓글 수정에 실패하였습니다.", gravity: ToastGravity.TOP);
                      }
                    }
                  },
                  child: Container(
                    height: 40,
                    width: 64,
                    margin: EdgeInsets.only(top: 85, right: 14, bottom: 39),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.horizontal(left: Radius.circular(20), right: Radius.circular(20)),
                        color: kMainColor
                    ),
                    child: Center(
                      child: Text(
                        "수정",
                        style: kEditButtonTextStyle,
                      ),
                    ),
                  ),
                )
              ],
            )
        )
      ],
    );
  }
}

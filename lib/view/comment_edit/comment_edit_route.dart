import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_view_provider.dart';

import '../../colors.dart';
import 'comment_edit_body.dart';

class CommentEditRoute extends StatefulWidget {
  @override
  _CommentEditRouteState createState() => _CommentEditRouteState();
}

class _CommentEditRouteState extends State<CommentEditRoute> {

  void refreshFeedDetail(){
    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: kAppBarMainColor,
        title: _CommentEditAppBar(),
      ),
      body: CommentEditBody(),
    );
  }
}

class _CommentEditAppBar extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "댓글 수정하기",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(),
          )
        ]
      )
    );
  }
}


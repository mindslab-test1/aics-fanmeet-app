import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/content_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_edit_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:video_player/video_player.dart';
import '../routes.dart';
import 'package:async/async.dart';

class VideoPlayerWidget extends StatefulWidget {
  final bool isNetwork;
  final bool autoPlay;
  final String url;
  const VideoPlayerWidget(
      {Key key, this.isNetwork = false, this.autoPlay = false, this.url})
      : super(key: key);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  AsyncMemoizer _memoizer = new AsyncMemoizer();
  VideoPlayerController _playerController;
  ChewieController _chewieController;

  List<String> getBackButtonInterceptorName() {
    List<String> list = BackButtonInterceptor.describe().split(new RegExp(r"([ |\n])"));
    List<String> names = [];
    for (int i = 0; i < list.length; i++) {
      if (list[i] == "BackButtonInterceptor:") {
        names.add(list[i + 1].substring(0, list[i + 1].length - 1));
      }
    }
    return names;
  }

  bool isUploadingVideo(){
    ContentWriteDataProvider _contentWriteDataProvider = Provider.of<ContentWriteDataProvider>(context, listen: false);
    FeedWriteDataProvider _feedWriteDataProvider = Provider.of<FeedWriteDataProvider>(context, listen: false);
    ContentEditDataProvider _contentEditDataProvider = Provider.of<ContentEditDataProvider>(context, listen: false);
    FeedEditDataProvider _feedEditDataProvider = Provider.of<FeedEditDataProvider>(context, listen: false);

    if(_contentWriteDataProvider.isUploadingVideo) return true;
    if(_feedWriteDataProvider.isUploadingVideo) return true;
    if(_contentEditDataProvider.isUploadingVideo) return true;
    if(_feedEditDataProvider.isUploadingVideo) return true;

    return false;
  }

  setPlayer() {
    return this._memoizer.runOnce(() async {
      if (widget.isNetwork) {
        _playerController = VideoPlayerController.network(
          widget.url,
        );
      } else {
        _playerController = VideoPlayerController.file(File(widget.url));
      }
      await _playerController.initialize();
      _playerController.addListener(() async {
        if (_playerController.value.duration ==
            _playerController.value.position) {
          await _playerController
              .seekTo(Duration(hours: 0, minutes: 0, seconds: 0));
          await _playerController.pause();
        }
      });
      _chewieController = ChewieController(
          aspectRatio: _playerController.value.size.width /
              _playerController.value.size.height,
          videoPlayerController: _playerController,
          deviceOrientationsAfterFullScreen: [DeviceOrientation.portraitUp],
          fullScreenByDefault: false,
          autoPlay: false,
          looping: false,
          allowedScreenSleep: false,
          autoInitialize: true
      );
      _chewieController.addListener(() {
        if (_chewieController.isFullScreen) {
          BackButtonInterceptor.removeByName("pop");
          BackButtonInterceptor.add((pop, _) {
            _chewieController.exitFullScreen();
            return true;
          }, name: "fullscreen");
        }
        else {
          if (getBackButtonInterceptorName().last == "fullscreen") {
            BackButtonInterceptor.removeByName("fullscreen");
            BackButtonInterceptor.add((pop, _) {
              if(isUploadingVideo()){
                Fluttertoast.showToast(msg: "컨텐츠 업로드 중입니다.", gravity: ToastGravity.TOP);
              }
              else{
                Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
              }
              return true;
            }, name: "pop", zIndex: 2, ifNotYetIntercepted: true);
          }
        }
      });
      return true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (MediaQuery.of(context).size.width - 60) * 9 / 16,
      child: Center(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: FutureBuilder(
                future: setPlayer(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Chewie(
                      controller: _chewieController,
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                })),
      ),
    );
  }

  @override
  void dispose() {
    _playerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }
}



enum CategoryEnum{
  acting,
  singing,
  talking,
  cooking,
  exercise,
  streaming,
  games,
  songs,
  eating,
  asmr,
  arts,
  travel,
  beauty,
  dailyLife,
  engineer,
  movie,
  review,
  dance
}


class HomeFeedData {
  final int feedId;
  final String celebName;
  final String celebProfileImage;
  final String imgUrl;
  final String timeStamp;
  final String text;

  const HomeFeedData({
    this.feedId,
    this.celebName,
    this.celebProfileImage,
    this.imgUrl,
    this.timeStamp,
    this.text
  }) : assert(feedId != null),
        assert(celebName != null),
        assert(timeStamp != null);

  @override
  String toString() {
    return 'HomeFeedData{feedId: $feedId, celebName: $celebName, celebProfileImage: $celebProfileImage, imgUrl: $imgUrl, timeStamp: $timeStamp, text: $text}';
  }
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_view_components.dart';

class CommentEditDataProvider extends ChangeNotifier{
  int _commentId = -1;
  String _commentText;
  List<CommentData> _commentDataList = [];

  set commentId(int value) {
    _commentId = value;
    notifyListeners();
  }

  int get commentId => _commentId;

  set commentText(String value) {
    _commentText = value;
    notifyListeners();
  }

  String get commentText => _commentText;

  set commentDataList(List<CommentData> value) {
     _commentDataList = value;
    notifyListeners();
  }

  List<CommentData> get commentDataList => _commentDataList;

}
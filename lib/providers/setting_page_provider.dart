
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_utlis.dart';

class SettingPageProvider extends ChangeNotifier{
  int _index = 0;
  Settings _settings = Settings.user;

  int get index => _index;
  Settings get settings => _settings;

  void setPage(int index, Settings settings){
    _index = index;
    _settings = settings;
    notifyListeners();
  }
}

import 'package:flutter/material.dart';

class SubViewControllerProvider extends ChangeNotifier{
  Map<String, PageController> _subControllers = {

  };

  PageController get(String label) => _subControllers[label];

  void add(String pageLabel, PageController controller){
    this._subControllers[pageLabel] = controller;
    notifyListeners();
  }

  void pop(String pageLabel) {
    this._subControllers.remove(pageLabel);
    notifyListeners();
  }

  void flush(){
    this._subControllers = {};
    notifyListeners();
  }
}
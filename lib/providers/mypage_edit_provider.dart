
import 'package:flutter/material.dart';

class MyPageEditProvider extends ChangeNotifier{
  String _name;
  String _profileFileUri;
  String _bannerFileUri;
  List _interestedCategory = [];
  bool _validatePressed = false;

  String get name => _name;

  bool get validatePressed => _validatePressed;

  set validatePressed(bool value){
    _validatePressed = value;
    notifyListeners();
  }

  set name(String value) {
    _name = value;
    notifyListeners();
  }

  String get profileFileUri => _profileFileUri;

  set profileFileUri(String value) {
    _profileFileUri = value;
    notifyListeners();
  }

  List get interestedCategory => _interestedCategory;

  set interestedCategory(List value) {
    _interestedCategory = value;
    notifyListeners();
  }

  String get bannerFileUri => _bannerFileUri;

  set bannerFileUri(String value) {
    _bannerFileUri = value;
    notifyListeners();
  }

  // reset once exiting edit view
  void reset() {
    _name = null;
    _profileFileUri = null;
    _bannerFileUri = null;
    _interestedCategory = [];
  }
}
import 'package:flutter/material.dart';
import 'feed_edit_data_provider.dart';

class ContentEditDataProvider extends ChangeNotifier{

  int _categoryId = 0;
  String _title = "";
  List<String> _imageUrls = [];
  int _contentId = 0;
  String _videoUrl;
  String _youtubeUrl;
  String _ttsId;
  String _text = "";
  List<String> _pictures = [];
  List<String> _removedPictures = []; //TODO : 추후에 list<long>으로 몇번째 사진이 없어질지 결정
  FeedContentType _contentType = FeedContentType.none;
  int _tier = 0;
  bool _isUploadingVideo = false;

  void setContentTypeWithMedia(List<String> pictures){
    if(_imageUrls.isNotEmpty || pictures.isNotEmpty){
      _contentType=FeedContentType.images;
    }
    else if(_videoUrl!=""){
      _contentType=FeedContentType.video;
    }
    else if(_youtubeUrl!=""){
      _contentType=FeedContentType.youtube;
    }
    else{
      _contentType=FeedContentType.none;
    }
  }

  void setContentEditData({
    @required int categoryId,
    @required List<String> removedPictures,
    @required List<String> pictures,
    @required int contentId,
    @required String title,
    @required List<String> imageUrls,
    @required String videoUrl,
    @required String ttsId,
    @required String text,
    @required String youtubeUrl,
    @required FeedContentType contentType,
    @required int tier
  }){
    _categoryId = categoryId ?? 0;
    _removedPictures = removedPictures ?? [];
    _pictures = pictures ?? [];
    _contentId = contentId ?? 0;
    _title = title ?? "";
    _imageUrls = imageUrls ?? [];
    _videoUrl = videoUrl ?? "";
    _ttsId = ttsId ?? "";
    _text = text ?? "";
    _youtubeUrl = youtubeUrl ?? "";
    _contentType = contentType ?? FeedContentType.none;
    _tier = tier ?? 0;
  }

  int get categoryId => _categoryId;
  set categoryId(int value) {
    _categoryId = value;
    notifyListeners();
  }

  List<String> get removedPictures => _removedPictures;
  set removedPictures(List<String> value) {
    _removedPictures = value;
    notifyListeners();
  }

  List<String> get pictures => _pictures;
  set pictures(List<String> value) {
    _pictures = value;
    notifyListeners();
  }

  int get contentId => _contentId;
  set contentId(int value) {
    _contentId = value;
    notifyListeners();
  }

  String get title => _title;
  set title(String title){
    _title = title;
    notifyListeners();
  }

  List<String> get imageUrls => _imageUrls;
  set imageUrls(List<String> value) {
    _imageUrls = value;
    notifyListeners();
  }

  String get videoUrl => _videoUrl;
  set videoUrl(String value) {
    _videoUrl = value;
    notifyListeners();
  }

  String get youtubeUrl => _youtubeUrl;
  set youtubeUrl(String value) {
    _youtubeUrl = value;
    notifyListeners();
  }

  String get ttsId => _ttsId;
  set ttsId(String value) {
    _ttsId = value;
    notifyListeners();
  }

  String get text => _text;
  set text(String value) {
    _text = value;
    notifyListeners();
  }

  FeedContentType get contentType => _contentType;
  set contentType(FeedContentType contentType){
    _contentType = contentType;
    notifyListeners();
  }

  int get tier => _tier;
  set tier(int tier){
    _tier = tier;
    notifyListeners();
  }

  bool get isUploadingVideo => _isUploadingVideo;
  set isUploadingVideo(bool isUploadingVideo){
    _isUploadingVideo = isUploadingVideo;
    notifyListeners();
  }

  void removeSetContent(){
    _imageUrls = [];
    _videoUrl = "";
    _youtubeUrl = "";
    _contentType = FeedContentType.none;
    _tier = 0;
    notifyListeners();
  }
}


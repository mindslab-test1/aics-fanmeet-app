
import 'package:flutter/material.dart';

class CommentReplyProvider extends ChangeNotifier{
  int _selectedCommentId;
  int _selectedCommentParentId;
  bool _isSelected = false;

  int get selectedCommentId => _selectedCommentId;
  set selectedCommentId(int value) {
    _selectedCommentId = value;
    notifyListeners();
  }

  int get selectedCommentParentId => _selectedCommentParentId;
  set selectedCommentParentId(int value) {
    _selectedCommentParentId = value;
    notifyListeners();
  }

  void setReplyTarget(int commentId, int parentId){
    _selectedCommentId = commentId;
    _selectedCommentParentId = parentId;
    _isSelected = true;
    notifyListeners();
  }

  void reset(){
    _selectedCommentId = null;
    _selectedCommentParentId = null;
    _isSelected = false;
    notifyListeners();
  }

  @override
  String toString() {
    return 'CommentReplyProvider{_selectedCommentId: $_selectedCommentId, _selectedCommentParentId: $_selectedCommentParentId, _isSelected: $_isSelected}';
  }
}
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_accept_view.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_requested_view.dart';

class CelebTtsProvider extends ChangeNotifier{

  List<RequestedCelebTtsData> _requested = [];
  List<AcceptedCelebTtsData> _accepted = [];

  List<RequestedCelebTtsData> get requested => _requested;
  List<AcceptedCelebTtsData> get accepted => _accepted;


  set requested(List<RequestedCelebTtsData> value){
    _requested = value;
    notifyListeners();
  }

  set accepted(List<AcceptedCelebTtsData> value){
    _accepted = value;
    notifyListeners();
  }

  void popRequested(int ttsId, {bool toAccepted = false}){
    RequestedCelebTtsData popObject = _requested.firstWhere((element) => element.tts == ttsId);
    _requested.removeWhere((element) => element.tts == ttsId);
    if(toAccepted) _accepted.add(AcceptedCelebTtsData(
      tts: popObject.tts,
      subscriberName: popObject.subscriberName,
      text: popObject.text,
      timestamp: popObject.timestamp,
      ttsUrl: popObject.ttsUrl
    ));
    notifyListeners();
  }

  void popAccepted(int ttsId){
    _accepted.removeWhere((element) => element.tts == ttsId);
  }

  void setDisplayList(List<RequestedCelebTtsData> requestedList, List<AcceptedCelebTtsData> acceptedList){
    _requested = requestedList;
    _accepted = acceptedList;
    notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_view_components.dart';

class CommentDataProvider extends ChangeNotifier{
  List<CommentData> _commentDataList = [];

  set commentDataList(List<CommentData> value) {
    _commentDataList = value;
    notifyListeners();
  }

  List<CommentData> get commentDataList => _commentDataList;

  /* can be used later */
  // void setCommentDataWithText(String text, int commentId){
  //   commentDataList.where((comment) => comment.commentId==commentId).forEach((element) {
  //     element = CommentData(
  //         userId : element.userId,
  //         userName : element.userName,
  //         profileImg : element.profileImg,
  //         badgeUrl : element.badgeUrl,
  //         likeCount : element.likeCount,
  //         isLike : element.isLike,
  //         text : text,
  //         timeStamp : element.timeStamp,
  //         commentId : element.commentId,
  //         replies : element.replies,
  //         type : element.type,
  //         isEdited : element.isEdited
  //     );
  //   });
  //   notifyListeners();
  // }
}
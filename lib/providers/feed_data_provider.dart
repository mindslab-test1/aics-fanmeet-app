
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';

class FeedDataProvider extends ChangeNotifier{
  FeedData _feedData = FeedData();
  bool _isUserFeed = true;
  bool _isContent = false;

  bool get isUserFeed => _isUserFeed;
  bool get isContent => _isContent;
  FeedData get feedData => _feedData;

  set isUserFeed(bool value){
    _isUserFeed = value;
    notifyListeners();
  }

  set isContent(bool isContent) {
    _isContent = isContent;
    notifyListeners();
  }

  set feedData(FeedData value) {
    _feedData = value;
    notifyListeners();
  }
}
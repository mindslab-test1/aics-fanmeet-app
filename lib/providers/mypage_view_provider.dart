
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/component/scrap_board_component.dart';
import 'package:rf_tap_fanseem/view/setting_membership/membership_mange_route.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_sub_view.dart';

class MyPageViewProvider extends ChangeNotifier{
  String _name = "";
  String _profileImgUrl;
  String _bannerImgUrl;
  List<CategoryEnum> _interestedCategoryList = [];

  List<CelebData> _celebList = [];
  List<ScrapBoardCardData> _boardData = [];

  List<CelebData> _celebViewData = [];
  List<ScrapBoardCardData> _boardViewData = [];

  List<RequestedTtsData> _requestedTtsList = [];

  List<MembershipData> _membershipList = [];

  String get name => _name;
  String get profileImgUrl => _profileImgUrl;
  String get bannerImgUrl => _bannerImgUrl;
  List<CategoryEnum> get interestedCategoryList => _interestedCategoryList;

  List<CelebData> get celebList => _celebList;
  List<ScrapBoardCardData> get boardData => _boardData;

  List<CelebData> get celebViewData => _celebViewData;
  List<ScrapBoardCardData> get boardViewData => _boardViewData;

  List<RequestedTtsData> get requestedTtsList => _requestedTtsList;

  List<MembershipData> get membershipList => _membershipList;

  set celebViewData(List<CelebData> value) {
    _celebViewData = value;
    notifyListeners();
  }

  set boardViewData(List<ScrapBoardCardData> value) {
    _boardViewData = value;
    notifyListeners();
  }

  set requestedTtsList(List<RequestedTtsData> value){
    _requestedTtsList = value;
    notifyListeners();
  }

  set interestedCategoryList(List<CategoryEnum> value) {
    _interestedCategoryList = value;
    notifyListeners();
  }

  set membershipList(List<MembershipData> value){
    _membershipList = value;
    notifyListeners();
  }

  void setMyPageInfo({
    String name,
    String profileImgUrl,
    String bannerImgUrl,
    List<CelebData> celebList,
    List<ScrapBoardCardData> scrapBoardData
  }){
    _name = name;
    _profileImgUrl = profileImgUrl;
    _bannerImgUrl = bannerImgUrl;
    if(celebList != null)
      _celebList = celebList;
    if(boardData != null)
      _boardData = scrapBoardData;
    notifyListeners();
  }
}
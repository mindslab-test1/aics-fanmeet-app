import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_item_vo.dart';


class MembershipProvider extends ChangeNotifier{
  String _membershipId = "";
  String _membershipName = "__default__";
  int _membershipCost = 0;
  List<HttpCommunityCelebMembershipListItemVo> _membershipList = [];

  String get membershipId => _membershipId;
  List<HttpCommunityCelebMembershipListItemVo> get membershipList => _membershipList;
  String get membershipName => _membershipName;
  int get membershipCost => _membershipCost;

  set membershipList(List<HttpCommunityCelebMembershipListItemVo> list){
    _membershipList = list;
    notifyListeners();
  }

  void setMembershipInfo(String membershipId, String membershipName, int membershipCost, ){
    _membershipId = membershipId;
    _membershipName = membershipName;
    _membershipCost = membershipCost;
    notifyListeners();
  }
}

import 'package:flutter/material.dart';

class ReportManageProvider extends ChangeNotifier {
  int _userId = -1;
  String _username = "";
  String _profileImgUrl;
  int _reportCount = -1;

  int get userId => _userId;
  String get username => _username;
  String get profileImgUrl => _profileImgUrl;
  int get reportCount => _reportCount;

  void setReportUser({
    int userId,
    String username,
    String profileImgUrl,
    int reportCount,
  }){
    _userId = userId;
    _username = username;
    _profileImgUrl = profileImgUrl;
    _reportCount = reportCount;
    notifyListeners();
  }
}
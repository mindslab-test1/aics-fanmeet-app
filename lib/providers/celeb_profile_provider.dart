
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/profile/dto/http_profile_view_dto.dart';

class CelebProfileProvider extends ChangeNotifier{
  int _celebId = 0;
  String _celebName = "";
  HttpProfileViewDto _celeb;

  HttpProfileViewDto get celeb => _celeb;
  set celeb(HttpProfileViewDto value) {
    _celeb = value;
  }

  int get celebId => _celebId;
  String get celebName => _celebName;

  void setCelebProfile({int celebId, String celebName}){
    _celebId = celebId;
    _celebName = celebName;
    notifyListeners();
  }

  void setSubscribing(){
    celeb = HttpProfileViewDto(
        isCeleb: this.celeb.isCeleb,
        isSubscribing: !this.celeb.isSubscribing,
        name: this.celeb.name,
        greeting: '',
        introductions: this.celeb.introductions,
        subscriberCount: this.celeb.subscriberCount,
        feedCount: this.celeb.feedCount,
        interests: this.celeb.interests,
        instagramUrl: this.celeb.instagramUrl,
        facebookUrl: this.celeb.facebookUrl,
        lineUrl: this.celeb.lineUrl,
        twitterUrl: this.celeb.twitterUrl,
        profileImageUrl: this.celeb.profileImageUrl,
        bannerImageUrl: this.celeb.bannerImageUrl
    );
  }
}
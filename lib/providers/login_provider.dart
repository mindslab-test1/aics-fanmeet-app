
import 'package:flutter/material.dart';

class LoginVerifyProvider extends ChangeNotifier{
  bool _isLogin = false;
  String _social = "";
  String _token = "";
  bool _isCeleb = false;
  int _userId;

  bool get isCeleb => _isCeleb;
  set isCeleb(bool value) {
    _isCeleb = value;
    notifyListeners();
  }

  bool get isLogin => _isLogin;
  set isLogin(bool value) {
    _isLogin = value;
    notifyListeners();
  }

  String get social => _social;
  set social(String value) {
    _social = value;
    notifyListeners();
  }

  String get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }

  void setLoginInfo(bool isLogin, String social, String token, bool isCeleb, int userId){
    _isLogin = isLogin;
    _social = social;
    _token = token;
    _isCeleb = isCeleb;
    _userId = userId;
    notifyListeners();
  }

  int get userId => _userId;
  set userId(int value) {
    _userId = value;
    notifyListeners();
  }

}
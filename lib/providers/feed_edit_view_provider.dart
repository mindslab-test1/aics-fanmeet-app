
import 'package:flutter/material.dart';

class FeedEditViewProvider extends ChangeNotifier{
  FeedEditType _editType = FeedEditType.general;
  String _title = "";

  FeedEditType get editType => _editType;
  String get title => _title;

  set editType(FeedEditType value){
    _editType = value;
    switch(value){
      case FeedEditType.general:
        _title = "피드 수정하기";
        break;
      case FeedEditType.celeb:
        _title = "피드 수정하기";
        break;
      case FeedEditType.contents:
        _title = "콘텐츠 수정하기";
        break;
      case FeedEditType.comments:
        _title = "댓글 수정하기";
    }
    notifyListeners();
  }
}

enum FeedEditType {
  general,
  celeb,
  contents,
  comments
}
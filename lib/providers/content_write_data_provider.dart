

import 'package:flutter/material.dart';

import 'feed_write_data_provider.dart';

class ContentWriteDataProvider extends ChangeNotifier{

  int _categoryId = 0;
  String _title = "";
  List<String> _imageUrls = [];
  String _videoUrl;
  String _youtubeUrl;
  String _ttsId;
  String _text = "";
  FeedContentType _contentType = FeedContentType.none;
  int _tier = 0;
  bool _isUploadingVideo = false;

  int get categoryId => _categoryId;
  set categoryId(int value) {
    _categoryId = value;
    notifyListeners();
  }

  String get title => _title;
  set title(String title){
    _title = title;
    notifyListeners();
  }

  List<String> get imageUrls => _imageUrls;
  set imageUrls(List<String> value) {
    _imageUrls = value;
    notifyListeners();
  }

  String get videoUrl => _videoUrl;
  set videoUrl(String value) {
    _videoUrl = value;
    notifyListeners();
  }

  String get youtubeUrl => _youtubeUrl;
  set youtubeUrl(String value) {
    _youtubeUrl = value;
    notifyListeners();
  }

  String get ttsId => _ttsId;
  set ttsId(String value) {
    _ttsId = value;
    notifyListeners();
  }

  String get text => _text;
  set text(String value) {
    _text = value;
    notifyListeners();
  }

  FeedContentType get contentType => _contentType;
  set contentType(FeedContentType contentType){
    _contentType = contentType;
    notifyListeners();
  }

  int get tier => _tier;
  set tier(int tier){
    _tier = tier;
    notifyListeners();
  }

  bool get isUploadingVideo => _isUploadingVideo;
  set isUploadingVideo(bool isUploadingVideo){
    _isUploadingVideo = isUploadingVideo;
    notifyListeners();
  }

  void removeSetContent(){
    _imageUrls = [];
    _videoUrl = null;
    _youtubeUrl = null;
    _contentType = FeedContentType.none;
    _tier = 0;
    notifyListeners();
  }
}
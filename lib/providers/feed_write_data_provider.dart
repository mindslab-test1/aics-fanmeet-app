
import 'package:flutter/material.dart';

class FeedWriteDataProvider extends ChangeNotifier{

  int _categoryId;
  List<String> _imageUrls = [];
  String _videoUrl;
  String _youtubeUrl;
  String _ttsId;
  String _text;
  FeedContentType _contentType = FeedContentType.none;
  int _accessLevel = 0;
  bool _isUploadingVideo = false;

  int get categoryId => _categoryId;
  set categoryId(int value) {
    _categoryId = value;
    notifyListeners();
  }

  List<String> get imageUrls => _imageUrls;
  set imageUrls(List<String> value) {
    _imageUrls = value;
    notifyListeners();
  }

  String get videoUrl => _videoUrl;
  set videoUrl(String value) {
    _videoUrl = value;
    notifyListeners();
  }

  String get youtubeUrl => _youtubeUrl;
  set youtubeUrl(String value) {
    _youtubeUrl = value;
    notifyListeners();
  }

  String get ttsId => _ttsId;
  set ttsId(String value) {
    _ttsId = value;
    notifyListeners();
  }

  String get text => _text;
  set text(String value) {
    _text = value;
    notifyListeners();
  }

  FeedContentType get contentType => _contentType;
  set contentType(FeedContentType contentType){
    _contentType = contentType;
    notifyListeners();
  }

  int get accessLevel => _accessLevel;
  set accessLevel(int accessLevel){
    _accessLevel = accessLevel;
    notifyListeners();
  }

  bool get isUploadingVideo => _isUploadingVideo;
  set isUploadingVideo(bool isUploadingVideo){
    _isUploadingVideo = isUploadingVideo;
    notifyListeners();
  }

  void removeSetContent(){
    _imageUrls = [];
    _videoUrl = null;
    _youtubeUrl = null;
    _contentType = FeedContentType.none;
    _accessLevel = 0;
    notifyListeners();
  }
}

enum FeedContentType {
  none,
  images,
  video,
  youtube
}
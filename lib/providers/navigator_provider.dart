
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'dart:io';

class MainNavigationProvider extends ChangeNotifier{
  String _title = "홈";
  bool _searchState = false;
  bool navButtonHit = false;
  BarLeftButton _leftButton = BarLeftButton.login;
//  BarRightButton _rightButton = BarRightButton.search;
  BarRightButton _rightButton = BarRightButton.none;
  VoidCallback _leftButtonFunction = () => print("no function");
  VoidCallback _rightButtonFunction = () => print("no function");
  PageController _pageController;
  ScrollPhysics _mainViewScrollPhysics = NeverScrollableScrollPhysics();
  Widget _floatingActionButton = Container(
    width: 0,
    height: 0,
  );

  int _currentIndex = 0;

  final _rootAppBarTitle = [
    "홈",
    "알림",
    // "기프트샵",
    "내 정보"
  ];

  get currentIndex => this._currentIndex;

  set currentIndex(int index){
    this._currentIndex = index;
    notifyListeners();
  }

  String get title => _title;

  set title(String value) {
    _title = value;
    notifyListeners();
  }

  bool get searchState => _searchState;

  set searchState(bool searchState){
    _searchState = searchState;
    notifyListeners();
  }

  BarLeftButton get leftButton => _leftButton;

  set leftButton(BarLeftButton value) {
    _leftButton = value;
    notifyListeners();
  }

  BarRightButton get rightButton => _rightButton;

  set rightButton(BarRightButton value) {
    _rightButton = value;
    notifyListeners();
  }

  VoidCallback get leftButtonFunction => _leftButtonFunction;
  VoidCallback get rightButtonFunction => _rightButtonFunction;

  PageController get pageController => _pageController;
  set pageController(PageController pageController){
    _pageController = pageController;
//    notifyListeners();
  }

  ScrollPhysics get mainViewScrollPhysics => _mainViewScrollPhysics;
  set mainViewScrollPhysics(ScrollPhysics physics){
    this._mainViewScrollPhysics = physics;
    notifyListeners();
  }

  set rightButtonFunction(VoidCallback function){
    _rightButtonFunction = function;
    notifyListeners();
  }

  set leftButtonFunction(VoidCallback function){
    _leftButtonFunction = function;
    notifyListeners();
  }

  Widget get floatingActionButton => _floatingActionButton;

  set floatingActionButton(Widget floatingActionButton){
    _floatingActionButton = floatingActionButton;
    notifyListeners();
  }

  void navigateRoot(int index, BuildContext context){
    _currentIndex = index;
    // if(Platform.isIOS&&index==2) // TODO: delete when IOS develop finishes
    //   index=3;
    _title = _rootAppBarTitle[index];
    switch(index){
      case 0:
        _leftButton = BarLeftButton.login;
//        _rightButton = BarRightButton.search;
        _rightButton = BarRightButton.none;
        _leftButtonFunction = navigateToLogin(context);
//        _rightButtonFunction = () => _searchState = true;
        _rightButtonFunction = dummyFunction;
        break;
      case 1:
        _leftButton = BarLeftButton.back;
        _rightButton = BarRightButton.none;
        _leftButtonFunction = () {
          this.navigateRoot(0, context);
//          this.navButtonHit = true;
//          _pageController.animateToPage(
//              0,
//              duration: Duration(milliseconds: 500),
//              curve: Curves.easeOut).then((value) => navButtonHit = false);
          this._pageController.jumpToPage(0);
        };
        _rightButtonFunction = dummyFunction();
        break;
//       case 2:
//         _leftButton = BarLeftButton.back;
//         _rightButton = BarRightButton.none;
//         _leftButtonFunction = () {
//           this.navigateRoot(0, context);
// //          this.navButtonHit = true;
// //          _pageController.animateToPage(
// //              0,
// //              duration: Duration(milliseconds: 500),
// //              curve: Curves.easeOut).then((value) => navButtonHit = false);
//           this._pageController.jumpToPage(0);
//         };
//         _rightButtonFunction = dummyFunction();
//         break;
      case 2:
        _leftButton = BarLeftButton.back;
        _rightButton = BarRightButton.settings;
        _leftButtonFunction = () {
          this.navigateRoot(0, context);
//          this.navButtonHit = true;
//          _pageController.animateToPage(
//              0,
//              duration: Duration(milliseconds: 500),
//              curve: Curves.easeOut).then((value) => navButtonHit = false);
          this._pageController.jumpToPage(0);
        };
        _rightButtonFunction = dummyFunction();
        break;
    }
    notifyListeners();
  }

  void setButtons({
    BarLeftButton leftButton,
    BarRightButton rightButton,
    VoidCallback leftFunction,
    VoidCallback rightFunction
  }){
    _leftButton = leftButton;
    _rightButton = rightButton;
    if(leftFunction != null)
      _leftButtonFunction = leftFunction;
    if(rightFunction != null)
      _rightButtonFunction = rightFunction;
    notifyListeners();
  }

  void setAll({
    String title,
    BarLeftButton leftButton,
    BarRightButton rightButton,
    VoidCallback leftFunction,
    VoidCallback rightFunction,
    ScrollPhysics scrollPhysics = const NeverScrollableScrollPhysics(),
    Widget floatingActionButton,
  }){
    this._title = title;
    this._mainViewScrollPhysics = scrollPhysics;
    this._floatingActionButton = floatingActionButton == null ? Container(
      width: 0,
      height: 0,
    ) : floatingActionButton;
    setButtons(leftButton: leftButton, rightButton: rightButton, leftFunction: leftFunction, rightFunction: rightFunction);
  }
}

enum BarLeftButton{
  none,
  back,
  crossOut,
  login
}

enum BarRightButton{
  none,
  details,
  search,
  settings,
  edit,
  reset,
  done
}


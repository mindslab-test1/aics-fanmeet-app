import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';

class TtsWriteProvider extends ChangeNotifier{
  int _ttsId;

  int get ttsId => _ttsId;

  set ttsId(int value) {
    _ttsId = value;
    notifyListeners();
  }

  int _monthlyUsed = 0;
  int _monthlyLimit = 0;
  int _requestLimit = 300;

  List<CelebData> _ttsCelebList = [];
  CelebData _selectedCeleb;


  int get monthlyUsed => _monthlyUsed;
  int get monthlyLimit => _monthlyLimit;
  int get requestLimit => _requestLimit;

  List<CelebData> get ttsCelebList => _ttsCelebList;
  set ttsCelebList(List<CelebData> value) {
    _ttsCelebList = value;
    notifyListeners();
  }

  CelebData get selectedCeleb => _selectedCeleb;
  set selectedCeleb(CelebData value) {
    _selectedCeleb = value;
    notifyListeners();
  }

  void setLimits({
    int monthlyUsed = 0,
    int monthlyLimit = 0,
    int requestLimit = 300
  }){
    this._monthlyUsed = monthlyUsed;
    this._monthlyLimit = monthlyLimit;
    this._requestLimit = requestLimit;
  }

  void reset(){
    this._ttsId = null;
    this._monthlyUsed = 0;
    this._monthlyLimit = 0;
    this._requestLimit = 0;
  }

  bool _fromCommunity = false;

  bool get fromCommunity => _fromCommunity;
  void setFromCommunity(){
    _fromCommunity = true;
    notifyListeners();
  }

  void resetTtsWrite(){
    _fromCommunity = false;
    notifyListeners();
  }
}
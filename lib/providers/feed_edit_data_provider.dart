
import 'package:flutter/material.dart';

class FeedEditDataProvider extends ChangeNotifier{

  int _categoryId;
  int _feedId;
  List<String> _imageUrls = [];
  List<String> _removedPictures = []; //TODO : 추후에 list<long>으로 몇번째 사진이 없어질지 결정
  List<String> _pictures=[];
  String _videoUrl;
  String _youtubeUrl;
  String _ttsId;
  String _text;
  FeedContentType _contentType = FeedContentType.none;
  int _accessLevel = 0;
  bool _isUploadingVideo = false;

  void setFeedEditData({
    @required int categoryId,
    @required int feedId,
    @required List<String> imageUrls,
    @required List<String> removedPictures,
    @required List<String> pictures,
    @required String videoUrl,
    @required String youtubeUrl,
    @required String ttsId,
    @required String text,
    @required FeedContentType contentType,
    @required int accessLevel
  }){
    _categoryId = categoryId ?? 0;
    _removedPictures = removedPictures ?? [];
    _pictures = pictures ?? [];
    _feedId = feedId ?? 0;
    _imageUrls = imageUrls ?? [];
    _videoUrl = videoUrl ?? "";
    _ttsId = ttsId ?? "";
    _text = text ?? "";
    _youtubeUrl = youtubeUrl ?? "";
    _contentType = contentType ?? FeedContentType.none;
    _accessLevel = accessLevel ?? 0;
  }
  void setContentTypeWithMedia(List<String> pictures){
    if(pictures.isNotEmpty||this.imageUrls.isNotEmpty){
      this.contentType = FeedContentType.images;
    }
    else if(this.videoUrl!=""){
      this.contentType = FeedContentType.video;
    }
    else if(this.youtubeUrl!=""){
      this.contentType = FeedContentType.youtube;
    }
    else{
      this.contentType = FeedContentType.none;
    }
  }

  int get categoryId => _categoryId;
  set categoryId(int value) {
    _categoryId = value;
    notifyListeners();
  }

  int get feedId => _feedId;
  set feedId(int value) {
    _feedId = value;
    notifyListeners();
  }

  List<String> get removedPictures => _removedPictures;
  set removedPictures(List<String> value) {
    _removedPictures = value;
    notifyListeners();
  }

  List<String> get pictures => _pictures;
  set pictures(List<String> value) {
    _pictures = value;
    notifyListeners();
  }

  List<String> get imageUrls => _imageUrls;
  set imageUrls(List<String> value) {
    _imageUrls = value;
    notifyListeners();
  }

  String get videoUrl => _videoUrl;
  set videoUrl(String value) {
    _videoUrl = value;
    notifyListeners();
  }

  String get youtubeUrl => _youtubeUrl;
  set youtubeUrl(String value) {
    _youtubeUrl = value;
    notifyListeners();
  }

  String get ttsId => _ttsId;
  set ttsId(String value) {
    _ttsId = value;
    notifyListeners();
  }

  String get text => _text;
  set text(String value) {
    _text = value;
    notifyListeners();
  }

  FeedContentType get contentType => _contentType;
  set contentType(FeedContentType contentType){
    _contentType = contentType;
    notifyListeners();
  }

  int get accessLevel => _accessLevel;
  set accessLevel(int accessLevel){
    _accessLevel = accessLevel;
    notifyListeners();
  }

  bool get isUploadingVideo => _isUploadingVideo;
  set isUploadingVideo(bool isUploadingVideo){
    _isUploadingVideo = isUploadingVideo;
    notifyListeners();
  }

  void removeSetContent(){
    _imageUrls = [];
    _videoUrl = "";
    _youtubeUrl = "";
    _contentType = FeedContentType.none;
    _accessLevel = 0;
    notifyListeners();
  }
}

enum FeedContentType {
  none,
  images,
  video,
  youtube
}
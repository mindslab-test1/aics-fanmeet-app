import '../rest_call_functions.dart';
import 'dto/http_login_response_dto.dart';

Future<HttpLoginResponseDto> httpAppleLogin({String deviceTokenId, String accessToken, String authorizationCode}) async {
  assert(deviceTokenId != null && accessToken != null && authorizationCode != null);
  Map<String, dynamic> body = {
    "deviceId": deviceTokenId,
    "authorizationCode": authorizationCode,
    "accessToken" : accessToken
  };
  Map<String, dynamic> response = await postAsGuest("/auth/login/apple", body);
  if (response == null) return null;
  if (response['status']==401)
    return HttpLoginResponseDto(
        status: response['status']
    );
  return HttpLoginResponseDto(
      status: response['status'],
      userId: response['data']['userId'],
      isCeleb: response['data']['isCeleb'],
      iosRefreshToken: response['data']['iosRefreshToken']
  );
}
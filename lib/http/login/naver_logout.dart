import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpNaverLogout({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/auth/logout", null);
//  if (response == null) return null;
  return response['status'] == 200;
}
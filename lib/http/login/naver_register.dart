import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<int> httpNaverRegister({String accessToken, String name, String introduction, String profileImagePath, String coverImagePath, List<int> interests}) async {
  assert(accessToken != null && name != null);
  Map<String, dynamic> body = {
    "accessToken": accessToken,
    "name": name,
    "hello": introduction,
    "interests": interests
  };
  List<MultipartFile> files = [];
  if (profileImagePath != null) {
    files.add(await MultipartFile.fromPath("profileImage", profileImagePath, filename: profileImagePath.split("/").last));
  }
  if (coverImagePath != null) {
    files.add(await MultipartFile.fromPath("bannerImage", coverImagePath, filename: coverImagePath.split("/").last));
  }

  Map<String, dynamic> response = await multipartPostAsGuest("/auth/register/naver", body, files);
  if (response == null) return null;
  return response['status'];
}
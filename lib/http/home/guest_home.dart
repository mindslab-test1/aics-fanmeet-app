
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_home_response_dto.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_recent_activity_vo.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

Future<HttpHomeResponseDto> httpGuestHome({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await getAsGuest("/home/guest");
  if (response == null || response['status'] != 200) return null;

  List<HttpRecentActivityVo> recentActivityList = List();
  for (Map<String, dynamic> iter in List.from(response['data']['recentActivityList'])) {
    recentActivityList.add(HttpRecentActivityVo(
        timestamp: formattedTime(iter['timestamp']),
        userId: iter['userId'],
        name: iter['name'],
        profileImageUrl: iter['profileImageUrl'],
        elementType: iter['elementType'],
        elementId: iter['elementId'],
        elementText: iter['elementText'],
        elementImageUrl: iter['elementImageUrl']
    ));
  }

  return HttpHomeResponseDto(
      subscribingCelebList: [],
      recentActivityList: recentActivityList
  );
}
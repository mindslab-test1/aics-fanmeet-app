
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_celeb_info_vo.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_home_celeb_list_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpHomeCelebListDto> httpHomeCelebList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/home/celebs", null);
  if (response == null || response['status'] != 200) return null;

  List<HttpCelebInfoVo> allCelebList = List();
  for (Map<String, dynamic> iter in List.from(response['data']['allCelebList'])) {
    allCelebList.add(HttpCelebInfoVo(
      id: iter['id'],
      profileImageUrl: iter['profileImageUrl'],
      name: iter['name'],
      badgeUrl: iter["badgeUrl"],
    ));
  }
  List<HttpCelebInfoVo> subscribingCelebList = List();
  for (Map<String, dynamic> iter in List.from(response['data']['subscribingCelebList'])) {
    subscribingCelebList.add(HttpCelebInfoVo(
      id: iter['id'],
      profileImageUrl: iter['profileImageUrl'],
      name: iter['name'],
      badgeUrl: iter["badgeUrl"],
    ));
  }

  return HttpHomeCelebListDto(
    allCelebList: allCelebList,
    subscribingCelebList: subscribingCelebList
  );
}
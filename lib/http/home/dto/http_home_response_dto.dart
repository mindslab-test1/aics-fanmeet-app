
import 'package:rf_tap_fanseem/http/home/dto/http_recent_activity_vo.dart';
import 'package:rf_tap_fanseem/http/home/dto/http_celeb_info_vo.dart';

class HttpHomeResponseDto {
  final List<HttpCelebInfoVo> subscribingCelebList;
  final List<HttpRecentActivityVo> recentActivityList;

  HttpHomeResponseDto({
    this.subscribingCelebList,
    this.recentActivityList
  });

  @override
  String toString() {
    return 'HttpHomeResponseDto{subscribingCelebList: $subscribingCelebList, recentActivityList: $recentActivityList}';
  }
}

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebFeedWriteComment({
  BuildContext context,
  int celebId,
  int feedId,
  int parentId,
  String text,
  String imageUrl,
  int ttsId
}) async {
  assert(context != null && celebId != null && feedId != null && text != null);

  List<MultipartFile> image;
  if (imageUrl != null) image = [await MultipartFile.fromPath("pictures", imageUrl, filename: imageUrl.split("/").last)];

  Map<String, dynamic> body = {
    "celeb": celebId,
    "celebFeed": feedId,
    "text": text,
  };
  if (parentId != null) body['parent'] = parentId;
  if (image != null) body['picture'] = image;
  if (ttsId != null) body['tts'] = ttsId;

  Map<String, dynamic> response = await multipartPostAsUser(context, "/community/celebfeed/comment/write", body, image);
  if (response == null || response['status'] != 200) return false;

  return true;
}
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebFeedEditComment({BuildContext context, int comment, String text, int tts}) async {
  assert(context != null && comment != null && text != null);
  Map<String, dynamic> body = {
    "comment": comment,
    "text": text,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/comment/edit", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}
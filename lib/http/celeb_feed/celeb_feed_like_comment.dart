
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_view_response_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebFeedLikeComment({BuildContext context, int commentId}) async {
  assert(context != null && commentId != null);
  Map<String, dynamic> body = {
    "comment": commentId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/comment/like", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}
import 'package:flutter/material.dart';

import '../rest_call_functions.dart';

Future<bool> httpCelebFeedDeleteComment({BuildContext context, int comment}) async {
  assert(context != null && comment != null);
  Map<String, dynamic> body = {
    "comment": comment,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/comment/delete", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}
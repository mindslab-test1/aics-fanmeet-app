
class HttpCommunityCelebMembershipListItemVo {
  final int membershipId;
  final String membershipName;
  final int membershipTier;
  final String productId;
  final int price;
  final String imageUrl;

  HttpCommunityCelebMembershipListItemVo({
    this.membershipId, this.membershipName, this.membershipTier, this.productId, this.price, this.imageUrl,
  }) : assert(membershipId != null && membershipName != null && membershipTier != null && productId != null && price != null);
}

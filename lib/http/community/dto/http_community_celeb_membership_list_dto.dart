
import 'http_community_celeb_membership_list_item_vo.dart';

class HttpCommunityCelebMembershipListDto {
  final int myTier;
  final List<HttpCommunityCelebMembershipListItemVo> membershipList;

  HttpCommunityCelebMembershipListDto({
    this.myTier,
    this.membershipList
  });
}

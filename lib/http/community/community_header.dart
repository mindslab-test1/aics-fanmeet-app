
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:provider/provider.dart';

Future<HttpCommunityHeaderDto> httpCommunityHeader({int celebId}) async {
  assert(celebId != null);
  Map<String, dynamic> body = {
    "celeb": celebId
  };
  Map<String, dynamic> response = await postAsGuest("/community/header", body);
  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];

  return HttpCommunityHeaderDto(
      feedCount: data['feedCount'],
      subscriberCount: data['subscriberCount'],
      celebId: data['celeb'],
      name: data['celebName'],
      profileImageUrl: data['celebProfileImage'],
      introduction: data['celebHello']
  );
}
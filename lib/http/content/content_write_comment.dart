
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpContentWriteComment({
  BuildContext context,
  int celebId,
  int contentId,
  int parentId,
  String text,
  String imageUrl,
  int ttsId
}) async {
  assert(context != null && celebId != null && contentId != null && text != null);

  List<MultipartFile> image;
  if (imageUrl != null) image = [await MultipartFile.fromPath("pictures", imageUrl, filename: imageUrl.split("/").last)];

  Map<String, dynamic> body = {
    "celeb": celebId,
    "content": contentId,
    "text": text,
  };
  if (parentId != null) body['parent'] = parentId;
  if (image != null) body['picture'] = image;
  if (ttsId != null) body['tts'] = ttsId;

  Map<String, dynamic> response = await multipartPostAsUser(context, "/community/content/comment/write", body, image);
  if (response == null || response['status'] != 200) return false;

  return true;
}

import 'package:flutter/cupertino.dart';

import '../rest_call_functions.dart';

Future<bool> httpContentDelete({BuildContext context, int content}) async {
  assert(context != null && content != null);
  Map<String, dynamic> body = {
    "content": content,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/content/delete", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_view_response_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

Future<HttpFeedViewResponseDto> httpContentView({BuildContext context, int contentId}) async {
  assert(context != null && contentId != null);
  Map<String, dynamic> body = {
    "content": contentId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/content/view", body);

  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];
  Map<String, dynamic> feed = data["feedItem"];
  HttpFeedItemVo feedItem = HttpFeedItemVo(
      feedId: feed['content'],
      celebId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId,
      title: feed['title'],
      text: feed['text'] == null ? "" : feed['text'],
      feedOwnerId: feed['feedOwner'],
      feedOwnerName: feed['feedOwnerName'],
      feedOwnerProfileImageUrl: feed['feedOwnerProfileImage'],
      commentCount: feed['commentCount'],
      likeCount: feed['likeCount'],
      scrapCount: feed['scrapCount'],
      categoryId: feed['category'],
      categoryName: feed['categoryName'],
      pictureUrls: List.from(feed['pictures'] == null ? [] : feed['pictures']),
      videoUrl: feed['video'],
      videoThumbnailUrl: feed['thumbnail'],
      youtubeUrl: feed["youtube"],
      updatedTime: formattedTime(feed['updated']),
      liked: feed['liked'],
      scraped: feed['scraped'],
      blocked: feed['blocked'],
      restricted: false,
      badgeImageUrl: feed['accessLevelBadgeImageUrl'],
      accessLevel: feed['accessLevel']
  );

  List<HttpCommentItemVo> comments = List();
  for (Map<String, dynamic> iter in List.from(data['comments'])) {
    List<HttpCommentItemVo> replies = [];
    for(Map<String, dynamic> repIter in List.from(iter["offspring"])){
      replies.add(HttpCommentItemVo()
        ..commentId=repIter['id']
        ..text=repIter['text']
        ..commentOwnerId=repIter['commentOwner']
        ..commentOwnerName=repIter['commentOwnerName']
        ..commentOwnerProfileImageUrl=repIter['commentOwnerProfileImage']
        ..badgeUrl=repIter["badgeUrl"]
        ..likeCount=repIter['likeCount']
        ..blocked=repIter['blocked']
        ..deleted=repIter['deleted']
        ..liked=repIter['liked']
        ..pictureUrl=repIter['picture']
        ..ttsUrl=repIter['tts']
        ..updatedTime=formattedTime(repIter['updated'])
        ..parentId=repIter['parent']
        ..replies=[]
        ..isEdited=repIter['modifyYn']==1);
    }

    comments.add(HttpCommentItemVo()
      ..commentId=iter['id']
      ..text=iter['text']
      ..commentOwnerId=iter['commentOwner']
      ..commentOwnerName=iter['commentOwnerName']
      ..commentOwnerProfileImageUrl=iter['commentOwnerProfileImage']
      ..badgeUrl=iter["badgeUrl"]
      ..likeCount=iter['likeCount']
      ..blocked=iter['blocked']
      ..deleted=iter['deleted']
      ..liked=iter['liked']
      ..pictureUrl=iter['picture']
      ..ttsUrl=iter['tts']
      ..updatedTime=formattedTime(iter['updated'])
      ..parentId=iter['parent']
      ..replies=replies
      ..isEdited=iter['modifyYn']==1
    );
  }


  return HttpFeedViewResponseDto(
      feedItem: feedItem,
      comments: comments
  );
}
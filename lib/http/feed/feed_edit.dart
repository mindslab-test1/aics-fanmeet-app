
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

bool isLoadedFile(String path){
  if(path.startsWith("http"))
    return true;
  else
    return false;
}

List<int> getRemovedPicturesIndex(List<String> removedPictures, List<String> pictures){
  List<int> removedPicturesIndex = [];

  if(removedPictures.isEmpty) return removedPicturesIndex;
  for (int i=0; i<pictures.length; i++) {
    if (removedPictures.contains(pictures[i]))
      removedPicturesIndex.add(i);
  }
  return removedPicturesIndex;
}

Future<bool> httpFeedEdit({
  BuildContext context,
  int feedId,
  int celebId,
  int categoryId,
  List<String> removedPictures,
  List<String> pictures,
  List<String> addedPictures,
  String videoUrl,
  String videoThumbnailUrl,
  String youtubeUrl,
  int ttsId,
  String text
}) async {
  assert(context != null && celebId != null && categoryId != null && feedId != null);
  List<MultipartFile> fileList = [];
  List<int> removedPicturesIndex = [];

  removedPicturesIndex = getRemovedPicturesIndex(removedPictures, pictures);

  for (String path in addedPictures) {
    fileList.add(await MultipartFile.fromPath("addedPictures", path, filename: path.split("/").last));
  }
  if (videoUrl != "") {
    if(!isLoadedFile(videoUrl)) {
      fileList.add(await MultipartFile.fromPath("video", videoUrl, filename: videoUrl.split("/").last));
      fileList.add(MultipartFile.fromBytes("thumbnail", [1], filename: "dummy.png"));
    }
  }

  Map<String, dynamic> body;
  body = {
    "feed" : feedId,
    "category": categoryId,
    "text": text,
    if(removedPicturesIndex.isNotEmpty) "removedPictures" : removedPicturesIndex,
  };

  if (youtubeUrl != null) body['youtube'] = youtubeUrl;
  if (ttsId != null) body['tts'] = ttsId;
  Map<String, dynamic> response = await multipartPostAsUser(context, "/community/feed/edit", body, fileList);
  if (response == null || response['status'] != 200) return false;
  return true;
}

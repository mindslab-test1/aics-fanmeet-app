
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_list_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

Future<HttpFeedListDto> httpFeedList({BuildContext context, int celebId, int howManyFeed, String lastCreated, int categoryId}) async {
  assert(context != null && celebId != null && howManyFeed != null);
  Map<String, dynamic> body = {
    "celeb": celebId,
    "get": howManyFeed,
    "category": categoryId,
    "from": lastCreated
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/feed/list", body);
  if (response == null || response['status'] != 200) return null;

  Map<String, dynamic> data = response['data'];
  List<HttpFeedItemVo> feedItemList = List();
  for (Map<String, dynamic> iter in List.from(data['feeds'])) {
    feedItemList.add(HttpFeedItemVo(
        feedId: iter['feed'],
        celebId: iter['celeb'],
        text: iter['text'] == null ? "" : iter['text'],
        feedOwnerId: iter['feedOwner'],
        feedOwnerName: iter['feedOwnerName'],
        feedOwnerProfileImageUrl: iter['feedOwnerProfileImage'],
        commentCount: iter['commentCount'],
        likeCount: iter['likeCount'],
        scrapCount: iter['scrapCount'],
        categoryId: iter['category'],
        categoryName: iter['categoryName'],
        pictureUrls: List.from(iter['pictures'] == null ? [] : iter['pictures']),
        videoUrl: iter['video'],
        videoThumbnailUrl: iter['thumbnail'],
        youtubeUrl: iter["youtube"],
        updatedTime: formattedTime(iter['updated']),
        liked: iter['liked'],
        scraped: iter['scraped'],
        blocked: iter['blocked'],
        restricted: false,
        badgeImageUrl: iter['feedOwnerMembershipBadgeImageUrl'],
        isEdited: iter['modifyYn'] == 1
    ));
  }

  return HttpFeedListDto(
      tailCreated: data['tailCreated'],
      feeds: feedItemList
  );
}
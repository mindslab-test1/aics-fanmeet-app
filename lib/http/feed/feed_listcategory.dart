
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_category_item_dto.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_list_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<List<HttpCategoryItemDto>> httpFeedListcategory() async {
  Map<String, dynamic> response = await postAsGuest("/community/feed/listcategory", null);
  if (response == null || response['status'] != 200) return null;

  Map<String, dynamic> data = response['data'];
  List<HttpCategoryItemDto> categoryList = List();
  for (Map<String, dynamic> iter in List.from(data['categories'])) {
    categoryList.add(HttpCategoryItemDto(
      id: iter['id'],
      name: iter['name']
    ));
  }

  return categoryList;
}

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpFeedWrite({
  BuildContext context,
  int celebId,
  int categoryId,
  List<String> imageUrls,
  String videoUrl,
  String videoThumbnailUrl,
  String youtubeUrl,
  int ttsId,
  String text
}) async {
  assert(context != null && celebId != null && categoryId != null);

  List<MultipartFile> fileList = [];
  for (String path in imageUrls) {
    fileList.add(await MultipartFile.fromPath("pictures", path, filename: path.split("/").last));
  }
  if (videoUrl != null) {
    fileList.add(await MultipartFile.fromPath("video", videoUrl, filename: videoUrl.split("/").last));
  }
  if (videoUrl != null) {
    fileList.add(MultipartFile.fromBytes("thumbnail", [1], filename: "dummy.png"));
//    fileList.add(await MultipartFile.fromPath("thumbnail", videoThumbnailUrl, filename: videoThumbnailUrl.split("/").last));
  }

  Map<String, dynamic> body = {
    "celeb": celebId,
    "category": categoryId,
//    "youtube": youtubeUrl,
//    "tts": ttsId,
    "text": text,
  };
  if (youtubeUrl != null) body['youtube'] = youtubeUrl;
  if (ttsId != null) body['tts'] = ttsId;
  
  Map<String, dynamic> response = await multipartPostAsUser(context, "/community/feed/write", body, fileList);
  if (response == null || response['status'] != 200) return false;

  return true;
}
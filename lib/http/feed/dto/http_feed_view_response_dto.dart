
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';

class HttpFeedViewResponseDto{
  final HttpFeedItemVo feedItem;
  final List<HttpCommentItemVo> comments;

  const HttpFeedViewResponseDto({
    this.feedItem,
    this.comments
  }) : assert (feedItem != null && comments != null);
}
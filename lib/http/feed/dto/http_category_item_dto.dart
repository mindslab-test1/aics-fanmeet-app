
class HttpCategoryItemDto {
  final int id;
  final String name;

  const HttpCategoryItemDto({
    this.id,
    this.name
  }) : assert(id != null && name != null);
}
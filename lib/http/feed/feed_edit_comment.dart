import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpFeedEditComment({BuildContext context, int comment, String text}) async {
  assert(context != null && comment != null && text != null);
  Map<String, dynamic> body = {
    "comment": comment,
    "text": text,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/feed/comment/edit", body);

  if (response == null || response['status'] != 200) return false;

  return true;
}
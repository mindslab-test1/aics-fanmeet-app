
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebSendMessage({BuildContext context, String text, bool isTts = false, int ttsId}) async {
  Map<String, dynamic> body = {
  };
  if(isTts) body["tts"] = ttsId;
  else body["text"] = text;

  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/write", body);
  return response["status"] == 200;
}
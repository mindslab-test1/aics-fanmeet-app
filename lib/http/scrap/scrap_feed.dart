
import 'package:flutter/cupertino.dart';

import '../rest_call_functions.dart';

Future<bool> httpFeedScrap({BuildContext context, int feedId, int boardId}) async {
  assert(context != null && feedId != null);
  Map<String, dynamic> body = {
    "feed": feedId,
    "scrapBoard": boardId,
//    "celeb": celebId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/feed/scrap", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}

Future<bool> httpCelebFeedScrap({BuildContext context, int feedId, int boardId}) async {
  assert(context != null && feedId != null);
  Map<String, dynamic> body = {
    "celebFeed": feedId,
    "scrapBoard": boardId,
//    "celeb": celebId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/scrap", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}

Future<bool> httpContentFeedScrap({BuildContext context, int feedId, int boardId}) async {
  assert(context != null && feedId != null);
  Map<String, dynamic> body = {
    "content": feedId,
    "scrapBoard": boardId,
  };

  Map<String, dynamic> response = await postAsUser(context, "/community/content/scrap", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}
import 'package:flutter/cupertino.dart';

import '../rest_call_functions.dart';

Future<bool> httpScrapAddBoard({String boardName, BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "boardName": boardName
  };
  Map<String, dynamic> response = await postAsUser(context, "/mypage/scrap/board/add", body);
  return response["status"] == 200;
}
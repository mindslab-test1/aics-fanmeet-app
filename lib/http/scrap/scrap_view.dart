
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import '../../providers/scrap_board_provider.dart';
import '../../view/main/component/feed_component.dart';
import '../rest_call_functions.dart';

Future<bool> httpScrapBoardView({int boardId, BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "boardId": boardId
  };
  
  Map<String, dynamic> response = await postAsUser(context, "/mypage/scrap/board/view", body);

  List<ScrapFeedData> dataList = [];

  for(dynamic data in response["data"]["scrapList"]){
    ScrapKind scrapKind;
    switch(data["scrapFeedKind"]){
      case "USER_FEED":
        scrapKind = ScrapKind.userFeed;
        break;
      case "CELEB_FEED":
        scrapKind = ScrapKind.celebFeed;
        break;
      case "CONTENT":
        scrapKind = ScrapKind.content;
        break;
      default:
        continue;
    }
    List<String> imgUrls = [];
    for(dynamic e in data["imgUrl"]){
      imgUrls.add(e.toString());
    }

    dataList.add(ScrapFeedData(
      userId: data["userId"],
      feedId: data["feedId"],
      title: data["title"],
      viewCount: data["viewCount"],
      userName: data["userName"],
      profileImg: data["profileImg"],
      postTimestamp: data["postTimeStamp"],
      category: data["category"],
      imgUrl: imgUrls,
      videoUrl: data["videoUrl"],
      text: data["text"],
      scrapCount: data["scrapCount"],
      isScrap: data["isScrap"],
      likeCount: data["likeCount"],
      isLike: data["isLike"],
      commentCount: data["commentCount"],
      canView: true,
      scrapKind: scrapKind
    ));
  }

  Provider.of<ScrapBoardProvider>(context, listen: false).boardFeedList = dataList;

  return response["status"] == 200;
}

enum ScrapKind{
  userFeed,
  celebFeed,
  content
}
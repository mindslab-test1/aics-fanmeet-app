
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpServiceNewMembership({BuildContext context, String purchaseToken, String productId}) async {
  assert(context != null &&  purchaseToken != null && productId != null);
  Map<String, dynamic> body = {
    "purchaseToken": purchaseToken,
    "productId": productId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/service/membership/new", body);
  if (response == null || response['status'] != 200) return null;
  return true;
}
Future<bool> httpServiceVerifyReceipt(BuildContext context, String receipt, int userId)async{
  assert(context != null &&  receipt != null && userId != null);
  Map<String, dynamic> body = {
    "receiptData": receipt,
    "userId": userId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/service/apple/membership/verifyReceipt", body);
  return response['status']==200;
}
Future<bool> httpServiceCompletePurchase(BuildContext context, String receipt, int userId)async{
  assert(context != null &&  receipt != null && userId != null);
  Map<String, dynamic> body = {
    "receiptData": receipt,
    "userId": userId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/service/apple/membership/completePurchase", body);
  return response['status']==200;
}
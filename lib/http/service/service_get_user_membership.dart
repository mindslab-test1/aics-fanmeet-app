
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';

Future<HttpServiceGetUserMembershipDto> httpServiceGetUserMembership({BuildContext context, int celebId}) async {
  assert(context != null && celebId != null);
  Map<String, dynamic> body = {
    "celeb": celebId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/service/membership/get/user", body);
  if (response == null || response['status'] != 200) return null;

  if (response['data'] == null) return null;
  Map<String, dynamic> data = response['data'];
  List<String> list = (data['benefitDescriptionList'] == null ? [] : List.from(data['benefitDescriptionList']));
  return HttpServiceGetUserMembershipDto(
    tier: data['tier'],
    name: data['name'],
    badgeImageUrl: data['badgeImageUrl'],
    badge: data['badge'],
    sticker: data['sticker'],
    voiceLetter: data['voiceLetter'],
    ttsWrite: data['ttsWrite'],
    ttsWriteLimit: data['ttsWriteLimit'],
    benefitDescriptionList: list
  );
}
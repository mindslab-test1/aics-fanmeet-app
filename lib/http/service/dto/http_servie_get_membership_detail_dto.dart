

class HttpServiceGetMembershipDetailDto {
  final int tier;
  final String name;
  final String badgeImageUrl;
  final bool badge;
  final bool sticker;
  final bool voiceLetter;
  final bool ttsWrite;
  final int ttsWriteLimit;
  final List<String> benefitDescriptionList;

  const HttpServiceGetMembershipDetailDto({
    this.tier,
    this.name,
    this.badgeImageUrl,
    this.badge,
    this.sticker,
    this.voiceLetter,
    this.ttsWrite,
    this.ttsWriteLimit,
    this.benefitDescriptionList
  });
}

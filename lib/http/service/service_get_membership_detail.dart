
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_membership_detail_dto.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';

Future<HttpServiceGetMembershipDetailDto> httpServiceGetMembershipDetail({BuildContext context, String productId}) async {
  assert(context != null && productId != null);
  Map<String, dynamic> body = {
    "productId": productId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/service/membership/get/detail", body);
  if (response == null || response['status'] != 200) return null;

  if (response['data'] == null) return null;
  Map<String, dynamic> data = response['data'];
  return HttpServiceGetMembershipDetailDto(
    tier: data['tier'],
    name: data['name'],
    badgeImageUrl: data['badgeImageUrl'],
    badge: data['badge'],
    sticker: data['sticker'],
    voiceLetter: data['voiceLetter'],
    ttsWrite: data['ttsWrite'],
    ttsWriteLimit: data['ttsWriteLimit'],
    benefitDescriptionList: List<String>.from(data['benefitDescriptionList'])
  );
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

import 'dto/http_celeb_my_page_tts_list_received_item_dto.dart';

Future<List<HttpCelebMyPageTtsListReceivedItemDto>> httpCelebMyPageTtsListReceived({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/tts/listreceived", null);
  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];

  List<HttpCelebMyPageTtsListReceivedItemDto> list = [];
  for (Map<String, dynamic> iter in List.from(data['receivedList'])) {
    list.add(HttpCelebMyPageTtsListReceivedItemDto(
      ttsId: iter["tts"],
      subscriberName: iter['subscriberName'],
      text: iter['text'],
      timestamp: iter['created'],
      ttsUrl: iter['ttsUrl']
    ));
  }

  return list;
}


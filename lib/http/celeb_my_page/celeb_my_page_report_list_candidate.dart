
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

import 'dto/http_celeb_my_page_report_list_candidate_item_dto.dart';

Future<List<HttpCelebMyPageReportListCandidateItemDto>> httpCelebMyReportListCandidate({BuildContext context, bool reverse}) async {
  assert(context != null && reverse != null);
  Map<String, dynamic> body = {
    "reverse": reverse,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/report/listcandidate", body);
  if (response == null || response['status'] != 200) return null;

  Map<String, dynamic> data = response['data'];

  List<HttpCelebMyPageReportListCandidateItemDto> list = [];
  for (Map<String, dynamic> iter in List.from(data['candidateList'])) {
    list.add(HttpCelebMyPageReportListCandidateItemDto(
        id: iter['id'],
        name: iter['name'],
        reportAccumulation: iter['reportAccumulation'],
        profileImageUrl: iter['profileImageUrl']
    ));
  }

  return list;
}
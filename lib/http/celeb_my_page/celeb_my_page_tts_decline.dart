
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebMyTtsDecline({BuildContext context, int ttsId}) async {
  assert(context != null && ttsId != null);
  Map<String, dynamic> body = {
    "tts": ttsId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/tts/decline", body);
  if (response == null || response['status'] != 200) return false;
  return true;
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebMyPageHeader({BuildContext context, String text}) async {
  assert(context != null && text != null);
  Map<String, dynamic> body = {
    "text": text,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/tts/generate", body);
  if (response == null || response['status'] != 200) return false;
  return true;
}
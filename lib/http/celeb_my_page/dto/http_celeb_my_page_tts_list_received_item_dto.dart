class HttpCelebMyPageTtsListReceivedItemDto {
  final int ttsId;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  HttpCelebMyPageTtsListReceivedItemDto({
    this.ttsId,
    this.subscriberName,
    this.text,
    this.timestamp,
    this.ttsUrl
  }) : assert(ttsId != null && subscriberName != null && text != null && timestamp != null && ttsUrl != null);
}

class HttpCelebMyPageHeaderDto {
  final String name;
  final int subscriberCount;
  final int feedCount;
  final String profileImageUrl;

  HttpCelebMyPageHeaderDto({
    this.name,
    this.subscriberCount,
    this.feedCount,
    this.profileImageUrl
  }) : assert(name != null && subscriberCount != null && feedCount != null);
}

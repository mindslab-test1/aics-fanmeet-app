
class HttpCelebMyPageTtsListApprovedItemDto {
  final int tts;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  HttpCelebMyPageTtsListApprovedItemDto({
    this.tts,
    this.subscriberName,
    this.text,
    this.timestamp,
    this.ttsUrl
  }) : assert(tts != null && subscriberName != null && text != null && timestamp != null && ttsUrl != null);
}

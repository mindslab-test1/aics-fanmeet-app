
class HttpCelebMyPageReportListBlockedItemDto {
  final String name;
  final int userId;
  final String profileImageUrl;

  HttpCelebMyPageReportListBlockedItemDto({
    this.name,
    this.userId,
    this.profileImageUrl
  }) : assert(name != null && userId != null && profileImageUrl != null);
}

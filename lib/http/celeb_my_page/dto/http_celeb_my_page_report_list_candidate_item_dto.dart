
class HttpCelebMyPageReportListCandidateItemDto {
  final int id;
  final String name;
  final int reportAccumulation;
  final String profileImageUrl;

  HttpCelebMyPageReportListCandidateItemDto({
    this.id,
    this.name,
    this.reportAccumulation,
    this.profileImageUrl
  }) : assert(id != null && name != null && reportAccumulation != null);
}

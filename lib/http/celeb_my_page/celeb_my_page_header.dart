
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/dto/http_celeb_my_page_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpCelebMyPageHeaderDto> httpCelebMyPageHeader({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/header", null);
  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];

  return HttpCelebMyPageHeaderDto(
    name: data['name'],
    subscriberCount: data['subscriberCount'],
    feedCount: data['feedCount'],
    profileImageUrl: data['profileImageUrl']
  );
}
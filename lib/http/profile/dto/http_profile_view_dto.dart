
class HttpProfileViewDto {
  final bool isCeleb;

  final bool isSubscribing;

  final String name;

  final String greeting;
  final List<Map<String, String>> introductions;

  final int subscriberCount;
  final int feedCount;

  final List<int> interests;

  final String instagramUrl;
  final String facebookUrl;
  final String lineUrl;
  final String twitterUrl;

  final String profileImageUrl;
  final String bannerImageUrl;

  const HttpProfileViewDto({
    this.isCeleb,
    this.greeting,
    this.introductions,
    this.isSubscribing,
    this.name,
    this.subscriberCount,
    this.feedCount,
    this.interests,
    this.instagramUrl,
    this.facebookUrl,
    this.lineUrl,
    this.twitterUrl,
    this.profileImageUrl,
    this.bannerImageUrl
  }) : assert(isCeleb != null && isSubscribing != null && name != null);
}
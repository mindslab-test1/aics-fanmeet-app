
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/profile/dto/http_profile_view_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpProfileViewDto> httpCelebProfileView({BuildContext context, int userId}) async {
  assert(context != null && userId != null);
  Map<String, dynamic> body = {
    "user": userId
  };
  Map<String, dynamic> response = await postAsUser(context, "/profile/view", body);
  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];

  return HttpProfileViewDto(
      isCeleb: data['isCeleb'],
      isSubscribing: data['isSubscribing'],
      name: data['name'],
      greeting: '',
      introductions: Map<String, String>.from(data['introduction']).entries.map((e) => {e.key:e.value}).toList(),
      subscriberCount: data['subscriberCount'],
      feedCount: data['feedCount'],
      interests: List.from(data['activeInterests']),
      instagramUrl: data['introduction']['instagram'],
      facebookUrl: data['introduction']['facebookUrl'],
      lineUrl: data['introduction']['lineUrl'],
      twitterUrl: data['introduction']['twitterUrl'],
      profileImageUrl: data['profileImageUrl'],
      bannerImageUrl: data['bannerImageUrl']
  );
}
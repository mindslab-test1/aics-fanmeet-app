
class HttpMessageCelebListDto {
  final int celebId;
  final String name;
  final String profileImageUrl;
  final String text;
  final String createdTime;
  final bool isTts;

  @override
  String toString() {
    return 'HttpMessageCelebListDto{celebId: $celebId, name: $name, profileImageUrl: $profileImageUrl, text: $text, createdTime: $createdTime, isTts: $isTts}';
  }

  const HttpMessageCelebListDto({
    this.celebId,
    this.name,
    this.profileImageUrl,
    this.text,
    this.createdTime,
    this.isTts
  }) : assert(celebId != null && name != null && isTts != null);
}
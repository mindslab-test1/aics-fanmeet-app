
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';

import '../../providers/mypage_view_provider.dart';
import '../rest_call_functions.dart';

Future<bool> httpMyPageProfileView({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/mypage/profile/view", {});
  if(response["status"] != 200) return false;
  dynamic data = response["data"];

  Provider.of<MyPageViewProvider>(context, listen: false).setMyPageInfo(
    name: data["name"],
    profileImgUrl: data["profileImgUrl"],
    bannerImgUrl: data["bannerImgUrl"]
  );
  List<CategoryEnum> interestedCategory = [];
  for(int e in data["interestedCategory"]){
    interestedCategory.add(CategoryEnum.values[e]);
  }

  Provider.of<MyPageViewProvider>(context, listen: false).interestedCategoryList = interestedCategory;

  return true;
}
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';

import '../../providers/tts_write_provider.dart';
import '../rest_call_functions.dart';

Future<bool> httpTtsWriteView(int celebId, {BuildContext context}) async {
  Map<String, dynamic> body = {
    "celebId": celebId
  };
  Map<String, dynamic> response = await postAsUser(context, "/mypage/tts/writeview", body);
  if(response["status"] != 200 && response["status"] != 500) {
    Provider.of<TtsWriteProvider>(context, listen: false).setLimits();
    // set to default no use limits
    return true;
  } else if(response["status"] == 500){
    return false;
  } else {
    Provider.of<TtsWriteProvider>(context, listen: false).setLimits(
      monthlyLimit: response["data"]["monthlyRate"],
      monthlyUsed: response["data"]["monthlyUsed"],
      requestLimit: 300,
    );
  }
  
  return true;
}

Future<String> httpTtsWriteSample(int celebId, String text, {BuildContext context}) async {
  Map<String, dynamic> body = {
    "celebId": celebId,
    "text": text,
  };
  Map<String, dynamic> response = await postAsUser(context, "/mypage/tts/sample", body);

  Provider.of<TtsWriteProvider>(context, listen: false).ttsId = response["data"]["id"];
  return response["data"]["url"];
}

Future<bool> httpTtsWriteRequest({int ttsId,int celebId, BuildContext context}) async {
  Map<String, dynamic> body = {
    "id": ttsId,
    "celebId" : celebId
  };
  Map<String, dynamic> response = await postAsUser(context, "/mypage/tts/request", body);

  return response["status"] == 200;
}
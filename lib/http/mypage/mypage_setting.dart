

import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/view/setting_push/setting_push_body.dart';

import '../rest_call_functions.dart';

Future<Map<String, List<PushSwitchData>>> httpMyPageSettings({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/setting/loadflags", {});

  List<PushSwitchData> alertSettings = [];
  alertSettings.add(PushSwitchData(optionName: "소리", optionState: response["data"]["sound"]));
  alertSettings.add(PushSwitchData(optionName: "진동", optionState: response["data"]["vibration"]));

  List<PushSwitchData> myActivitySettings = [];
  myActivitySettings.add(PushSwitchData(optionName: "좋아요", optionState: response["data"]["myLike"]));
  myActivitySettings.add(PushSwitchData(optionName: "스크랩", optionState: response["data"]["myScrap"]));
  myActivitySettings.add(PushSwitchData(optionName: "댓글", optionState: response["data"]["myComment"]));
  
  List<PushSwitchData> celebActivitySettings = [];
  celebActivitySettings.add(PushSwitchData(optionName: "새로운 피드", optionState: response["data"]["celebFeed"]));
  celebActivitySettings.add(PushSwitchData(optionName: "새로운 콘텐츠", optionState: response["data"]["celebContent"]));
  celebActivitySettings.add(PushSwitchData(optionName: "셀럽의 메세지", optionState: response["data"]["celebMessage"]));

  return {
    "alertSettings": alertSettings,
    "myActivitySettings": myActivitySettings,
    "celebActivitySettings": celebActivitySettings
  };

}

//@field:NotNull
//var sound: Boolean? = null,
//@field:NotNull
//var vibration: Boolean? = null,
//@field:NotNull
//var myLike: Boolean? = null,
//@field:NotNull
//var myScrap: Boolean? = null,
//@field:NotNull
//var myComment: Boolean? = null,
//@field:NotNull
//var celebFeed: Boolean? = null,
//@field:NotNull
//var celebContent: Boolean? = null,
//@field:NotNull
//var celebMessage: Boolean? = null,
//@field:NotNull
//var celebActivity: Boolean? = null


import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpReportFeed({BuildContext context, int whomId, int feedId, String reason}) async {
  assert(context != null && whomId != null && feedId != null);
  Map<String, dynamic> body = {
    "whom": whomId,
    "feedId": feedId,
    "reason": reason
  };
  Map<String, dynamic> response = await postAsUser(context, "/report/feed", body);
  if (response == null || response['status'] != 200) return null;
  return true;
}
class HttpReportHistoryDetailVo {
  final int objectId;
  final String type;
  final String reportedDate;
  final String reason;
  final int reportCount;

  HttpReportHistoryDetailVo({
    this.objectId,
    this.type,
    this.reportedDate,
    this.reason,
    this.reportCount
  }) : assert(objectId != null && type != null && reportedDate != null && reason != null && reportCount != null);
}
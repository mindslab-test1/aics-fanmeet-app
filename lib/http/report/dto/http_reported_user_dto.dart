class HttpReportedUserDto {
  final int userId;
  final String userName;
  final String profileImageUrl;
  final int reportCount;

  const HttpReportedUserDto({
    this.userId,
    this.userName,
    this.profileImageUrl,
    this.reportCount
  }) : assert(userId != null && userName != null && reportCount != null);
}

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpReportComment({BuildContext context, int whomId, int commentId, String reason}) async {
  assert(context != null && whomId != null && commentId != null);
  Map<String, dynamic> body = {
    "whom": whomId,
    "commentId": commentId,
    "reason": reason
  };
  Map<String, dynamic> response = await postAsUser(context, "/report/comment", body);
  if (response == null || response['status'] != 200) return null;
  return true;
}

import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/report/dto/http_report_history_detail_vo.dart';
import 'package:rf_tap_fanseem/http/report/dto/http_reported_user_detail_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpReportedUserDetailDto> httpReportedUserList({BuildContext context, int userId}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "userId": userId
  };
  Map<String, dynamic> response = await postAsUser(context, "/report/list/detail", body);
  if (response == null || response['status'] != 200) return null;

  Map<String, dynamic> data = response['data'];
  List<HttpReportHistoryDetailVo> historyDetailList = List();
  for (Map<String, dynamic> iter in List.from(data['historyDetailList'])) {
    historyDetailList.add(HttpReportHistoryDetailVo(
      objectId: iter['objectId'],
      type: iter['type'],
      reportedDate: iter['reportedDate'],
      reason: iter['text'],
      reportCount: iter['reportCount']
    ));
  }

  return HttpReportedUserDetailDto(
    userId: data['userId'],
    userName: data['userName'],
    profileImageUrl: data['profileImageUrl'],
    reportCount: data['reportCount'],
    historyDetailList: historyDetailList
  );
}
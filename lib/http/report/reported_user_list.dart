
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/report/dto/http_reported_user_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<List<HttpReportedUserDto>> httpReportedUserList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> body = {};
  Map<String, dynamic> response = await postAsUser(context, "/report/list", body);
  if (response == null || response['status'] != 200) return null;

  List<HttpReportedUserDto> result = List();
  for (Map<String, dynamic> iter in List.from(response['data']['userList'])) {
    result.add(HttpReportedUserDto(
        userId: iter['userId'],
        userName: iter['userName'],
        profileImageUrl: iter['profileImageUrl'],
        reportCount: iter['reportCount']
    ));
  }

  return result;
}
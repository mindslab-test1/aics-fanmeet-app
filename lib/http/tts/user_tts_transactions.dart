
import 'package:flutter/cupertino.dart';

import '../rest_call_functions.dart';

Future httpSampleTts({String text, int celebId, BuildContext context}) async {
  Map<String, dynamic> body = {
    "text": text,
    "celebId": celebId
  };
  Map<String, dynamic> responseRaw = await postAsUser(context, "/mypage/tts/sample", body);
  switch(responseRaw["status"]){
    case 200:
      break;
    case 400:
      return TtsSampleStatus.textTooLong;
    case 403:
      return TtsSampleStatus.forbidden;
    case 415:
      return TtsSampleStatus.noModel;
    case 500:
    default:
      return TtsSampleStatus.internal;
  }

  Map response = {
    "url": responseRaw["data"]["url"],
    "sampleId": responseRaw["data"]["id"],
  };

  return response;
}

Future<TtsSampleStatus> httpRequestTts({int sampleId, BuildContext context}) async {
  Map<String, dynamic> body = {
    "id": sampleId
  };
  Map<String, dynamic> responseRaw = await postAsUser(context, "/mypage/tts/request", body);

  switch(responseRaw["status"]){
    case 200:
      return TtsSampleStatus.ok;
    case 400:
      return TtsSampleStatus.textTooLong;
    case 403:
      return TtsSampleStatus.forbidden;
    case 415:
      return TtsSampleStatus.noModel;
    case 500:
    default:
      return TtsSampleStatus.internal;
  }
}

enum TtsSampleStatus{
  ok,
  textTooLong,
  forbidden,
  noModel,
  internal
}